N=5;
PBWIDTH=11+1;
PBLENGTH=74+1;
PBHEIGHT=5.4+1;
WALL=2;

difference() {
    // Cube holding everything
    // add 1mm on each side for "wall"
    cube([N*PBWIDTH+WALL*(N+3),PBLENGTH+2*WALL,PBHEIGHT+2*WALL]);

    // Subtract the cubbies for the PBs
    for(i=[0:N-1])
    {
        translate([i*PBWIDTH+WALL*(i+2),WALL,WALL]) cube([PBWIDTH,PBLENGTH+10,PBHEIGHT+10]);
    }

    // Subtract space for roof
    translate([WALL,WALL,PBHEIGHT+WALL]) cube([N*PBWIDTH+WALL*(N+1),PBLENGTH+WALL,WALL]);

    // Subtract space for door
    translate([WALL,PBLENGTH+WALL,0]) cube([N*PBWIDTH+WALL*(N+1),WALL,PBHEIGHT+WALL]);
}
