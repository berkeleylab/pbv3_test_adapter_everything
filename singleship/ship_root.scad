N=2;
PBWIDTH=9+1;
PBLENGTH=72+1;
PBHEIGHT=5.4+1;
WALL=2;

BUFFER_ROOF=0.5;
BUFFER_HOLD=2.0;
BUFFER_SHLD=2.0;

union() {
    // The base roof
    translate([BUFFER_ROOF,0,0]) cube([N*PBWIDTH+(N+1)*WALL-2*BUFFER_ROOF,PBLENGTH+WALL,WALL]);    
    

    // Door
    translate([BUFFER_ROOF,0,WALL]) cube([N*PBWIDTH+(N+1)*WALL-2*BUFFER_ROOF,WALL,PBHEIGHT+WALL]);

    // holder for shield
    translate([WALL,PBLENGTH-1-21.5+WALL,WALL])
        difference()
        {
            cube([(N-1)*WALL+N*PBWIDTH,1,1]);
            for(i=[0:N])
            {
                translate([0                  +i*PBWIDTH+i*WALL,0,0]) cube([BUFFER_SHLD,1,1]);
                translate([PBWIDTH-BUFFER_SHLD+i*PBWIDTH+i*WALL,0,0]) cube([BUFFER_SHLD,1,1]);
                translate([PBWIDTH            +i*PBWIDTH+i*WALL,0,0]) cube([WALL       ,1,1]);
            }
        }

    // holder for capacitor
    translate([WALL,WALL,WALL])
        difference()
        {

            cube([(N-1)*WALL+N*PBWIDTH,7,PBHEIGHT-2.1-1]);
            for(i=[0:N-1])
            {
                translate([0                  +i*PBWIDTH+i*WALL,0,0]) cube([BUFFER_HOLD,7,PBHEIGHT-2.1-1]);
                translate([PBWIDTH-BUFFER_HOLD+i*PBWIDTH+i*WALL,0,0]) cube([BUFFER_HOLD,7,PBHEIGHT-2.1-1]);
                translate([PBWIDTH            +i*PBWIDTH+i*WALL,0,0]) cube([WALL       ,7,PBHEIGHT-2.1-1]);
            }
        }
       

}
