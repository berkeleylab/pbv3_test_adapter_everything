EESchema Schematic File Version 4
LIBS:pbv3_mass_test_adapter_active-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 21 26
Title "Powerboard v3 Massive Active Tester Board"
Date "2019-06-04"
Rev ""
Comp "Lawrence Berkeley National Laboratory"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 3350 1500 2    60   ~ 12
VOUT_RTN1
Text HLabel 1350 3500 0    60   Input ~ 12
VOUT[0..11]
Entry Wire Line
	1350 3300 1450 3200
Entry Wire Line
	1350 3200 1450 3100
Entry Wire Line
	1350 3100 1450 3000
Entry Wire Line
	1350 3000 1450 2900
Entry Wire Line
	1350 2900 1450 2800
Entry Wire Line
	1350 2800 1450 2700
Entry Wire Line
	1350 2700 1450 2600
Entry Wire Line
	1350 2600 1450 2500
Entry Wire Line
	1350 2500 1450 2400
Wire Wire Line
	1450 3200 1550 3200
Wire Wire Line
	1450 3100 1550 3100
Wire Wire Line
	1450 3000 1550 3000
Wire Wire Line
	1450 2900 1550 2900
Wire Wire Line
	1450 2800 1550 2800
Wire Wire Line
	1450 2700 1550 2700
Wire Wire Line
	1450 2600 1550 2600
Wire Wire Line
	1450 2500 1550 2500
Wire Wire Line
	1450 2400 1550 2400
Text Label 1550 3200 0    60   ~ 12
VOUT1
Text Label 1550 3100 0    60   ~ 12
VOUT2
Text Label 1550 3000 0    60   ~ 12
VOUT3
Text Label 1550 2900 0    60   ~ 12
VOUT4
Text Label 1550 2800 0    60   ~ 12
VOUT5
Text Label 1550 2700 0    60   ~ 12
VOUT6
Text Label 1550 2600 0    60   ~ 12
VOUT7
Text Label 1550 2500 0    60   ~ 12
VOUT8
Text Label 1550 2400 0    60   ~ 12
VOUT9
Text HLabel 1350 2300 0    60   Input ~ 12
VOUT_RTN[0..11]
Entry Wire Line
	1350 2100 1450 2000
Entry Wire Line
	1350 2000 1450 1900
Entry Wire Line
	1350 1900 1450 1800
Entry Wire Line
	1350 1800 1450 1700
Entry Wire Line
	1350 1700 1450 1600
Entry Wire Line
	1350 1600 1450 1500
Entry Wire Line
	1350 1500 1450 1400
Entry Wire Line
	1350 1400 1450 1300
Entry Wire Line
	1350 1300 1450 1200
Wire Wire Line
	1450 2000 1550 2000
Wire Wire Line
	1450 1900 1550 1900
Wire Wire Line
	1450 1800 1550 1800
Wire Wire Line
	1450 1700 1550 1700
Wire Wire Line
	1450 1600 1550 1600
Wire Wire Line
	1450 1500 1550 1500
Wire Wire Line
	1450 1400 1550 1400
Wire Wire Line
	1450 1300 1550 1300
Wire Wire Line
	1450 1200 1550 1200
Text Label 1550 2000 0    60   ~ 12
VOUT_RTN1
Text Label 1550 1900 0    60   ~ 12
VOUT_RTN2
Text Label 1550 1800 0    60   ~ 12
VOUT_RTN3
Text Label 1550 1700 0    60   ~ 12
VOUT_RTN4
Text Label 1550 1600 0    60   ~ 12
VOUT_RTN5
Text Label 1550 1500 0    60   ~ 12
VOUT_RTN6
Text Label 1550 1400 0    60   ~ 12
VOUT_RTN7
Text Label 1550 1300 0    60   ~ 12
VOUT_RTN8
Text Label 1550 1200 0    60   ~ 12
VOUT_RTN9
Text Label 3350 1600 2    60   ~ 12
VOUT1
Text Label 3350 1950 2    60   ~ 12
VOUT2
Text Label 3350 2050 2    60   ~ 12
VOUT_RTN2
Text Label 3350 2400 2    60   ~ 12
VOUT_RTN3
Text Label 3450 4000 2    60   ~ 12
HV_OUT1
Text Label 3450 4100 2    60   ~ 12
HV_OUT2
Text Label 3450 4200 2    60   ~ 12
HV_OUT3
Text Label 3450 4300 2    60   ~ 12
HV_OUT4
Text Label 3450 4400 2    60   ~ 12
HV_OUT5
Text Label 3450 4500 2    60   ~ 12
HV_OUT6
Text Label 3450 4600 2    60   ~ 12
HV_OUT7
Text Label 3450 4700 2    60   ~ 12
HV_OUT8
Text Label 3450 4800 2    60   ~ 12
HV_OUT9
Text HLabel 1350 6500 0    60   Input ~ 12
HV_OUT[0..9]
Entry Wire Line
	1350 6300 1450 6200
Entry Wire Line
	1350 6200 1450 6100
Entry Wire Line
	1350 6100 1450 6000
Entry Wire Line
	1350 6000 1450 5900
Entry Wire Line
	1350 5900 1450 5800
Entry Wire Line
	1350 5800 1450 5700
Entry Wire Line
	1350 5700 1450 5600
Entry Wire Line
	1350 5600 1450 5500
Entry Wire Line
	1350 5500 1450 5400
Wire Wire Line
	1450 6200 1550 6200
Wire Wire Line
	1450 6100 1550 6100
Wire Wire Line
	1450 6000 1550 6000
Wire Wire Line
	1450 5900 1550 5900
Wire Wire Line
	1450 5800 1550 5800
Wire Wire Line
	1450 5700 1550 5700
Wire Wire Line
	1450 5600 1550 5600
Wire Wire Line
	1450 5500 1550 5500
Wire Wire Line
	1450 5400 1550 5400
Text HLabel 1350 5100 0    60   Input ~ 12
HV_OUT_RTN[0..9]
Entry Wire Line
	1350 4900 1450 4800
Entry Wire Line
	1350 4800 1450 4700
Entry Wire Line
	1350 4700 1450 4600
Entry Wire Line
	1350 4600 1450 4500
Entry Wire Line
	1350 4500 1450 4400
Entry Wire Line
	1350 4400 1450 4300
Entry Wire Line
	1350 4300 1450 4200
Entry Wire Line
	1350 4200 1450 4100
Entry Wire Line
	1350 4100 1450 4000
Wire Wire Line
	1450 4800 1550 4800
Wire Wire Line
	1450 4700 1550 4700
Wire Wire Line
	1450 4600 1550 4600
Wire Wire Line
	1450 4500 1550 4500
Wire Wire Line
	1450 4400 1550 4400
Wire Wire Line
	1450 4300 1550 4300
Wire Wire Line
	1450 4200 1550 4200
Wire Wire Line
	1450 4100 1550 4100
Wire Wire Line
	1450 4000 1550 4000
Text Label 1550 4800 0    60   ~ 12
HV_OUT_RTN1
Text Label 1550 4700 0    60   ~ 12
HV_OUT_RTN2
Text Label 1550 4600 0    60   ~ 12
HV_OUT_RTN3
Text Label 1550 4500 0    60   ~ 12
HV_OUT_RTN4
Text Label 1550 4400 0    60   ~ 12
HV_OUT_RTN5
Text Label 1550 4300 0    60   ~ 12
HV_OUT_RTN6
Text Label 1550 4200 0    60   ~ 12
HV_OUT_RTN7
Text Label 1550 4100 0    60   ~ 12
HV_OUT_RTN8
Text Label 1550 6200 0    60   ~ 12
HV_OUT1
Text Label 1550 6100 0    60   ~ 12
HV_OUT2
Text Label 1550 6000 0    60   ~ 12
HV_OUT3
Text Label 1550 5900 0    60   ~ 12
HV_OUT4
Text Label 1550 5800 0    60   ~ 12
HV_OUT5
Text Label 1550 5700 0    60   ~ 12
HV_OUT6
Text Label 1550 5600 0    60   ~ 12
HV_OUT7
Text Label 1550 5500 0    60   ~ 12
HV_OUT8
Text Label 1550 5400 0    60   ~ 12
HV_OUT9
Text Label 1550 2100 0    60   ~ 12
VOUT_RTN0
Text Label 1550 3300 0    60   ~ 12
VOUT0
Text Label 1550 4900 0    60   ~ 12
HV_OUT_RTN0
Text Label 1550 6300 0    60   ~ 12
HV_OUT0
Wire Wire Line
	1450 6300 1550 6300
Wire Wire Line
	1450 4900 1550 4900
Wire Wire Line
	1550 3300 1450 3300
Wire Wire Line
	1550 2100 1450 2100
Entry Wire Line
	1350 2200 1450 2100
Entry Wire Line
	1350 3400 1450 3300
Entry Wire Line
	1350 6400 1450 6300
Entry Wire Line
	1350 5000 1450 4900
Text Label 3450 3900 2    60   ~ 12
HV_OUT0
Text Label 3350 1050 2    60   ~ 12
VOUT0
Text Label 3350 1150 2    60   ~ 12
VOUT_RTN0
Text Label 3350 2500 2    60   ~ 12
VOUT3
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J2101
U 1 1 5D057286
P 3550 1050
F 0 "J2101" H 3600 1267 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 3600 1176 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 3550 1050 50  0001 C CNN
F 3 "~" H 3550 1050 50  0001 C CNN
F 4 "15910040" H 3550 1050 50  0001 C CNN "mfg#"
	1    3550 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 1150 3850 1150
Wire Wire Line
	3850 1050 3350 1050
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J2102
U 1 1 5D061789
P 3550 1500
F 0 "J2102" H 3600 1717 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 3600 1626 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 3550 1500 50  0001 C CNN
F 3 "~" H 3550 1500 50  0001 C CNN
	1    3550 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 1500 3850 1500
Wire Wire Line
	3850 1600 3350 1600
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J2103
U 1 1 5D06B399
P 3550 1950
F 0 "J2103" H 3600 2167 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 3600 2076 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 3550 1950 50  0001 C CNN
F 3 "~" H 3550 1950 50  0001 C CNN
	1    3550 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 1950 3850 1950
Wire Wire Line
	3850 2050 3350 2050
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J2104
U 1 1 5D075147
P 3550 2400
F 0 "J2104" H 3600 2617 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 3600 2526 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 3550 2400 50  0001 C CNN
F 3 "~" H 3550 2400 50  0001 C CNN
	1    3550 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 2400 3850 2400
Wire Wire Line
	3850 2500 3350 2500
Text Label 3350 2850 2    60   ~ 12
VOUT4
Text Label 3350 2950 2    60   ~ 12
VOUT_RTN4
Text Label 3350 3300 2    60   ~ 12
VOUT_RTN5
Text Label 3350 3400 2    60   ~ 12
VOUT5
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J2105
U 1 1 5D07F2F6
P 3550 2850
F 0 "J2105" H 3600 3067 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 3600 2976 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 3550 2850 50  0001 C CNN
F 3 "~" H 3550 2850 50  0001 C CNN
	1    3550 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 2850 3850 2850
Wire Wire Line
	3850 2950 3350 2950
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J2106
U 1 1 5D07F302
P 3550 3300
F 0 "J2106" H 3600 3517 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 3600 3426 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 3550 3300 50  0001 C CNN
F 3 "~" H 3550 3300 50  0001 C CNN
	1    3550 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 3300 3850 3300
Wire Wire Line
	3850 3400 3350 3400
Text Label 4900 1050 2    60   ~ 12
VOUT6
Text Label 4900 1150 2    60   ~ 12
VOUT_RTN6
Text Label 4900 1500 2    60   ~ 12
VOUT_RTN7
Text Label 4900 1600 2    60   ~ 12
VOUT7
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J2108
U 1 1 5D085C2B
P 5100 1050
F 0 "J2108" H 5150 1267 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 5150 1176 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 5100 1050 50  0001 C CNN
F 3 "~" H 5100 1050 50  0001 C CNN
	1    5100 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1050 5400 1050
Wire Wire Line
	5400 1150 4900 1150
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J2109
U 1 1 5D085C37
P 5100 1500
F 0 "J2109" H 5150 1717 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 5150 1626 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 5100 1500 50  0001 C CNN
F 3 "~" H 5100 1500 50  0001 C CNN
	1    5100 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1500 5400 1500
Wire Wire Line
	5400 1600 4900 1600
Text Label 4900 1950 2    60   ~ 12
VOUT8
Text Label 4900 2050 2    60   ~ 12
VOUT_RTN8
Text Label 4900 2400 2    60   ~ 12
VOUT_RTN9
Text Label 4900 2500 2    60   ~ 12
VOUT9
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J2110
U 1 1 5D09DF6C
P 5100 1950
F 0 "J2110" H 5150 2167 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 5150 2076 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 5100 1950 50  0001 C CNN
F 3 "~" H 5100 1950 50  0001 C CNN
	1    5100 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1950 5400 1950
Wire Wire Line
	5400 2050 4900 2050
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J2111
U 1 1 5D09DF78
P 5100 2400
F 0 "J2111" H 5150 2617 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 5150 2526 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 5100 2400 50  0001 C CNN
F 3 "~" H 5100 2400 50  0001 C CNN
	1    5100 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 2400 5400 2400
Wire Wire Line
	5400 2500 4900 2500
Text Label 4900 2850 2    60   ~ 12
VOUT10
Text Label 4900 2950 2    60   ~ 12
VOUT_RTN10
Text Label 4900 3300 2    60   ~ 12
VOUT_RTN11
Text Label 4900 3400 2    60   ~ 12
VOUT11
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J2112
U 1 1 5D0B3DA3
P 5100 2850
F 0 "J2112" H 5150 3067 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 5150 2976 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 5100 2850 50  0001 C CNN
F 3 "~" H 5100 2850 50  0001 C CNN
	1    5100 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 2850 5400 2850
Wire Wire Line
	5400 2950 4900 2950
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J2113
U 1 1 5D0B3DAF
P 5100 3300
F 0 "J2113" H 5150 3517 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 5150 3426 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 5100 3300 50  0001 C CNN
F 3 "~" H 5100 3300 50  0001 C CNN
	1    5100 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3300 5400 3300
Wire Wire Line
	5400 3400 4900 3400
Text Label 1550 4000 0    60   ~ 12
HV_OUT_RTN9
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J2107
U 1 1 5D27738A
P 3650 4300
F 0 "J2107" H 3700 4917 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 3700 4826 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x10_P2.54mm_Vertical_SMD" H 3650 4300 50  0001 C CNN
F 3 "~" H 3650 4300 50  0001 C CNN
F 4 "15912200" H 3650 4300 50  0001 C CNN "mfg#"
	1    3650 4300
	1    0    0    -1  
$EndComp
Text Label 3950 4000 0    60   ~ 12
HV_OUT_RTN1
Text Label 3950 4100 0    60   ~ 12
HV_OUT_RTN2
Text Label 3950 4200 0    60   ~ 12
HV_OUT_RTN3
Text Label 3950 4300 0    60   ~ 12
HV_OUT_RTN4
Text Label 3950 4400 0    60   ~ 12
HV_OUT_RTN5
Text Label 3950 4500 0    60   ~ 12
HV_OUT_RTN6
Text Label 3950 4600 0    60   ~ 12
HV_OUT_RTN7
Text Label 3950 4700 0    60   ~ 12
HV_OUT_RTN8
Text Label 3950 4800 0    60   ~ 12
HV_OUT_RTN9
Text Label 3950 3900 0    60   ~ 12
HV_OUT_RTN0
Wire Bus Line
	1350 1300 1350 2300
Wire Bus Line
	1350 2500 1350 3500
Wire Bus Line
	1350 5500 1350 6500
Wire Bus Line
	1350 4100 1350 5100
$EndSCHEMATC
