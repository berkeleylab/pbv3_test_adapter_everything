EESchema Schematic File Version 4
LIBS:pbv3_mass_test_adapter_active-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 25 26
Title "Powerboard v3 Massive Active Tester Board"
Date "2019-06-04"
Rev ""
Comp "Lawrence Berkeley National Laboratory"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 7100 1100 0    60   Output ~ 12
A0_CMDin_P
Text HLabel 7600 1100 2    60   Output ~ 12
A0_CMDin_N
Text HLabel 7100 1200 0    60   Input ~ 12
A0_CMDout_P
Text HLabel 7600 1200 2    60   Input ~ 12
A0_CMDout_N
Text HLabel 7100 900  0    60   BiDi ~ 12
A0_SDA
Text HLabel 7100 1000 0    60   Output ~ 12
A0_SCL
Text HLabel 1350 2750 0    60   Input ~ 12
A1_CMDin_P
Text HLabel 1850 2750 2    60   Input ~ 12
A1_CMDin_N
Text HLabel 1350 2850 0    60   Output ~ 12
A1_CMDout_P
Text HLabel 1850 2850 2    60   Output ~ 12
A1_CMDout_N
Text HLabel 1350 2550 0    60   BiDi ~ 12
A1_SDA
Text HLabel 1350 2650 0    60   Input ~ 12
A1_SCL
Text HLabel 3300 2750 0    60   Input ~ 12
A2_CMDin_P
Text HLabel 3300 2850 0    60   Output ~ 12
A2_CMDout_P
Text HLabel 3300 2550 0    60   BiDi ~ 12
A2_SDA
Text HLabel 3300 2650 0    60   Input ~ 12
A2_SCL
Text HLabel 5250 2750 0    60   Input ~ 12
A3_CMDin_P
Text HLabel 5250 2850 0    60   Output ~ 12
A3_CMDout_P
Text HLabel 5250 2550 0    60   BiDi ~ 12
A3_SDA
Text HLabel 5250 2650 0    60   Input ~ 12
A3_SCL
Text HLabel 7200 2750 0    60   Input ~ 12
A4_CMDin_P
Text HLabel 7700 2750 2    60   Input ~ 12
A4_CMDin_N
Text HLabel 7200 2850 0    60   Output ~ 12
A4_CMDout_P
Text HLabel 7700 2850 2    60   Output ~ 12
A4_CMDout_N
Text HLabel 7200 2550 0    60   BiDi ~ 12
A4_SDA
Text HLabel 7200 2650 0    60   Input ~ 12
A4_SCL
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J2501
U 1 1 5D4C52BE
P 1550 2750
F 0 "J2501" H 1600 3167 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 1600 3076 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Horizontal" H 1550 2750 50  0001 C CNN
F 3 "~" H 1550 2750 50  0001 C CNN
F 4 "68021-412HLF" H 1550 2750 50  0001 C CNN "mfg#"
	1    1550 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 2550 1850 2650
Text HLabel 1350 2950 0    60   Input ~ 0
A1_SCLK
Text HLabel 1350 3050 0    60   Input ~ 0
A1_DIN
Text HLabel 1850 3050 2    60   Output ~ 0
A1_DOUT
Text HLabel 1850 2950 2    60   Input ~ 0
~A1_CS
Text HLabel 5750 2850 2    60   Output ~ 12
A3_CMDout_N
Text HLabel 5750 2750 2    60   Input ~ 12
A3_CMDin_N
$Comp
L power:GND #PWR02501
U 1 1 5D4CAB4C
P 1975 2500
F 0 "#PWR02501" H 1975 2250 50  0001 C CNN
F 1 "GND" H 1980 2327 50  0000 C CNN
F 2 "" H 1975 2500 50  0001 C CNN
F 3 "" H 1975 2500 50  0001 C CNN
	1    1975 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 2550 1850 2500
Wire Wire Line
	1850 2500 1975 2500
Connection ~ 1850 2550
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J2502
U 1 1 5D4CD182
P 3500 2750
F 0 "J2502" H 3550 3167 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 3550 3076 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Horizontal" H 3500 2750 50  0001 C CNN
F 3 "~" H 3500 2750 50  0001 C CNN
	1    3500 2750
	1    0    0    -1  
$EndComp
Text HLabel 3300 2950 0    60   Input ~ 0
A2_SCLK
Text HLabel 3300 3050 0    60   Input ~ 0
A2_DIN
Text HLabel 3800 3050 2    60   Output ~ 0
A2_DOUT
Text HLabel 3800 2950 2    60   Input ~ 0
~A2_CS
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J2503
U 1 1 5D4CEA9B
P 5450 2750
F 0 "J2503" H 5500 3167 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 5500 3076 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Horizontal" H 5450 2750 50  0001 C CNN
F 3 "~" H 5450 2750 50  0001 C CNN
	1    5450 2750
	1    0    0    -1  
$EndComp
Text HLabel 5250 2950 0    60   Input ~ 0
A3_SCLK
Text HLabel 5250 3050 0    60   Input ~ 0
A3_DIN
Text HLabel 5750 3050 2    60   Output ~ 0
A3_DOUT
Text HLabel 5750 2950 2    60   Input ~ 0
~A3_CS
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J2505
U 1 1 5D4CFC1C
P 7400 2750
F 0 "J2505" H 7450 3167 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 7450 3076 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Horizontal" H 7400 2750 50  0001 C CNN
F 3 "~" H 7400 2750 50  0001 C CNN
	1    7400 2750
	1    0    0    -1  
$EndComp
Text HLabel 7200 2950 0    60   Input ~ 0
A4_SCLK
Text HLabel 7200 3050 0    60   Input ~ 0
A4_DIN
Text HLabel 7700 3050 2    60   Output ~ 0
A4_DOUT
Text HLabel 7700 2950 2    60   Input ~ 0
~A4_CS
$Comp
L power:GND #PWR02502
U 1 1 5D4D128C
P 3925 2500
F 0 "#PWR02502" H 3925 2250 50  0001 C CNN
F 1 "GND" H 3930 2327 50  0000 C CNN
F 2 "" H 3925 2500 50  0001 C CNN
F 3 "" H 3925 2500 50  0001 C CNN
	1    3925 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2500 3925 2500
$Comp
L power:GND #PWR02503
U 1 1 5D4D1A9E
P 5875 2500
F 0 "#PWR02503" H 5875 2250 50  0001 C CNN
F 1 "GND" H 5880 2327 50  0000 C CNN
F 2 "" H 5875 2500 50  0001 C CNN
F 3 "" H 5875 2500 50  0001 C CNN
	1    5875 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2500 5875 2500
$Comp
L power:GND #PWR02505
U 1 1 5D4D2804
P 7825 2500
F 0 "#PWR02505" H 7825 2250 50  0001 C CNN
F 1 "GND" H 7830 2327 50  0000 C CNN
F 2 "" H 7825 2500 50  0001 C CNN
F 3 "" H 7825 2500 50  0001 C CNN
	1    7825 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 2500 7825 2500
Wire Wire Line
	7700 2500 7700 2550
Connection ~ 7700 2550
Wire Wire Line
	7700 2550 7700 2650
Wire Wire Line
	5750 2500 5750 2550
Connection ~ 5750 2550
Wire Wire Line
	5750 2550 5750 2650
Wire Wire Line
	3800 2500 3800 2550
Connection ~ 3800 2550
Wire Wire Line
	3800 2550 3800 2650
Text HLabel 3800 2750 2    60   Input ~ 12
A2_CMDin_N
Text HLabel 3800 2850 2    60   Output ~ 12
A2_CMDout_N
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J2504
U 1 1 5D4D3E8C
P 7300 1100
F 0 "J2504" H 7350 1517 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 7350 1426 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Horizontal" H 7300 1100 50  0001 C CNN
F 3 "~" H 7300 1100 50  0001 C CNN
	1    7300 1100
	1    0    0    -1  
$EndComp
Text HLabel 7100 1300 0    60   Output ~ 0
A0_SCLK
Text HLabel 7100 1400 0    60   Output ~ 0
A0_DIN
Text HLabel 7600 1400 2    60   Input ~ 0
A0_DOUT
Text HLabel 7600 1300 2    60   Output ~ 0
~A0_CS
$Comp
L power:GND #PWR02504
U 1 1 5D4D5508
P 7725 850
F 0 "#PWR02504" H 7725 600 50  0001 C CNN
F 1 "GND" H 7730 677 50  0000 C CNN
F 2 "" H 7725 850 50  0001 C CNN
F 3 "" H 7725 850 50  0001 C CNN
	1    7725 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 850  7725 850 
Wire Wire Line
	7600 1000 7600 900 
Connection ~ 7600 900 
Wire Wire Line
	7600 900  7600 850 
$EndSCHEMATC
