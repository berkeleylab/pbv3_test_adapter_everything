EESchema Schematic File Version 4
LIBS:pbv3_mass_test_adapter_active-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 36
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5D222B79
P 5600 3800
AR Path="/59CA64BD/5D222B79" Ref="Q?"  Part="1" 
AR Path="/59CA64BD/5D21E4ED/5D222B79" Ref="Q1"  Part="1" 
AR Path="/59CA64BD/5D257A30/5D222B79" Ref="Q2"  Part="1" 
AR Path="/59CA64BD/5D25C872/5D222B79" Ref="Q3"  Part="1" 
AR Path="/59CA64BD/5D25C877/5D222B79" Ref="Q4"  Part="1" 
AR Path="/59CA64BD/5D2619E4/5D222B79" Ref="Q5"  Part="1" 
AR Path="/59CA64BD/5D2619E9/5D222B79" Ref="Q6"  Part="1" 
AR Path="/59CA64BD/5D267183/5D222B79" Ref="Q7"  Part="1" 
AR Path="/59CA64BD/5D267188/5D222B79" Ref="Q8"  Part="1" 
AR Path="/59CA64BD/5D26718D/5D222B79" Ref="Q9"  Part="1" 
AR Path="/59CA64BD/5D267192/5D222B79" Ref="Q10"  Part="1" 
AR Path="/59CA64BD/5D267197/5D222B79" Ref="Q11"  Part="1" 
AR Path="/59CA64BD/5D26719C/5D222B79" Ref="Q12"  Part="1" 
F 0 "Q10" H 5800 3850 50  0000 L CNN
F 1 "SSM3J355R" H 5800 3750 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5800 3900 50  0001 C CNN
F 3 "" H 5600 3800 50  0001 C CNN
	1    5600 3800
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5D222B80
P 5500 4250
AR Path="/59CA64BD/5D222B80" Ref="R?"  Part="1" 
AR Path="/59CA64BD/5D21E4ED/5D222B80" Ref="R1"  Part="1" 
AR Path="/59CA64BD/5D257A30/5D222B80" Ref="R2"  Part="1" 
AR Path="/59CA64BD/5D25C872/5D222B80" Ref="R3"  Part="1" 
AR Path="/59CA64BD/5D25C877/5D222B80" Ref="R4"  Part="1" 
AR Path="/59CA64BD/5D2619E4/5D222B80" Ref="R5"  Part="1" 
AR Path="/59CA64BD/5D2619E9/5D222B80" Ref="R6"  Part="1" 
AR Path="/59CA64BD/5D267183/5D222B80" Ref="R7"  Part="1" 
AR Path="/59CA64BD/5D267188/5D222B80" Ref="R8"  Part="1" 
AR Path="/59CA64BD/5D26718D/5D222B80" Ref="R9"  Part="1" 
AR Path="/59CA64BD/5D267192/5D222B80" Ref="R10"  Part="1" 
AR Path="/59CA64BD/5D267197/5D222B80" Ref="R11"  Part="1" 
AR Path="/59CA64BD/5D26719C/5D222B80" Ref="R12"  Part="1" 
F 0 "R10" V 5580 4250 50  0000 C CNN
F 1 "621" V 5500 4250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5430 4250 50  0001 C CNN
F 3 "" H 5500 4250 50  0001 C CNN
	1    5500 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5D222B86
P 5500 4650
AR Path="/59CA64BD/5D222B86" Ref="D?"  Part="1" 
AR Path="/59CA64BD/5D21E4ED/5D222B86" Ref="D1"  Part="1" 
AR Path="/59CA64BD/5D257A30/5D222B86" Ref="D2"  Part="1" 
AR Path="/59CA64BD/5D25C872/5D222B86" Ref="D3"  Part="1" 
AR Path="/59CA64BD/5D25C877/5D222B86" Ref="D4"  Part="1" 
AR Path="/59CA64BD/5D2619E4/5D222B86" Ref="D5"  Part="1" 
AR Path="/59CA64BD/5D2619E9/5D222B86" Ref="D6"  Part="1" 
AR Path="/59CA64BD/5D267183/5D222B86" Ref="D7"  Part="1" 
AR Path="/59CA64BD/5D267188/5D222B86" Ref="D8"  Part="1" 
AR Path="/59CA64BD/5D26718D/5D222B86" Ref="D9"  Part="1" 
AR Path="/59CA64BD/5D267192/5D222B86" Ref="D10"  Part="1" 
AR Path="/59CA64BD/5D267197/5D222B86" Ref="D11"  Part="1" 
AR Path="/59CA64BD/5D26719C/5D222B86" Ref="D12"  Part="1" 
F 0 "D10" H 5500 4750 50  0000 C CNN
F 1 "LED" H 5500 4550 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5500 4650 50  0001 C CNN
F 3 "" H 5500 4650 50  0001 C CNN
	1    5500 4650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5500 3600 5500 3500
Wire Wire Line
	5500 4000 5500 4100
Wire Wire Line
	5500 4500 5500 4400
Wire Wire Line
	5500 4900 5500 4800
$Comp
L power:GND #PWR?
U 1 1 5D222B90
P 5750 3500
AR Path="/59CA64BD/5D222B90" Ref="#PWR?"  Part="1" 
AR Path="/59CA64BD/5D21E4ED/5D222B90" Ref="#PWR0101"  Part="1" 
AR Path="/59CA64BD/5D257A30/5D222B90" Ref="#PWR0103"  Part="1" 
AR Path="/59CA64BD/5D25C872/5D222B90" Ref="#PWR0105"  Part="1" 
AR Path="/59CA64BD/5D25C877/5D222B90" Ref="#PWR0107"  Part="1" 
AR Path="/59CA64BD/5D2619E4/5D222B90" Ref="#PWR0109"  Part="1" 
AR Path="/59CA64BD/5D2619E9/5D222B90" Ref="#PWR0111"  Part="1" 
AR Path="/59CA64BD/5D267183/5D222B90" Ref="#PWR0113"  Part="1" 
AR Path="/59CA64BD/5D267188/5D222B90" Ref="#PWR0115"  Part="1" 
AR Path="/59CA64BD/5D26718D/5D222B90" Ref="#PWR0117"  Part="1" 
AR Path="/59CA64BD/5D267192/5D222B90" Ref="#PWR0119"  Part="1" 
AR Path="/59CA64BD/5D267197/5D222B90" Ref="#PWR0121"  Part="1" 
AR Path="/59CA64BD/5D26719C/5D222B90" Ref="#PWR0123"  Part="1" 
F 0 "#PWR0119" H 5750 3250 50  0001 C CNN
F 1 "GND" H 5750 3350 50  0000 C CNN
F 2 "" H 5750 3500 50  0001 C CNN
F 3 "" H 5750 3500 50  0001 C CNN
	1    5750 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3500 5750 3500
$Comp
L power:-5V #PWR?
U 1 1 5D222B97
P 5750 4900
AR Path="/59CA64BD/5D222B97" Ref="#PWR?"  Part="1" 
AR Path="/59CA64BD/5D21E4ED/5D222B97" Ref="#PWR0102"  Part="1" 
AR Path="/59CA64BD/5D257A30/5D222B97" Ref="#PWR0104"  Part="1" 
AR Path="/59CA64BD/5D25C872/5D222B97" Ref="#PWR0106"  Part="1" 
AR Path="/59CA64BD/5D25C877/5D222B97" Ref="#PWR0108"  Part="1" 
AR Path="/59CA64BD/5D2619E4/5D222B97" Ref="#PWR0110"  Part="1" 
AR Path="/59CA64BD/5D2619E9/5D222B97" Ref="#PWR0112"  Part="1" 
AR Path="/59CA64BD/5D267183/5D222B97" Ref="#PWR0114"  Part="1" 
AR Path="/59CA64BD/5D267188/5D222B97" Ref="#PWR0116"  Part="1" 
AR Path="/59CA64BD/5D26718D/5D222B97" Ref="#PWR0118"  Part="1" 
AR Path="/59CA64BD/5D267192/5D222B97" Ref="#PWR0120"  Part="1" 
AR Path="/59CA64BD/5D267197/5D222B97" Ref="#PWR0122"  Part="1" 
AR Path="/59CA64BD/5D26719C/5D222B97" Ref="#PWR0124"  Part="1" 
F 0 "#PWR0120" H 5750 5000 50  0001 C CNN
F 1 "-5V" H 5750 5050 50  0000 C CNN
F 2 "" H 5750 4900 50  0001 C CNN
F 3 "" H 5750 4900 50  0001 C CNN
	1    5750 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 4900 5750 4900
Text HLabel 5800 3800 2    60   Input ~ 0
HV
$EndSCHEMATC
