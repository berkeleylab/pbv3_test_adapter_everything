EESchema Schematic File Version 4
LIBS:pbv3_mass_test_adapter_active-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 26 26
Title "Powerboard v3 Massive Active Tester Board"
Date "2019-06-04"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1150 1000 0    60   Input ~ 12
COIL_P[0..9]
Text HLabel 1150 2400 0    60   Input ~ 12
COIL_N[0..9]
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J2301
U 1 1 5CE45EAD
P 4950 1900
F 0 "J2301" H 5000 2517 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 5000 2426 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x10_P2.54mm_Vertical_SMD" H 4950 1900 50  0001 C CNN
F 3 "~" H 4950 1900 50  0001 C CNN
	1    4950 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 1500 4700 1500
Wire Wire Line
	4750 1600 4700 1600
Wire Wire Line
	4750 1700 4700 1700
Wire Wire Line
	4750 1900 4700 1900
Wire Wire Line
	4750 2100 4700 2100
Wire Wire Line
	4750 2300 4700 2300
Wire Wire Line
	4750 1800 4700 1800
Wire Wire Line
	4750 2000 4700 2000
Wire Wire Line
	4750 2200 4700 2200
Wire Wire Line
	4750 2400 4700 2400
Wire Wire Line
	5250 1500 5300 1500
Wire Wire Line
	5250 1600 5300 1600
Wire Wire Line
	5250 1700 5300 1700
Wire Wire Line
	5250 1800 5300 1800
Wire Wire Line
	5250 1900 5300 1900
Wire Wire Line
	5250 2000 5300 2000
Wire Wire Line
	5250 2100 5300 2100
Wire Wire Line
	5250 2200 5300 2200
Wire Wire Line
	5250 2300 5300 2300
Wire Wire Line
	5250 2400 5300 2400
Text Label 4700 1500 2    60   ~ 12
COIL_P0
Text Label 4700 1600 2    60   ~ 12
COIL_P1
Text Label 4700 1700 2    60   ~ 12
COIL_P2
Text Label 4700 1800 2    60   ~ 12
COIL_P3
Text Label 4700 1900 2    60   ~ 12
COIL_P4
Text Label 4700 2000 2    60   ~ 12
COIL_P5
Text Label 4700 2100 2    60   ~ 12
COIL_P6
Text Label 4700 2200 2    60   ~ 12
COIL_P7
Text Label 4700 2300 2    60   ~ 12
COIL_P8
Text Label 4700 2400 2    60   ~ 12
COIL_P9
Text Label 5300 1500 0    60   ~ 12
COIL_N0
Text Label 5300 1600 0    60   ~ 12
COIL_N1
Text Label 5300 1700 0    60   ~ 12
COIL_N2
Text Label 5300 1800 0    60   ~ 12
COIL_N3
Text Label 5300 1900 0    60   ~ 12
COIL_N4
Text Label 5300 2000 0    60   ~ 12
COIL_N5
Text Label 5300 2100 0    60   ~ 12
COIL_N6
Text Label 5300 2200 0    60   ~ 12
COIL_N7
Text Label 5300 2300 0    60   ~ 12
COIL_N8
Text Label 5300 2400 0    60   ~ 12
COIL_N9
Entry Wire Line
	1150 1100 1250 1200
Entry Wire Line
	1150 1200 1250 1300
Entry Wire Line
	1150 1300 1250 1400
Entry Wire Line
	1150 1400 1250 1500
Entry Wire Line
	1150 1500 1250 1600
Entry Wire Line
	1150 1600 1250 1700
Entry Wire Line
	1150 1700 1250 1800
Entry Wire Line
	1150 1800 1250 1900
Entry Wire Line
	1150 1900 1250 2000
Entry Wire Line
	1150 2000 1250 2100
Text Label 1300 1200 0    60   ~ 12
COIL_P0
Text Label 1300 1300 0    60   ~ 12
COIL_P1
Text Label 1300 1400 0    60   ~ 12
COIL_P2
Text Label 1300 1500 0    60   ~ 12
COIL_P3
Text Label 1300 1600 0    60   ~ 12
COIL_P4
Text Label 1300 1700 0    60   ~ 12
COIL_P5
Text Label 1300 1800 0    60   ~ 12
COIL_P6
Text Label 1300 1900 0    60   ~ 12
COIL_P7
Text Label 1300 2000 0    60   ~ 12
COIL_P8
Text Label 1300 2100 0    60   ~ 12
COIL_P9
Wire Wire Line
	1300 1200 1250 1200
Wire Wire Line
	1300 1300 1250 1300
Wire Wire Line
	1300 1400 1250 1400
Wire Wire Line
	1300 1500 1250 1500
Wire Wire Line
	1300 1600 1250 1600
Wire Wire Line
	1300 1700 1250 1700
Wire Wire Line
	1300 1800 1250 1800
Wire Wire Line
	1300 1900 1250 1900
Wire Wire Line
	1300 2000 1250 2000
Wire Wire Line
	1300 2100 1250 2100
Entry Wire Line
	1150 2500 1250 2600
Entry Wire Line
	1150 2600 1250 2700
Entry Wire Line
	1150 2700 1250 2800
Entry Wire Line
	1150 2800 1250 2900
Entry Wire Line
	1150 2900 1250 3000
Entry Wire Line
	1150 3000 1250 3100
Entry Wire Line
	1150 3100 1250 3200
Entry Wire Line
	1150 3200 1250 3300
Entry Wire Line
	1150 3300 1250 3400
Entry Wire Line
	1150 3400 1250 3500
Wire Wire Line
	1300 2600 1250 2600
Wire Wire Line
	1300 2700 1250 2700
Wire Wire Line
	1300 2800 1250 2800
Wire Wire Line
	1300 2900 1250 2900
Wire Wire Line
	1300 3000 1250 3000
Wire Wire Line
	1300 3100 1250 3100
Wire Wire Line
	1300 3200 1250 3200
Wire Wire Line
	1300 3300 1250 3300
Wire Wire Line
	1300 3400 1250 3400
Wire Wire Line
	1300 3500 1250 3500
Text Label 1300 2600 0    60   ~ 12
COIL_N0
Text Label 1300 2700 0    60   ~ 12
COIL_N1
Text Label 1300 2800 0    60   ~ 12
COIL_N2
Text Label 1300 2900 0    60   ~ 12
COIL_N3
Text Label 1300 3000 0    60   ~ 12
COIL_N4
Text Label 1300 3100 0    60   ~ 12
COIL_N5
Text Label 1300 3200 0    60   ~ 12
COIL_N6
Text Label 1300 3300 0    60   ~ 12
COIL_N7
Text Label 1300 3400 0    60   ~ 12
COIL_N8
Text Label 1300 3500 0    60   ~ 12
COIL_N9
Wire Bus Line
	1150 1000 1150 2000
Wire Bus Line
	1150 2400 1150 3400
$EndSCHEMATC
