EESchema Schematic File Version 4
LIBS:pbv3_mass_test_adapter_active-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 20 26
Title "Powerboard v3 Massive Active Tester Board"
Date "2019-06-04"
Rev ""
Comp "Lawrence Berkeley National Laboratory"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1500 650  0    60   Input ~ 12
HVIN
Text HLabel 1500 750  0    60   Output ~ 12
HVIN_RTN
Text HLabel 1350 6700 0    60   Output ~ 12
HVOUT_RTN[0..9]
Wire Bus Line
	1400 6700 1350 6700
Entry Wire Line
	1400 6750 1500 6850
Entry Wire Line
	1400 6850 1500 6950
Entry Wire Line
	1400 6950 1500 7050
Entry Wire Line
	1400 7050 1500 7150
Entry Wire Line
	1400 7150 1500 7250
Entry Wire Line
	1400 7250 1500 7350
Entry Wire Line
	1400 7350 1500 7450
Entry Wire Line
	1400 7450 1500 7550
Entry Wire Line
	1400 7550 1500 7650
Wire Wire Line
	1500 6850 1550 6850
Wire Wire Line
	1500 6950 1550 6950
Wire Wire Line
	1500 7050 1550 7050
Wire Wire Line
	1500 7150 1550 7150
Wire Wire Line
	1500 7250 1550 7250
Wire Wire Line
	1500 7350 1550 7350
Wire Wire Line
	1500 7450 1550 7450
Wire Wire Line
	1500 7550 1550 7550
Wire Wire Line
	1500 7650 1550 7650
NoConn ~ 7400 1200
Text Label 7400 1100 2    50   ~ 10
HVIN_RTN
Text Label 7400 2100 2    50   ~ 10
VOUT0
Text Label 7400 1700 2    50   ~ 10
VOUT_RTN0
Text Label 7400 2500 2    50   ~ 10
VOUT1
Text Label 7400 3700 2    50   ~ 10
VOUT2
Text Label 7400 4100 2    50   ~ 10
VOUT3
Text Label 7400 5300 2    50   ~ 10
VOUT4
Text Label 7400 5700 2    50   ~ 10
VOUT5
Text Label 7400 2900 2    50   ~ 10
VOUT_RTN1
Text Label 7400 3300 2    50   ~ 10
VOUT_RTN2
Text Label 7400 4500 2    50   ~ 10
VOUT_RTN3
Text Label 7400 4900 2    50   ~ 10
VOUT_RTN4
Text Label 9400 1100 2    50   ~ 10
VOUT_RTN5
Text Label 9400 1900 2    50   ~ 10
VOUT6
Text Label 9400 2300 2    50   ~ 10
VOUT7
Text Label 9400 3500 2    50   ~ 10
VOUT8
Text Label 9400 3900 2    50   ~ 10
VOUT9
Text Label 9400 1500 2    50   ~ 10
VOUT_RTN6
Text Label 9400 2700 2    50   ~ 10
VOUT_RTN7
Text Label 9400 3100 2    50   ~ 10
VOUT_RTN8
Text Label 9400 4300 2    50   ~ 10
VOUT_RTN9
Text Label 8100 1200 0    50   ~ 10
HVIN
NoConn ~ 8100 1300
NoConn ~ 8100 1400
Text Label 8100 1900 0    50   ~ 10
CMDin_P0
Text Label 8100 3500 0    50   ~ 10
CMDin_P2
Text Label 8100 4300 0    50   ~ 10
CMDin_P3
Text Label 8100 5100 0    50   ~ 10
CMDin_P4
Text Label 8100 5900 0    50   ~ 10
CMDin_P5
Text Label 10100 1700 0    50   ~ 10
CMDin_P6
Text Label 10100 2500 0    50   ~ 10
CMDin_P7
Text Label 10100 3300 0    50   ~ 10
CMDin_P8
Text Label 10100 4100 0    50   ~ 10
CMDin_P9
Text Label 8100 2000 0    50   ~ 10
CMDin_N0
Text Label 8100 3600 0    50   ~ 10
CMDin_N2
Text Label 8100 4400 0    50   ~ 10
CMDin_N3
Text Label 8100 5200 0    50   ~ 10
CMDin_N4
Text Label 8100 6000 0    50   ~ 10
CMDin_N5
Text Label 10100 1800 0    50   ~ 10
CMDin_N6
Text Label 10100 2600 0    50   ~ 10
CMDin_N7
Text Label 10100 3400 0    50   ~ 10
CMDin_N8
Text Label 10100 4200 0    50   ~ 10
CMDin_N9
Text Label 8100 2100 0    50   ~ 10
CMDout_P0
Text Label 8100 3700 0    50   ~ 10
CMDout_P2
Text Label 8100 4500 0    50   ~ 10
CMDout_P3
Text Label 8100 5300 0    50   ~ 10
CMDout_P4
Text Label 10100 1100 0    50   ~ 10
CMDout_P5
Text Label 10100 1900 0    50   ~ 10
CMDout_P6
Text Label 10100 2700 0    50   ~ 10
CMDout_P7
Text Label 10100 3500 0    50   ~ 10
CMDout_P8
Text Label 10100 4300 0    50   ~ 10
CMDout_P9
Text Label 8100 2200 0    50   ~ 10
CMDout_N0
Text Label 8100 3800 0    50   ~ 10
CMDout_N2
Text Label 8100 4600 0    50   ~ 10
CMDout_N3
Text Label 8100 5400 0    50   ~ 10
CMDout_N4
Text Label 10100 1200 0    50   ~ 10
CMDout_N5
Text Label 10100 2000 0    50   ~ 10
CMDout_N6
Text Label 10100 2800 0    50   ~ 10
CMDout_N7
Text Label 10100 3600 0    50   ~ 10
CMDout_N8
Text Label 10100 4400 0    50   ~ 10
CMDout_N9
Text Label 8100 2400 0    50   ~ 10
HVOUT0
Text Label 8100 4000 0    50   ~ 10
HVOUT2
Text Label 8100 4800 0    50   ~ 10
HVOUT3
Text Label 8100 5600 0    50   ~ 10
HVOUT4
Text Label 10100 1400 0    50   ~ 10
HVOUT5
Text Label 10100 2200 0    50   ~ 10
HVOUT6
Text Label 10100 3000 0    50   ~ 10
HVOUT7
Text Label 10100 3800 0    50   ~ 10
HVOUT8
Text Label 10100 4600 0    50   ~ 10
HVOUT9
Text Label 8100 2300 0    50   ~ 10
HVOUT_RTN0
Text Label 8100 3900 0    50   ~ 10
HVOUT_RTN2
Text Label 8100 4700 0    50   ~ 10
HVOUT_RTN3
Text Label 8100 5500 0    50   ~ 10
HVOUT_RTN4
Text Label 10100 1300 0    50   ~ 10
HVOUT_RTN5
Text Label 10100 2100 0    50   ~ 10
HVOUT_RTN6
Text Label 10100 2900 0    50   ~ 10
HVOUT_RTN7
Text Label 10100 3700 0    50   ~ 10
HVOUT_RTN8
Text Label 10100 4500 0    50   ~ 10
HVOUT_RTN9
Text Label 8100 2500 0    50   ~ 10
COIL_P0
Text Label 8100 4100 0    50   ~ 10
COIL_P2
Text Label 8100 4900 0    50   ~ 10
COIL_P3
Text Label 8100 5700 0    50   ~ 10
COIL_P4
Text Label 10100 1500 0    50   ~ 10
COIL_P5
Text Label 10100 2300 0    50   ~ 10
COIL_P6
Text Label 10100 3100 0    50   ~ 10
COIL_P7
Text Label 10100 3900 0    50   ~ 10
COIL_P8
Text Label 10100 4700 0    50   ~ 10
COIL_P9
Text Label 8100 2600 0    50   ~ 10
COIL_N0
Text Label 8100 4200 0    50   ~ 10
COIL_N2
Text Label 8100 5000 0    50   ~ 10
COIL_N3
Text Label 8100 5800 0    50   ~ 10
COIL_N4
Text Label 10100 1600 0    50   ~ 10
COIL_N5
Text Label 10100 2400 0    50   ~ 10
COIL_N6
Text Label 10100 3200 0    50   ~ 10
COIL_N7
Text Label 10100 4000 0    50   ~ 10
COIL_N8
Text Label 10100 4800 0    50   ~ 10
COIL_N9
Text Label 9400 5400 2    50   ~ 10
VIN
Text Label 9400 5500 2    50   ~ 10
VIN_RTN
Text Label 7400 1300 2    50   ~ 10
I2C_SCL
Text Label 7400 1400 2    50   ~ 10
I2C_SDA
Text Label 9400 4700 2    50   ~ 10
3V3
Text Label 7400 1500 2    50   ~ 10
OUT
Text Label 8100 1700 0    50   ~ 10
ANALOGUE
$Comp
L lbl_conn:Conn_02x100_Row_Letter_First-lbl_conn U2001
U 1 1 5CC7A00E
P 7750 2500
F 0 "U2001" H 7750 4165 50  0000 C CNN
F 1 "Conn_02x100_Row_Letter_First-lbl_conn" H 7750 -1150 50  0000 C CNN
F 2 "lbl_conn:Sullins_EdgeCard_200" H 6550 2500 50  0001 C CNN
F 3 "" H 6550 2500 50  0001 C CNN
F 4 "GBB100DHAN" H 7750 2500 50  0001 C CNN "mfg#"
	1    7750 2500
	1    0    0    -1  
$EndComp
Text Label 7400 1600 2    50   ~ 10
OUT_RTN
Wire Wire Line
	9400 4700 9150 4700
$Comp
L power:+3V3 #PWR02001
U 1 1 5CC7A016
P 9150 4700
F 0 "#PWR02001" H 9150 4550 50  0001 C CNN
F 1 "+3V3" H 9165 4873 50  0000 C CNN
F 2 "" H 9150 4700 50  0001 C CNN
F 3 "" H 9150 4700 50  0001 C CNN
	1    9150 4700
	-1   0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02002
U 1 1 5CC7A03A
P 8350 1200
F 0 "#FLG02002" H 8350 1275 50  0001 C CNN
F 1 "PWR_FLAG" H 8350 1374 50  0000 C CNN
F 2 "" H 8350 1200 50  0001 C CNN
F 3 "~" H 8350 1200 50  0001 C CNN
	1    8350 1200
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02001
U 1 1 5CC7A040
P 7400 1000
F 0 "#FLG02001" H 7400 1075 50  0001 C CNN
F 1 "PWR_FLAG" H 7400 1174 50  0000 C CNN
F 2 "" H 7400 1000 50  0001 C CNN
F 3 "~" H 7400 1000 50  0001 C CNN
	1    7400 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 1200 8350 1200
NoConn ~ 8100 1100
Wire Wire Line
	7400 1000 7400 1100
Connection ~ 9400 4900
Wire Wire Line
	9400 4900 9400 5000
Connection ~ 9400 5000
Wire Wire Line
	9400 5000 9400 5100
Connection ~ 9400 5500
Wire Wire Line
	9400 5500 9400 5600
Connection ~ 10100 5000
Wire Wire Line
	10100 5000 10100 4900
Connection ~ 10100 5100
Wire Wire Line
	10100 5100 10100 5000
Wire Wire Line
	10100 5500 10100 5600
Connection ~ 10100 5600
Wire Wire Line
	10100 5500 9400 5500
Wire Wire Line
	9400 5600 10100 5600
Wire Wire Line
	9400 5100 10100 5100
Wire Wire Line
	10100 5000 9400 5000
Wire Wire Line
	9400 4900 10100 4900
Wire Wire Line
	7400 1700 7400 1800
Connection ~ 7400 1800
Wire Wire Line
	7400 1800 7400 1900
Connection ~ 7400 1900
Wire Wire Line
	7400 1900 7400 2000
Wire Wire Line
	7400 2100 7400 2200
Connection ~ 7400 2200
Wire Wire Line
	7400 2200 7400 2300
Connection ~ 7400 2300
Wire Wire Line
	7400 2300 7400 2400
Wire Wire Line
	7400 2500 7400 2600
Connection ~ 7400 2600
Wire Wire Line
	7400 2600 7400 2700
Connection ~ 7400 2700
Wire Wire Line
	7400 2700 7400 2800
Wire Wire Line
	7400 2900 7400 3000
Connection ~ 7400 3000
Wire Wire Line
	7400 3000 7400 3100
Connection ~ 7400 3100
Wire Wire Line
	7400 3100 7400 3200
Wire Wire Line
	7400 3300 7400 3400
Connection ~ 7400 3400
Wire Wire Line
	7400 3400 7400 3500
Connection ~ 7400 3500
Wire Wire Line
	7400 3500 7400 3600
Wire Wire Line
	7400 3700 7400 3800
Connection ~ 7400 3800
Wire Wire Line
	7400 3800 7400 3900
Connection ~ 7400 3900
Wire Wire Line
	7400 3900 7400 4000
Wire Wire Line
	7400 4100 7400 4200
Connection ~ 7400 4200
Wire Wire Line
	7400 4200 7400 4300
Connection ~ 7400 4300
Wire Wire Line
	7400 4300 7400 4400
Wire Wire Line
	7400 4500 7400 4600
Connection ~ 7400 4600
Wire Wire Line
	7400 4600 7400 4700
Connection ~ 7400 4700
Wire Wire Line
	7400 4700 7400 4800
Wire Wire Line
	7400 4900 7400 5000
Connection ~ 7400 5000
Wire Wire Line
	7400 5000 7400 5100
Connection ~ 7400 5100
Wire Wire Line
	7400 5100 7400 5200
Wire Wire Line
	7400 5300 7400 5400
Connection ~ 7400 5400
Wire Wire Line
	7400 5400 7400 5500
Connection ~ 7400 5500
Wire Wire Line
	7400 5500 7400 5600
Wire Wire Line
	7400 5700 7400 5800
Connection ~ 7400 5800
Wire Wire Line
	7400 5800 7400 5900
Connection ~ 7400 5900
Wire Wire Line
	7400 5900 7400 6000
Connection ~ 9400 5600
Connection ~ 9400 5100
Wire Wire Line
	10100 5700 10100 5800
Wire Wire Line
	10100 5600 10100 5700
Connection ~ 10100 5700
Wire Wire Line
	10100 5200 10100 5100
$Comp
L lbl_conn:Conn_02x100_Row_Letter_First-lbl_conn U2001
U 2 1 5CC7A09F
P 9750 2500
F 0 "U2001" H 9750 4165 50  0000 C CNN
F 1 "Conn_02x100_Row_Letter_First-lbl_conn" H 9750 -1150 50  0000 C CNN
F 2 "lbl_conn:Sullins_EdgeCard_200" H 9750 2500 50  0001 C CNN
F 3 "" H 9750 2500 50  0001 C CNN
	2    9750 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 5600 9400 5700
Wire Wire Line
	9400 5200 9400 5100
Wire Wire Line
	9400 5700 9400 5800
Connection ~ 9400 5700
Text Label 8100 1800 0    50   ~ 10
ANALOGUE_RTN
Connection ~ 10100 5800
Wire Wire Line
	9400 5200 10100 5200
Wire Wire Line
	9400 5800 10100 5800
Wire Wire Line
	10100 5700 9400 5700
Connection ~ 10100 5200
Connection ~ 10100 4900
Wire Wire Line
	10100 5300 10100 5400
Connection ~ 10100 5500
Wire Wire Line
	10100 5900 10100 6000
Connection ~ 9400 5200
Connection ~ 9400 5800
Wire Wire Line
	9400 1500 9400 1600
Connection ~ 9400 1600
Wire Wire Line
	9400 1600 9400 1700
Connection ~ 9400 1700
Wire Wire Line
	9400 1700 9400 1800
Wire Wire Line
	9400 1900 9400 2000
Connection ~ 9400 2000
Wire Wire Line
	9400 2000 9400 2100
Connection ~ 9400 2100
Wire Wire Line
	9400 2100 9400 2200
Wire Wire Line
	9400 2300 9400 2400
Connection ~ 9400 2400
Wire Wire Line
	9400 2400 9400 2500
Connection ~ 9400 2500
Wire Wire Line
	9400 2500 9400 2600
Wire Wire Line
	9400 2700 9400 2800
Connection ~ 9400 2800
Wire Wire Line
	9400 2800 9400 2900
Connection ~ 9400 2900
Wire Wire Line
	9400 2900 9400 3000
Wire Wire Line
	9400 3100 9400 3200
Connection ~ 9400 3200
Wire Wire Line
	9400 3200 9400 3300
Connection ~ 9400 3300
Wire Wire Line
	9400 3300 9400 3400
Wire Wire Line
	9400 3500 9400 3600
Connection ~ 9400 3600
Wire Wire Line
	9400 3600 9400 3700
Connection ~ 9400 3700
Wire Wire Line
	9400 3700 9400 3800
Wire Wire Line
	9400 3900 9400 4000
Connection ~ 9400 4000
Wire Wire Line
	9400 4000 9400 4100
Connection ~ 9400 4100
Wire Wire Line
	9400 4100 9400 4200
Wire Wire Line
	9400 4300 9400 4400
Connection ~ 9400 4400
Wire Wire Line
	9400 4400 9400 4500
Connection ~ 9400 4500
Wire Wire Line
	9400 4500 9400 4600
Wire Wire Line
	10100 5900 10100 5800
Connection ~ 10100 5900
Wire Wire Line
	10100 5300 10100 5200
Connection ~ 10100 5300
Wire Wire Line
	9400 1100 9400 1200
Connection ~ 9400 1200
Wire Wire Line
	9400 1200 9400 1300
Connection ~ 9400 1300
Wire Wire Line
	9400 1300 9400 1400
NoConn ~ 8100 1500
NoConn ~ 8100 1600
Text Label 8100 2700 0    50   ~ 10
CMDin_P1
Text Label 8100 2800 0    50   ~ 10
CMDin_N1
Text Label 8100 2900 0    50   ~ 10
CMDout_P1
Text Label 8100 3000 0    50   ~ 10
CMDout_N1
Text Label 8100 3200 0    50   ~ 10
HVOUT1
Text Label 8100 3100 0    50   ~ 10
HVOUT_RTN1
Text Label 8100 3300 0    50   ~ 10
COIL_P1
Text Label 8100 3400 0    50   ~ 10
COIL_N1
Wire Wire Line
	9400 6000 9400 5900
Connection ~ 9400 5900
Wire Wire Line
	9400 5900 9400 5800
Wire Wire Line
	9400 5900 10100 5900
Wire Wire Line
	10100 6000 9400 6000
Connection ~ 10100 6000
Connection ~ 9400 6000
Wire Wire Line
	9400 5400 9400 5300
Connection ~ 9400 5300
Wire Wire Line
	9400 5300 9400 5200
Text Label 1550 6850 0    60   ~ 12
HVOUT_RTN0
Text Label 1550 6950 0    60   ~ 12
HVOUT_RTN1
Text Label 1550 7050 0    60   ~ 12
HVOUT_RTN2
Text Label 1550 7150 0    60   ~ 12
HVOUT_RTN3
Text Label 1550 7250 0    60   ~ 12
HVOUT_RTN4
Text Label 1550 7350 0    60   ~ 12
HVOUT_RTN5
Text Label 1550 7450 0    60   ~ 12
HVOUT_RTN6
Text Label 1550 7550 0    60   ~ 12
HVOUT_RTN7
Text Label 1550 7650 0    60   ~ 12
HVOUT_RTN8
Text Label 1550 7750 0    60   ~ 12
HVOUT_RTN9
Entry Wire Line
	1400 7650 1500 7750
Wire Wire Line
	1500 7750 1550 7750
Text HLabel 1350 5550 0    60   Output ~ 12
HVOUT[0..9]
Wire Bus Line
	1400 5550 1350 5550
Entry Wire Line
	1400 5600 1500 5700
Entry Wire Line
	1400 5700 1500 5800
Entry Wire Line
	1400 5800 1500 5900
Entry Wire Line
	1400 5900 1500 6000
Entry Wire Line
	1400 6000 1500 6100
Entry Wire Line
	1400 6100 1500 6200
Entry Wire Line
	1400 6200 1500 6300
Entry Wire Line
	1400 6300 1500 6400
Entry Wire Line
	1400 6400 1500 6500
Wire Wire Line
	1500 5700 1550 5700
Wire Wire Line
	1500 5800 1550 5800
Wire Wire Line
	1500 5900 1550 5900
Wire Wire Line
	1500 6000 1550 6000
Wire Wire Line
	1500 6100 1550 6100
Wire Wire Line
	1500 6200 1550 6200
Wire Wire Line
	1500 6300 1550 6300
Wire Wire Line
	1500 6400 1550 6400
Wire Wire Line
	1500 6500 1550 6500
Entry Wire Line
	1400 6500 1500 6600
Wire Wire Line
	1500 6600 1550 6600
Text Label 1550 5700 0    60   ~ 12
HVOUT0
Text Label 1550 5800 0    60   ~ 12
HVOUT1
Text Label 1550 5900 0    60   ~ 12
HVOUT2
Text Label 1550 6000 0    60   ~ 12
HVOUT3
Text Label 1550 6100 0    60   ~ 12
HVOUT4
Text Label 1550 6200 0    60   ~ 12
HVOUT5
Text Label 1550 6300 0    60   ~ 12
HVOUT6
Text Label 1550 6400 0    60   ~ 12
HVOUT7
Text Label 1550 6500 0    60   ~ 12
HVOUT8
Text Label 1550 6600 0    60   ~ 12
HVOUT9
Text HLabel 1350 4400 0    60   Output ~ 12
VOUT_RTN[0..9]
Wire Bus Line
	1400 4400 1350 4400
Entry Wire Line
	1400 4450 1500 4550
Entry Wire Line
	1400 4550 1500 4650
Entry Wire Line
	1400 4650 1500 4750
Entry Wire Line
	1400 4750 1500 4850
Entry Wire Line
	1400 4850 1500 4950
Entry Wire Line
	1400 4950 1500 5050
Entry Wire Line
	1400 5050 1500 5150
Entry Wire Line
	1400 5150 1500 5250
Entry Wire Line
	1400 5250 1500 5350
Wire Wire Line
	1500 4550 1550 4550
Wire Wire Line
	1500 4650 1550 4650
Wire Wire Line
	1500 4750 1550 4750
Wire Wire Line
	1500 4850 1550 4850
Wire Wire Line
	1500 4950 1550 4950
Wire Wire Line
	1500 5050 1550 5050
Wire Wire Line
	1500 5150 1550 5150
Wire Wire Line
	1500 5250 1550 5250
Wire Wire Line
	1500 5350 1550 5350
Entry Wire Line
	1400 5350 1500 5450
Wire Wire Line
	1500 5450 1550 5450
Text Label 1550 4550 0    60   ~ 12
VOUT_RTN0
Text Label 1550 4650 0    60   ~ 12
VOUT_RTN1
Text Label 1550 4750 0    60   ~ 12
VOUT_RTN2
Text Label 1550 4850 0    60   ~ 12
VOUT_RTN3
Text Label 1550 4950 0    60   ~ 12
VOUT_RTN4
Text Label 1550 5050 0    60   ~ 12
VOUT_RTN5
Text Label 1550 5150 0    60   ~ 12
VOUT_RTN6
Text Label 1550 5250 0    60   ~ 12
VOUT_RTN7
Text Label 1550 5350 0    60   ~ 12
VOUT_RTN8
Text Label 1550 5450 0    60   ~ 12
VOUT_RTN9
Wire Bus Line
	1400 3250 1350 3250
Entry Wire Line
	1400 3300 1500 3400
Entry Wire Line
	1400 3400 1500 3500
Entry Wire Line
	1400 3500 1500 3600
Entry Wire Line
	1400 3600 1500 3700
Entry Wire Line
	1400 3700 1500 3800
Entry Wire Line
	1400 3800 1500 3900
Entry Wire Line
	1400 3900 1500 4000
Entry Wire Line
	1400 4000 1500 4100
Entry Wire Line
	1400 4100 1500 4200
Wire Wire Line
	1500 3400 1550 3400
Wire Wire Line
	1500 3500 1550 3500
Wire Wire Line
	1500 3600 1550 3600
Wire Wire Line
	1500 3700 1550 3700
Wire Wire Line
	1500 3800 1550 3800
Wire Wire Line
	1500 3900 1550 3900
Wire Wire Line
	1500 4000 1550 4000
Wire Wire Line
	1500 4100 1550 4100
Wire Wire Line
	1500 4200 1550 4200
Entry Wire Line
	1400 4200 1500 4300
Wire Wire Line
	1500 4300 1550 4300
Text Label 1550 3400 0    60   ~ 12
VOUT0
Text HLabel 1350 3250 0    60   Output ~ 12
VOUT[0..9]
Text Label 1550 3500 0    60   ~ 12
VOUT1
Text Label 1550 3600 0    60   ~ 12
VOUT2
Text Label 1550 3700 0    60   ~ 12
VOUT3
Text Label 1550 3800 0    60   ~ 12
VOUT4
Text Label 1550 3900 0    60   ~ 12
VOUT5
Text Label 1550 4000 0    60   ~ 12
VOUT6
Text Label 1550 4100 0    60   ~ 12
VOUT7
Text Label 1550 4200 0    60   ~ 12
VOUT8
Text Label 1550 4300 0    60   ~ 12
VOUT9
Text HLabel 2000 650  0    60   Input ~ 12
I2C_SCL
Text HLabel 2000 750  0    60   BiDi ~ 12
I2C_SDA
Text HLabel 2000 900  0    60   Output ~ 12
OUT
Text HLabel 2000 1000 0    60   UnSpc ~ 12
OUT_RTN
Text HLabel 1500 900  0    60   Input ~ 12
ANALOGUE
Text HLabel 1500 1000 0    60   UnSpc ~ 12
ANALOGUE_RTN
Wire Bus Line
	3000 3250 2950 3250
Entry Wire Line
	3000 3300 3100 3400
Entry Wire Line
	3000 3400 3100 3500
Entry Wire Line
	3000 3500 3100 3600
Entry Wire Line
	3000 3600 3100 3700
Entry Wire Line
	3000 3700 3100 3800
Entry Wire Line
	3000 3800 3100 3900
Entry Wire Line
	3000 3900 3100 4000
Entry Wire Line
	3000 4000 3100 4100
Entry Wire Line
	3000 4100 3100 4200
Wire Wire Line
	3100 3400 3150 3400
Wire Wire Line
	3100 3500 3150 3500
Wire Wire Line
	3100 3600 3150 3600
Wire Wire Line
	3100 3700 3150 3700
Wire Wire Line
	3100 3800 3150 3800
Wire Wire Line
	3100 3900 3150 3900
Wire Wire Line
	3100 4000 3150 4000
Wire Wire Line
	3100 4100 3150 4100
Wire Wire Line
	3100 4200 3150 4200
Entry Wire Line
	3000 4200 3100 4300
Wire Wire Line
	3100 4300 3150 4300
Text HLabel 2950 3250 0    60   Output ~ 12
COIL_P[0..9]
Text Label 3150 3400 0    60   ~ 12
COIL_P0
Text Label 3150 3500 0    60   ~ 12
COIL_P1
Text Label 3150 3600 0    60   ~ 12
COIL_P2
Text Label 3150 3700 0    60   ~ 12
COIL_P3
Text Label 3150 3800 0    60   ~ 12
COIL_P4
Text Label 3150 3900 0    60   ~ 12
COIL_P5
Text Label 3150 4000 0    60   ~ 12
COIL_P6
Text Label 3150 4100 0    60   ~ 12
COIL_P7
Text Label 3150 4200 0    60   ~ 12
COIL_P8
Text Label 3150 4300 0    60   ~ 12
COIL_P9
Wire Bus Line
	4500 3250 4450 3250
Entry Wire Line
	4500 3300 4600 3400
Entry Wire Line
	4500 3400 4600 3500
Entry Wire Line
	4500 3500 4600 3600
Entry Wire Line
	4500 3600 4600 3700
Entry Wire Line
	4500 3700 4600 3800
Entry Wire Line
	4500 3800 4600 3900
Entry Wire Line
	4500 3900 4600 4000
Entry Wire Line
	4500 4000 4600 4100
Entry Wire Line
	4500 4100 4600 4200
Wire Wire Line
	4600 3400 4650 3400
Wire Wire Line
	4600 3500 4650 3500
Wire Wire Line
	4600 3600 4650 3600
Wire Wire Line
	4600 3700 4650 3700
Wire Wire Line
	4600 3800 4650 3800
Wire Wire Line
	4600 3900 4650 3900
Wire Wire Line
	4600 4000 4650 4000
Wire Wire Line
	4600 4100 4650 4100
Wire Wire Line
	4600 4200 4650 4200
Entry Wire Line
	4500 4200 4600 4300
Wire Wire Line
	4600 4300 4650 4300
Text HLabel 4450 3250 0    60   Output ~ 12
COIL_N[0..9]
Text Label 4650 3400 0    60   ~ 12
COIL_N0
Text Label 4650 3500 0    60   ~ 12
COIL_N1
Text Label 4650 3600 0    60   ~ 12
COIL_N2
Text Label 4650 3700 0    60   ~ 12
COIL_N3
Text Label 4650 3800 0    60   ~ 12
COIL_N4
Text Label 4650 3900 0    60   ~ 12
COIL_N5
Text Label 4650 4000 0    60   ~ 12
COIL_N6
Text Label 4650 4100 0    60   ~ 12
COIL_N7
Text Label 4650 4200 0    60   ~ 12
COIL_N8
Text Label 4650 4300 0    60   ~ 12
COIL_N9
Wire Bus Line
	3000 4400 2950 4400
Entry Wire Line
	3000 4450 3100 4550
Entry Wire Line
	3000 4550 3100 4650
Entry Wire Line
	3000 4650 3100 4750
Entry Wire Line
	3000 4750 3100 4850
Entry Wire Line
	3000 4850 3100 4950
Entry Wire Line
	3000 4950 3100 5050
Entry Wire Line
	3000 5050 3100 5150
Entry Wire Line
	3000 5150 3100 5250
Entry Wire Line
	3000 5250 3100 5350
Wire Wire Line
	3100 4550 3150 4550
Wire Wire Line
	3100 4650 3150 4650
Wire Wire Line
	3100 4750 3150 4750
Wire Wire Line
	3100 4850 3150 4850
Wire Wire Line
	3100 4950 3150 4950
Wire Wire Line
	3100 5050 3150 5050
Wire Wire Line
	3100 5150 3150 5150
Wire Wire Line
	3100 5250 3150 5250
Wire Wire Line
	3100 5350 3150 5350
Entry Wire Line
	3000 5350 3100 5450
Wire Wire Line
	3100 5450 3150 5450
Text HLabel 2950 4400 0    60   Output ~ 12
CMDin_P[0..9]
Wire Bus Line
	4500 4400 4450 4400
Entry Wire Line
	4500 4450 4600 4550
Entry Wire Line
	4500 4550 4600 4650
Entry Wire Line
	4500 4650 4600 4750
Entry Wire Line
	4500 4750 4600 4850
Entry Wire Line
	4500 4850 4600 4950
Entry Wire Line
	4500 4950 4600 5050
Entry Wire Line
	4500 5050 4600 5150
Entry Wire Line
	4500 5150 4600 5250
Entry Wire Line
	4500 5250 4600 5350
Wire Wire Line
	4600 4550 4650 4550
Wire Wire Line
	4600 4650 4650 4650
Wire Wire Line
	4600 4750 4650 4750
Wire Wire Line
	4600 4850 4650 4850
Wire Wire Line
	4600 4950 4650 4950
Wire Wire Line
	4600 5050 4650 5050
Wire Wire Line
	4600 5150 4650 5150
Wire Wire Line
	4600 5250 4650 5250
Wire Wire Line
	4600 5350 4650 5350
Entry Wire Line
	4500 5350 4600 5450
Wire Wire Line
	4600 5450 4650 5450
Text HLabel 4450 4400 0    60   Output ~ 12
CMDin_N[0..9]
Wire Bus Line
	3000 5550 2950 5550
Entry Wire Line
	3000 5600 3100 5700
Entry Wire Line
	3000 5700 3100 5800
Entry Wire Line
	3000 5800 3100 5900
Entry Wire Line
	3000 5900 3100 6000
Entry Wire Line
	3000 6000 3100 6100
Entry Wire Line
	3000 6100 3100 6200
Entry Wire Line
	3000 6200 3100 6300
Entry Wire Line
	3000 6300 3100 6400
Entry Wire Line
	3000 6400 3100 6500
Wire Wire Line
	3100 5700 3150 5700
Wire Wire Line
	3100 5800 3150 5800
Wire Wire Line
	3100 5900 3150 5900
Wire Wire Line
	3100 6000 3150 6000
Wire Wire Line
	3100 6100 3150 6100
Wire Wire Line
	3100 6200 3150 6200
Wire Wire Line
	3100 6300 3150 6300
Wire Wire Line
	3100 6400 3150 6400
Wire Wire Line
	3100 6500 3150 6500
Entry Wire Line
	3000 6500 3100 6600
Wire Wire Line
	3100 6600 3150 6600
Text HLabel 2950 5550 0    60   Output ~ 12
CMDout_P[0..9]
Wire Bus Line
	4500 5550 4450 5550
Entry Wire Line
	4500 5600 4600 5700
Entry Wire Line
	4500 5700 4600 5800
Entry Wire Line
	4500 5800 4600 5900
Entry Wire Line
	4500 5900 4600 6000
Entry Wire Line
	4500 6000 4600 6100
Entry Wire Line
	4500 6100 4600 6200
Entry Wire Line
	4500 6200 4600 6300
Entry Wire Line
	4500 6300 4600 6400
Entry Wire Line
	4500 6400 4600 6500
Wire Wire Line
	4600 5700 4650 5700
Wire Wire Line
	4600 5800 4650 5800
Wire Wire Line
	4600 5900 4650 5900
Wire Wire Line
	4600 6000 4650 6000
Wire Wire Line
	4600 6100 4650 6100
Wire Wire Line
	4600 6200 4650 6200
Wire Wire Line
	4600 6300 4650 6300
Wire Wire Line
	4600 6400 4650 6400
Wire Wire Line
	4600 6500 4650 6500
Entry Wire Line
	4500 6500 4600 6600
Wire Wire Line
	4600 6600 4650 6600
Text HLabel 4450 5550 0    60   Output ~ 12
CMDout_N[0..9]
Text Label 3150 4550 0    60   ~ 12
CMDin_P0
Text Label 3150 4650 0    60   ~ 12
CMDin_P1
Text Label 3150 4750 0    60   ~ 12
CMDin_P2
Text Label 3150 4850 0    60   ~ 12
CMDin_P3
Text Label 3150 4950 0    60   ~ 12
CMDin_P4
Text Label 3150 5050 0    60   ~ 12
CMDin_P5
Text Label 3150 5150 0    60   ~ 12
CMDin_P6
Text Label 3150 5250 0    60   ~ 12
CMDin_P7
Text Label 3150 5350 0    60   ~ 12
CMDin_P8
Text Label 3150 5450 0    60   ~ 12
CMDin_P9
Text Label 4650 4550 0    60   ~ 12
CMDin_N0
Text Label 4650 4650 0    60   ~ 12
CMDin_N1
Text Label 4650 4750 0    60   ~ 12
CMDin_N2
Text Label 4650 4850 0    60   ~ 12
CMDin_N3
Text Label 4650 4950 0    60   ~ 12
CMDin_N4
Text Label 4650 5050 0    60   ~ 12
CMDin_N5
Text Label 4650 5150 0    60   ~ 12
CMDin_N6
Text Label 4650 5250 0    60   ~ 12
CMDin_N7
Text Label 4650 5350 0    60   ~ 12
CMDin_N8
Text Label 4650 5450 0    60   ~ 12
CMDin_N9
Text Label 4650 5700 0    60   ~ 12
CMDout_N0
Text Label 4650 5800 0    60   ~ 12
CMDout_N1
Text Label 4650 5900 0    60   ~ 12
CMDout_N2
Text Label 4650 6000 0    60   ~ 12
CMDout_N3
Text Label 4650 6100 0    60   ~ 12
CMDout_N4
Text Label 4650 6200 0    60   ~ 12
CMDout_N5
Text Label 4650 6300 0    60   ~ 12
CMDout_N6
Text Label 4650 6400 0    60   ~ 12
CMDout_N7
Text Label 4650 6500 0    60   ~ 12
CMDout_N8
Text Label 4650 6600 0    60   ~ 12
CMDout_N9
$Comp
L Device:R R2001
U 1 1 5D39C5B7
P 1350 1450
F 0 "R2001" H 1420 1496 50  0000 L CNN
F 1 "100" H 1420 1405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1280 1450 50  0001 C CNN
F 3 "~" H 1350 1450 50  0001 C CNN
	1    1350 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 1300 1350 1300
Connection ~ 1350 1300
Wire Wire Line
	1350 1300 1450 1300
Wire Wire Line
	1250 1600 1350 1600
Connection ~ 1350 1600
Wire Wire Line
	1350 1600 1450 1600
Text Label 1250 1300 2    60   ~ 12
CMDin_P0
Text Label 1250 1600 2    60   ~ 12
CMDin_N0
$Comp
L Device:R R2002
U 1 1 5D3CFEA6
P 1350 1900
F 0 "R2002" H 1420 1946 50  0000 L CNN
F 1 "100" H 1420 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1280 1900 50  0001 C CNN
F 3 "~" H 1350 1900 50  0001 C CNN
	1    1350 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 1750 1350 1750
Connection ~ 1350 1750
Wire Wire Line
	1350 1750 1450 1750
Wire Wire Line
	1250 2050 1350 2050
Connection ~ 1350 2050
Wire Wire Line
	1350 2050 1450 2050
Text Label 1250 1750 2    60   ~ 12
CMDin_P5
Text Label 1250 2050 2    60   ~ 12
CMDin_N5
$Comp
L Device:R R2005
U 1 1 5D3DE03F
P 2150 1450
F 0 "R2005" H 2220 1496 50  0000 L CNN
F 1 "100" H 2220 1405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2080 1450 50  0001 C CNN
F 3 "~" H 2150 1450 50  0001 C CNN
	1    2150 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 1300 2150 1300
Connection ~ 2150 1300
Wire Wire Line
	2150 1300 2250 1300
Wire Wire Line
	2050 1600 2150 1600
Connection ~ 2150 1600
Wire Wire Line
	2150 1600 2250 1600
Text Label 2050 1300 2    60   ~ 12
CMDin_P1
Text Label 2050 1600 2    60   ~ 12
CMDin_N1
$Comp
L Device:R R2006
U 1 1 5D3DE051
P 2150 1900
F 0 "R2006" H 2220 1946 50  0000 L CNN
F 1 "100" H 2220 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2080 1900 50  0001 C CNN
F 3 "~" H 2150 1900 50  0001 C CNN
	1    2150 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 1750 2150 1750
Connection ~ 2150 1750
Wire Wire Line
	2150 1750 2250 1750
Wire Wire Line
	2050 2050 2150 2050
Connection ~ 2150 2050
Wire Wire Line
	2150 2050 2250 2050
Text Label 2050 1750 2    60   ~ 12
CMDin_P6
Text Label 2050 2050 2    60   ~ 12
CMDin_N6
$Comp
L Device:R R2009
U 1 1 5D3EDCC9
P 2950 1450
F 0 "R2009" H 3020 1496 50  0000 L CNN
F 1 "100" H 3020 1405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2880 1450 50  0001 C CNN
F 3 "~" H 2950 1450 50  0001 C CNN
	1    2950 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 1300 2950 1300
Connection ~ 2950 1300
Wire Wire Line
	2950 1300 3050 1300
Wire Wire Line
	2850 1600 2950 1600
Connection ~ 2950 1600
Wire Wire Line
	2950 1600 3050 1600
Text Label 2850 1300 2    60   ~ 12
CMDin_P2
Text Label 2850 1600 2    60   ~ 12
CMDin_N2
$Comp
L Device:R R2010
U 1 1 5D3EDCDB
P 2950 1900
F 0 "R2010" H 3020 1946 50  0000 L CNN
F 1 "100" H 3020 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2880 1900 50  0001 C CNN
F 3 "~" H 2950 1900 50  0001 C CNN
	1    2950 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 1750 2950 1750
Connection ~ 2950 1750
Wire Wire Line
	2950 1750 3050 1750
Wire Wire Line
	2850 2050 2950 2050
Connection ~ 2950 2050
Wire Wire Line
	2950 2050 3050 2050
Text Label 2850 1750 2    60   ~ 12
CMDin_P7
Text Label 2850 2050 2    60   ~ 12
CMDin_N7
$Comp
L Device:R R2013
U 1 1 5D3EDCED
P 3750 1450
F 0 "R2013" H 3820 1496 50  0000 L CNN
F 1 "100" H 3820 1405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3680 1450 50  0001 C CNN
F 3 "~" H 3750 1450 50  0001 C CNN
	1    3750 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 1300 3750 1300
Connection ~ 3750 1300
Wire Wire Line
	3750 1300 3850 1300
Wire Wire Line
	3650 1600 3750 1600
Connection ~ 3750 1600
Wire Wire Line
	3750 1600 3850 1600
Text Label 3650 1300 2    60   ~ 12
CMDin_P3
Text Label 3650 1600 2    60   ~ 12
CMDin_N3
$Comp
L Device:R R2014
U 1 1 5D3EDCFF
P 3750 1900
F 0 "R2014" H 3820 1946 50  0000 L CNN
F 1 "100" H 3820 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3680 1900 50  0001 C CNN
F 3 "~" H 3750 1900 50  0001 C CNN
	1    3750 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 1750 3750 1750
Connection ~ 3750 1750
Wire Wire Line
	3750 1750 3850 1750
Wire Wire Line
	3650 2050 3750 2050
Connection ~ 3750 2050
Wire Wire Line
	3750 2050 3850 2050
Text Label 3650 1750 2    60   ~ 12
CMDin_P8
Text Label 3650 2050 2    60   ~ 12
CMDin_N8
$Comp
L Device:R R2017
U 1 1 5D402E31
P 4550 1450
F 0 "R2017" H 4620 1496 50  0000 L CNN
F 1 "100" H 4620 1405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4480 1450 50  0001 C CNN
F 3 "~" H 4550 1450 50  0001 C CNN
	1    4550 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 1300 4550 1300
Connection ~ 4550 1300
Wire Wire Line
	4550 1300 4650 1300
Wire Wire Line
	4450 1600 4550 1600
Connection ~ 4550 1600
Wire Wire Line
	4550 1600 4650 1600
Text Label 4450 1300 2    60   ~ 12
CMDin_P4
Text Label 4450 1600 2    60   ~ 12
CMDin_N4
$Comp
L Device:R R2018
U 1 1 5D402E43
P 4550 1900
F 0 "R2018" H 4620 1946 50  0000 L CNN
F 1 "100" H 4620 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4480 1900 50  0001 C CNN
F 3 "~" H 4550 1900 50  0001 C CNN
	1    4550 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 1750 4550 1750
Connection ~ 4550 1750
Wire Wire Line
	4550 1750 4650 1750
Wire Wire Line
	4450 2050 4550 2050
Connection ~ 4550 2050
Wire Wire Line
	4550 2050 4650 2050
Text Label 4450 1750 2    60   ~ 12
CMDin_P9
Text Label 4450 2050 2    60   ~ 12
CMDin_N9
Text HLabel 950  650  0    60   Input ~ 12
VIN
Text HLabel 950  750  0    60   Output ~ 12
VIN_RTN
Text Label 3150 5700 0    60   ~ 12
CMDout_P0
Text Label 3150 5800 0    60   ~ 12
CMDout_P1
Text Label 3150 5900 0    60   ~ 12
CMDout_P2
Text Label 3150 6000 0    60   ~ 12
CMDout_P3
Text Label 3150 6100 0    60   ~ 12
CMDout_P4
Text Label 3150 6200 0    60   ~ 12
CMDout_P5
Text Label 3150 6300 0    60   ~ 12
CMDout_P6
Text Label 3150 6400 0    60   ~ 12
CMDout_P7
Text Label 3150 6500 0    60   ~ 12
CMDout_P8
Text Label 3150 6600 0    60   ~ 12
CMDout_P9
$Comp
L Device:R R2003
U 1 1 5D0FE2DC
P 1350 2450
F 0 "R2003" H 1420 2496 50  0000 L CNN
F 1 "100" H 1420 2405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1280 2450 50  0001 C CNN
F 3 "~" H 1350 2450 50  0001 C CNN
	1    1350 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 2300 1350 2300
Connection ~ 1350 2300
Wire Wire Line
	1350 2300 1450 2300
Wire Wire Line
	1250 2600 1350 2600
Connection ~ 1350 2600
Wire Wire Line
	1350 2600 1450 2600
Text Label 1250 2300 2    60   ~ 12
CMDout_P0
$Comp
L Device:R R2004
U 1 1 5D0FE2EE
P 1350 2900
F 0 "R2004" H 1420 2946 50  0000 L CNN
F 1 "100" H 1420 2855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1280 2900 50  0001 C CNN
F 3 "~" H 1350 2900 50  0001 C CNN
	1    1350 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 2750 1350 2750
Connection ~ 1350 2750
Wire Wire Line
	1350 2750 1450 2750
Wire Wire Line
	1250 3050 1350 3050
Connection ~ 1350 3050
Wire Wire Line
	1350 3050 1450 3050
$Comp
L Device:R R2007
U 1 1 5D0FE300
P 2150 2450
F 0 "R2007" H 2220 2496 50  0000 L CNN
F 1 "100" H 2220 2405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2080 2450 50  0001 C CNN
F 3 "~" H 2150 2450 50  0001 C CNN
	1    2150 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 2300 2150 2300
Connection ~ 2150 2300
Wire Wire Line
	2150 2300 2250 2300
Wire Wire Line
	2050 2600 2150 2600
Connection ~ 2150 2600
Wire Wire Line
	2150 2600 2250 2600
Text Label 2050 2300 2    60   ~ 12
CMDout_P1
$Comp
L Device:R R2008
U 1 1 5D0FE312
P 2150 2900
F 0 "R2008" H 2220 2946 50  0000 L CNN
F 1 "100" H 2220 2855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2080 2900 50  0001 C CNN
F 3 "~" H 2150 2900 50  0001 C CNN
	1    2150 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 2750 2150 2750
Connection ~ 2150 2750
Wire Wire Line
	2150 2750 2250 2750
Wire Wire Line
	2050 3050 2150 3050
Connection ~ 2150 3050
Wire Wire Line
	2150 3050 2250 3050
$Comp
L Device:R R2011
U 1 1 5D0FE324
P 2950 2450
F 0 "R2011" H 3020 2496 50  0000 L CNN
F 1 "100" H 3020 2405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2880 2450 50  0001 C CNN
F 3 "~" H 2950 2450 50  0001 C CNN
	1    2950 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 2300 2950 2300
Connection ~ 2950 2300
Wire Wire Line
	2950 2300 3050 2300
Wire Wire Line
	2850 2600 2950 2600
Connection ~ 2950 2600
Wire Wire Line
	2950 2600 3050 2600
$Comp
L Device:R R2012
U 1 1 5D0FE336
P 2950 2900
F 0 "R2012" H 3020 2946 50  0000 L CNN
F 1 "100" H 3020 2855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2880 2900 50  0001 C CNN
F 3 "~" H 2950 2900 50  0001 C CNN
	1    2950 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 2750 2950 2750
Connection ~ 2950 2750
Wire Wire Line
	2950 2750 3050 2750
Wire Wire Line
	2850 3050 2950 3050
Connection ~ 2950 3050
Wire Wire Line
	2950 3050 3050 3050
$Comp
L Device:R R2015
U 1 1 5D0FE348
P 3750 2450
F 0 "R2015" H 3820 2496 50  0000 L CNN
F 1 "100" H 3820 2405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3680 2450 50  0001 C CNN
F 3 "~" H 3750 2450 50  0001 C CNN
	1    3750 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2300 3750 2300
Connection ~ 3750 2300
Wire Wire Line
	3750 2300 3850 2300
Wire Wire Line
	3650 2600 3750 2600
Connection ~ 3750 2600
Wire Wire Line
	3750 2600 3850 2600
$Comp
L Device:R R2016
U 1 1 5D0FE35A
P 3750 2900
F 0 "R2016" H 3820 2946 50  0000 L CNN
F 1 "100" H 3820 2855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3680 2900 50  0001 C CNN
F 3 "~" H 3750 2900 50  0001 C CNN
	1    3750 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2750 3750 2750
Connection ~ 3750 2750
Wire Wire Line
	3750 2750 3850 2750
Wire Wire Line
	3650 3050 3750 3050
Connection ~ 3750 3050
Wire Wire Line
	3750 3050 3850 3050
$Comp
L Device:R R2019
U 1 1 5D0FE36C
P 4550 2450
F 0 "R2019" H 4620 2496 50  0000 L CNN
F 1 "100" H 4620 2405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4480 2450 50  0001 C CNN
F 3 "~" H 4550 2450 50  0001 C CNN
	1    4550 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 2300 4550 2300
Connection ~ 4550 2300
Wire Wire Line
	4550 2300 4650 2300
Wire Wire Line
	4450 2600 4550 2600
Connection ~ 4550 2600
Wire Wire Line
	4550 2600 4650 2600
$Comp
L Device:R R2020
U 1 1 5D0FE37E
P 4550 2900
F 0 "R2020" H 4620 2946 50  0000 L CNN
F 1 "100" H 4620 2855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4480 2900 50  0001 C CNN
F 3 "~" H 4550 2900 50  0001 C CNN
	1    4550 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 2750 4550 2750
Connection ~ 4550 2750
Wire Wire Line
	4550 2750 4650 2750
Wire Wire Line
	4450 3050 4550 3050
Connection ~ 4550 3050
Wire Wire Line
	4550 3050 4650 3050
Text Label 2850 2300 2    60   ~ 12
CMDout_P2
Text Label 3650 2300 2    60   ~ 12
CMDout_P3
Text Label 4450 2300 2    60   ~ 12
CMDout_P4
Text Label 1250 2750 2    60   ~ 12
CMDout_P5
Text Label 2050 2750 2    60   ~ 12
CMDout_P6
Text Label 2850 2750 2    60   ~ 12
CMDout_P7
Text Label 3650 2750 2    60   ~ 12
CMDout_P8
Text Label 4450 2750 2    60   ~ 12
CMDout_P9
Text Label 1250 2600 2    60   ~ 12
CMDout_N0
Text Label 2050 2600 2    60   ~ 12
CMDout_N1
Text Label 2850 2600 2    60   ~ 12
CMDout_N2
Text Label 3650 2600 2    60   ~ 12
CMDout_N3
Text Label 4450 2600 2    60   ~ 12
CMDout_N4
Text Label 1250 3050 2    60   ~ 12
CMDout_N5
Text Label 2050 3050 2    60   ~ 12
CMDout_N6
Text Label 2850 3050 2    60   ~ 12
CMDout_N7
Text Label 3650 3050 2    60   ~ 12
CMDout_N8
Text Label 4450 3050 2    60   ~ 12
CMDout_N9
$Comp
L power:GND #PWR02002
U 1 1 5D437E95
P 9150 4800
F 0 "#PWR02002" H 9150 4550 50  0001 C CNN
F 1 "GND" H 9155 4627 50  0000 C CNN
F 2 "" H 9150 4800 50  0001 C CNN
F 3 "" H 9150 4800 50  0001 C CNN
	1    9150 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 4800 9400 4800
Wire Bus Line
	1400 6700 1400 7650
Wire Bus Line
	1400 5550 1400 6500
Wire Bus Line
	1400 4400 1400 5350
Wire Bus Line
	1400 3250 1400 4200
Wire Bus Line
	3000 3250 3000 4200
Wire Bus Line
	4500 3250 4500 4200
Wire Bus Line
	3000 4400 3000 5350
Wire Bus Line
	4500 4400 4500 5350
Wire Bus Line
	3000 5550 3000 6500
Wire Bus Line
	4500 5550 4500 6500
$EndSCHEMATC
