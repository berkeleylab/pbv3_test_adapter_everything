EESchema Schematic File Version 4
LIBS:pbv3_mass_test_adapter_active-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 26
Title "Powerboard v3 Massive Active Tester Board"
Date "2019-06-04"
Rev ""
Comp "Lawrence Berkeley National Laboratory"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:LM324 U301
U 1 1 5B3D2EA1
P 6550 4150
AR Path="/59C5F008/59CBA3F1/5B3D2EA1" Ref="U301"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3D2EA1" Ref="U401"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3D2EA1" Ref="U501"  Part="1" 
F 0 "U301" H 6550 4350 50  0000 L CNN
F 1 "TLV274" H 6550 3950 50  0000 L CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 6500 4250 50  0001 C CNN
F 3 "" H 6600 4350 50  0001 C CNN
	1    6550 4150
	1    0    0    -1  
$EndComp
Entry Wire Line
	1050 1950 1150 2050
Entry Wire Line
	1150 1950 1250 2050
Entry Wire Line
	1250 1950 1350 2050
Entry Wire Line
	1350 1950 1450 2050
Text HLabel 1550 2050 2    60   Input ~ 12
VOUT[0..3]
Text Label 1050 1900 1    60   ~ 12
VOUT0
Text Label 1150 1900 1    60   ~ 12
VOUT1
Text Label 1250 1900 1    60   ~ 12
VOUT2
Text Label 1350 1900 1    60   ~ 12
VOUT3
Entry Wire Line
	1050 1400 1150 1500
Entry Wire Line
	1150 1400 1250 1500
Entry Wire Line
	1250 1400 1350 1500
Entry Wire Line
	1350 1400 1450 1500
Text Label 1050 1400 1    60   ~ 12
VDAC0
Text Label 1150 1400 1    60   ~ 12
VDAC1
Text Label 1250 1400 1    60   ~ 12
VDAC2
Text Label 1350 1400 1    60   ~ 12
VDAC3
Text HLabel 1550 1500 2    60   Input ~ 12
VDAC[0..3]
Text Label 8400 3500 2    60   ~ 12
VDAC3
$Comp
L Device:R R321
U 1 1 5A1CE3CC
P 8500 4350
AR Path="/59C5F008/59CBA3F1/5A1CE3CC" Ref="R321"  Part="1" 
AR Path="/59C5F008/59CC6E78/5A1CE3CC" Ref="R421"  Part="1" 
AR Path="/59C5F008/5CF391C7/5A1CE3CC" Ref="R521"  Part="1" 
F 0 "R321" V 8580 4350 50  0000 C CNN
F 1 "5k" V 8500 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8430 4350 50  0001 C CNN
F 3 "" H 8500 4350 50  0001 C CNN
F 4 "CRCW08055K00FKTA" V 8500 4350 50  0001 C CNN "mfg#"
	1    8500 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R320
U 1 1 5A1CE3D2
P 8500 3750
AR Path="/59C5F008/59CBA3F1/5A1CE3D2" Ref="R320"  Part="1" 
AR Path="/59C5F008/59CC6E78/5A1CE3D2" Ref="R420"  Part="1" 
AR Path="/59C5F008/5CF391C7/5A1CE3D2" Ref="R520"  Part="1" 
F 0 "R320" V 8580 3750 50  0000 C CNN
F 1 "100k" V 8500 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8430 3750 50  0001 C CNN
F 3 "" H 8500 3750 50  0001 C CNN
	1    8500 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0319
U 1 1 5A1CE3D8
P 8500 5000
AR Path="/59C5F008/59CBA3F1/5A1CE3D8" Ref="#PWR0319"  Part="1" 
AR Path="/59C5F008/59CC6E78/5A1CE3D8" Ref="#PWR0419"  Part="1" 
AR Path="/59C5F008/5CF391C7/5A1CE3D8" Ref="#PWR0519"  Part="1" 
F 0 "#PWR0519" H 8500 4750 50  0001 C CNN
F 1 "GND" H 8500 4850 50  0000 C CNN
F 2 "" H 8500 5000 50  0001 C CNN
F 3 "" H 8500 5000 50  0001 C CNN
	1    8500 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R324
U 1 1 5A1CE3DE
P 10200 4750
AR Path="/59C5F008/59CBA3F1/5A1CE3DE" Ref="R324"  Part="1" 
AR Path="/59C5F008/59CC6E78/5A1CE3DE" Ref="R424"  Part="1" 
AR Path="/59C5F008/5CF391C7/5A1CE3DE" Ref="R524"  Part="1" 
F 0 "R324" V 10280 4750 50  0000 C CNN
F 1 "25m" V 10200 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_2512_6332Metric" V 10130 4750 50  0001 C CNN
F 3 "" H 10200 4750 50  0001 C CNN
F 4 "CSR2512C0R025F" V 10200 4750 50  0001 C CNN "mfg#"
	1    10200 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0320
U 1 1 5A1CE3E4
P 10200 5000
AR Path="/59C5F008/59CBA3F1/5A1CE3E4" Ref="#PWR0320"  Part="1" 
AR Path="/59C5F008/59CC6E78/5A1CE3E4" Ref="#PWR0420"  Part="1" 
AR Path="/59C5F008/5CF391C7/5A1CE3E4" Ref="#PWR0520"  Part="1" 
F 0 "#PWR0520" H 10200 4750 50  0001 C CNN
F 1 "GND" H 10200 4850 50  0000 C CNN
F 2 "" H 10200 5000 50  0001 C CNN
F 3 "" H 10200 5000 50  0001 C CNN
	1    10200 5000
	1    0    0    -1  
$EndComp
Text Label 10300 3450 0    60   ~ 12
VOUT3
$Comp
L Amplifier_Operational:LM324 U301
U 3 1 59C5D05A
P 4050 4150
AR Path="/59C5F008/59CBA3F1/59C5D05A" Ref="U301"  Part="3" 
AR Path="/59C5F008/59CC6E78/59C5D05A" Ref="U401"  Part="3" 
AR Path="/59C5F008/5CF391C7/59C5D05A" Ref="U501"  Part="3" 
F 0 "U301" H 4050 4350 50  0000 L CNN
F 1 "TLV274" H 4050 3950 50  0000 L CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 4000 4250 50  0001 C CNN
F 3 "" H 4100 4350 50  0001 C CNN
F 4 "TLV274CPWR" H 4050 4150 50  0001 C CNN "mfg#"
	3    4050 4150
	1    0    0    -1  
$EndComp
Text Label 900  3500 2    60   ~ 12
VDAC0
$Comp
L Device:R R302
U 1 1 5B3D2EA3
P 1000 4350
AR Path="/59C5F008/59CBA3F1/5B3D2EA3" Ref="R302"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3D2EA3" Ref="R402"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3D2EA3" Ref="R502"  Part="1" 
F 0 "R302" V 1080 4350 50  0000 C CNN
F 1 "5k" V 1000 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 930 4350 50  0001 C CNN
F 3 "" H 1000 4350 50  0001 C CNN
	1    1000 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R301
U 1 1 5B3D2EA4
P 1000 3750
AR Path="/59C5F008/59CBA3F1/5B3D2EA4" Ref="R301"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3D2EA4" Ref="R401"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3D2EA4" Ref="R501"  Part="1" 
F 0 "R301" V 1080 3750 50  0000 C CNN
F 1 "100k" V 1000 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 930 3750 50  0001 C CNN
F 3 "" H 1000 3750 50  0001 C CNN
	1    1000 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0301
U 1 1 5B3D2EA5
P 1000 5000
AR Path="/59C5F008/59CBA3F1/5B3D2EA5" Ref="#PWR0301"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3D2EA5" Ref="#PWR0401"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3D2EA5" Ref="#PWR0501"  Part="1" 
F 0 "#PWR0501" H 1000 4750 50  0001 C CNN
F 1 "GND" H 1000 4850 50  0000 C CNN
F 2 "" H 1000 5000 50  0001 C CNN
F 3 "" H 1000 5000 50  0001 C CNN
	1    1000 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R307
U 1 1 5B3D2EA6
P 2700 4750
AR Path="/59C5F008/59CBA3F1/5B3D2EA6" Ref="R307"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3D2EA6" Ref="R407"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3D2EA6" Ref="R507"  Part="1" 
F 0 "R307" V 2780 4750 50  0000 C CNN
F 1 "25m" V 2700 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_2512_6332Metric" V 2630 4750 50  0001 C CNN
F 3 "" H 2700 4750 50  0001 C CNN
	1    2700 4750
	1    0    0    -1  
$EndComp
Text Label 2800 3450 0    60   ~ 12
VOUT0
$Comp
L power:GND #PWR0310
U 1 1 5A1CE8B0
P 3400 1200
AR Path="/59C5F008/59CBA3F1/5A1CE8B0" Ref="#PWR0310"  Part="1" 
AR Path="/59C5F008/59CC6E78/5A1CE8B0" Ref="#PWR0410"  Part="1" 
AR Path="/59C5F008/5CF391C7/5A1CE8B0" Ref="#PWR0510"  Part="1" 
F 0 "#PWR0510" H 3400 950 50  0001 C CNN
F 1 "GND" H 3400 1050 50  0000 C CNN
F 2 "" H 3400 1200 50  0001 C CNN
F 3 "" H 3400 1200 50  0001 C CNN
	1    3400 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C302
U 1 1 5A1CE8B6
P 3150 1150
AR Path="/59C5F008/59CBA3F1/5A1CE8B6" Ref="C302"  Part="1" 
AR Path="/59C5F008/59CC6E78/5A1CE8B6" Ref="C402"  Part="1" 
AR Path="/59C5F008/5CF391C7/5A1CE8B6" Ref="C502"  Part="1" 
F 0 "C302" H 3175 1250 50  0000 L CNN
F 1 "0.1uF" H 3175 1050 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3188 1000 50  0001 C CNN
F 3 "" H 3150 1150 50  0001 C CNN
	1    3150 1150
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C303
U 1 1 5A1CE8BE
P 3150 2350
AR Path="/59C5F008/59CBA3F1/5A1CE8BE" Ref="C303"  Part="1" 
AR Path="/59C5F008/59CC6E78/5A1CE8BE" Ref="C403"  Part="1" 
AR Path="/59C5F008/5CF391C7/5A1CE8BE" Ref="C503"  Part="1" 
F 0 "C303" H 3175 2450 50  0000 L CNN
F 1 "0.1uF" H 3175 2250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3188 2200 50  0001 C CNN
F 3 "" H 3150 2350 50  0001 C CNN
	1    3150 2350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0309
U 1 1 5A1CE8C4
P 3350 2600
AR Path="/59C5F008/59CBA3F1/5A1CE8C4" Ref="#PWR0309"  Part="1" 
AR Path="/59C5F008/59CC6E78/5A1CE8C4" Ref="#PWR0409"  Part="1" 
AR Path="/59C5F008/5CF391C7/5A1CE8C4" Ref="#PWR0509"  Part="1" 
F 0 "#PWR0509" H 3350 2350 50  0001 C CNN
F 1 "GND" H 3350 2450 50  0000 C CNN
F 2 "" H 3350 2600 50  0001 C CNN
F 3 "" H 3350 2600 50  0001 C CNN
	1    3350 2600
	1    0    0    -1  
$EndComp
$Comp
L power:-5V #PWR0308
U 1 1 5A1CE8CC
P 2950 2600
AR Path="/59C5F008/59CBA3F1/5A1CE8CC" Ref="#PWR0308"  Part="1" 
AR Path="/59C5F008/59CC6E78/5A1CE8CC" Ref="#PWR0408"  Part="1" 
AR Path="/59C5F008/5CF391C7/5A1CE8CC" Ref="#PWR0508"  Part="1" 
F 0 "#PWR0508" H 2950 2700 50  0001 C CNN
F 1 "-5V" H 2950 2750 50  0000 C CNN
F 2 "" H 2950 2600 50  0001 C CNN
F 3 "" H 2950 2600 50  0001 C CNN
	1    2950 2600
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0307
U 1 1 5B3D2EAD
P 2950 1050
AR Path="/59C5F008/59CBA3F1/5B3D2EAD" Ref="#PWR0307"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3D2EAD" Ref="#PWR0407"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3D2EAD" Ref="#PWR0507"  Part="1" 
F 0 "#PWR0507" H 2950 900 50  0001 C CNN
F 1 "+5V" H 2950 1190 50  0000 C CNN
F 2 "" H 2950 1050 50  0001 C CNN
F 3 "" H 2950 1050 50  0001 C CNN
	1    2950 1050
	1    0    0    -1  
$EndComp
Text Label 1350 2600 1    60   ~ 12
VOUT_RTN3
Text Label 1250 2600 1    60   ~ 12
VOUT_RTN2
Text Label 1150 2600 1    60   ~ 12
VOUT_RTN1
Text Label 1050 2600 1    60   ~ 12
VOUT_RTN0
Text HLabel 1550 2750 2    60   Input ~ 12
VOUT_RTN[0..3]
Entry Wire Line
	1350 2650 1450 2750
Entry Wire Line
	1250 2650 1350 2750
Entry Wire Line
	1150 2650 1250 2750
Entry Wire Line
	1050 2650 1150 2750
Text Label 10350 4950 0    60   ~ 12
VOUT_RTN3
Text Label 2850 4950 0    60   ~ 12
VOUT_RTN0
$Comp
L Device:R R322
U 1 1 5B36304E
P 9650 4150
AR Path="/59C5F008/59CBA3F1/5B36304E" Ref="R322"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B36304E" Ref="R422"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B36304E" Ref="R522"  Part="1" 
F 0 "R322" V 9730 4150 50  0000 C CNN
F 1 "10k" V 9650 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9580 4150 50  0001 C CNN
F 3 "" H 9650 4150 50  0001 C CNN
	1    9650 4150
	0    1    1    0   
$EndComp
$Comp
L Device:R R323
U 1 1 5B3630DA
P 9650 4550
AR Path="/59C5F008/59CBA3F1/5B3630DA" Ref="R323"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3630DA" Ref="R423"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3630DA" Ref="R523"  Part="1" 
F 0 "R323" V 9730 4550 50  0000 C CNN
F 1 "10k" V 9650 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9580 4550 50  0001 C CNN
F 3 "" H 9650 4550 50  0001 C CNN
	1    9650 4550
	0    1    1    0   
$EndComp
$Comp
L Device:C C306
U 1 1 5B36315F
P 9450 4350
AR Path="/59C5F008/59CBA3F1/5B36315F" Ref="C306"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B36315F" Ref="C406"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B36315F" Ref="C506"  Part="1" 
F 0 "C306" H 9550 4350 50  0000 L CNN
F 1 "0.1uF" H 9475 4250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9488 4200 50  0001 C CNN
F 3 "" H 9450 4350 50  0001 C CNN
	1    9450 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 1950 1050 1900
Wire Wire Line
	1150 1900 1150 1950
Wire Wire Line
	1250 1900 1250 1950
Wire Wire Line
	1350 1900 1350 1950
Wire Wire Line
	8750 4050 8500 4050
Wire Wire Line
	8750 4250 8650 4250
Wire Wire Line
	8650 4250 8650 4550
Wire Wire Line
	8500 3900 8500 4050
Connection ~ 8500 4050
Wire Wire Line
	8500 3600 8500 3500
Wire Wire Line
	8500 3500 8400 3500
Wire Wire Line
	8500 4500 8500 5000
Wire Wire Line
	10200 4350 10200 4550
Wire Wire Line
	10200 3950 10200 3450
Wire Wire Line
	10200 3450 10300 3450
Connection ~ 10200 4550
Wire Wire Line
	10200 4900 10200 4950
Wire Wire Line
	1250 4050 1000 4050
Wire Wire Line
	1250 4250 1150 4250
Wire Wire Line
	1150 4250 1150 4550
Wire Wire Line
	1000 3900 1000 4050
Connection ~ 1000 4050
Wire Wire Line
	1000 3600 1000 3500
Wire Wire Line
	1000 3500 900  3500
Wire Wire Line
	1000 4500 1000 5000
Wire Wire Line
	2700 4350 2700 4550
Wire Wire Line
	2700 3950 2700 3450
Wire Wire Line
	2700 3450 2800 3450
Wire Wire Line
	2950 2050 2950 2350
Wire Wire Line
	2950 1050 2950 1150
Connection ~ 2700 4550
Wire Wire Line
	2700 4900 2700 4950
Wire Wire Line
	1850 4150 1950 4150
Wire Wire Line
	3300 1150 3400 1150
Wire Wire Line
	3400 1150 3400 1200
Wire Wire Line
	3300 2350 3350 2350
Wire Wire Line
	3350 2350 3350 2600
Wire Wire Line
	3000 2350 2950 2350
Connection ~ 2950 2350
Wire Wire Line
	3000 1150 2950 1150
Connection ~ 2950 1150
Wire Wire Line
	1350 2600 1350 2650
Wire Wire Line
	1250 2600 1250 2650
Wire Wire Line
	1150 2600 1150 2650
Wire Wire Line
	1050 2600 1050 2650
Wire Wire Line
	10200 4950 10350 4950
Wire Wire Line
	2700 4950 2850 4950
Connection ~ 2700 4950
Connection ~ 10200 4950
Wire Wire Line
	9350 4150 9450 4150
Wire Wire Line
	9800 4150 9900 4150
Wire Wire Line
	9450 4200 9450 4150
Connection ~ 9450 4150
Wire Wire Line
	9450 4500 9450 4550
Wire Wire Line
	8650 4550 9450 4550
Connection ~ 9450 4550
Wire Wire Line
	9800 4550 10200 4550
$Comp
L Device:R R304
U 1 1 5B364C05
P 2150 4150
AR Path="/59C5F008/59CBA3F1/5B364C05" Ref="R304"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B364C05" Ref="R404"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B364C05" Ref="R504"  Part="1" 
F 0 "R304" V 2230 4150 50  0000 C CNN
F 1 "10k" V 2150 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2080 4150 50  0001 C CNN
F 3 "" H 2150 4150 50  0001 C CNN
	1    2150 4150
	0    1    1    0   
$EndComp
$Comp
L Device:R R305
U 1 1 5B364C0B
P 2150 4550
AR Path="/59C5F008/59CBA3F1/5B364C0B" Ref="R305"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B364C0B" Ref="R405"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B364C0B" Ref="R505"  Part="1" 
F 0 "R305" V 2230 4550 50  0000 C CNN
F 1 "10k" V 2150 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2080 4550 50  0001 C CNN
F 3 "" H 2150 4550 50  0001 C CNN
	1    2150 4550
	0    1    1    0   
$EndComp
$Comp
L Device:C C301
U 1 1 5B364C11
P 1950 4350
AR Path="/59C5F008/59CBA3F1/5B364C11" Ref="C301"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B364C11" Ref="C401"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B364C11" Ref="C501"  Part="1" 
F 0 "C301" H 2050 4350 50  0000 L CNN
F 1 "0.1uF" H 1975 4250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1988 4200 50  0001 C CNN
F 3 "" H 1950 4350 50  0001 C CNN
	1    1950 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 4150 2400 4150
Wire Wire Line
	1950 4200 1950 4150
Connection ~ 1950 4150
Wire Wire Line
	1950 4500 1950 4550
Wire Wire Line
	1150 4550 1950 4550
Connection ~ 1950 4550
Wire Wire Line
	2300 4550 2700 4550
$Comp
L Amplifier_Operational:LM324 U301
U 2 1 5B3D2EB2
P 1550 4150
AR Path="/59C5F008/59CBA3F1/5B3D2EB2" Ref="U301"  Part="2" 
AR Path="/59C5F008/59CC6E78/5B3D2EB2" Ref="U401"  Part="2" 
AR Path="/59C5F008/5CF391C7/5B3D2EB2" Ref="U501"  Part="2" 
F 0 "U301" H 1550 4350 50  0000 L CNN
F 1 "TLV274" H 1550 3950 50  0000 L CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 1500 4250 50  0001 C CNN
F 3 "" H 1600 4350 50  0001 C CNN
	2    1550 4150
	1    0    0    -1  
$EndComp
Text Label 3400 3500 2    60   ~ 12
VDAC1
$Comp
L Device:R R309
U 1 1 5B366CAF
P 3500 4350
AR Path="/59C5F008/59CBA3F1/5B366CAF" Ref="R309"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B366CAF" Ref="R409"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B366CAF" Ref="R509"  Part="1" 
F 0 "R309" V 3580 4350 50  0000 C CNN
F 1 "5k" V 3500 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3430 4350 50  0001 C CNN
F 3 "" H 3500 4350 50  0001 C CNN
	1    3500 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R308
U 1 1 5B366CB5
P 3500 3750
AR Path="/59C5F008/59CBA3F1/5B366CB5" Ref="R308"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B366CB5" Ref="R408"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B366CB5" Ref="R508"  Part="1" 
F 0 "R308" V 3580 3750 50  0000 C CNN
F 1 "100k" V 3500 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3430 3750 50  0001 C CNN
F 3 "" H 3500 3750 50  0001 C CNN
	1    3500 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R314
U 1 1 5B366CC1
P 5200 4750
AR Path="/59C5F008/59CBA3F1/5B366CC1" Ref="R314"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B366CC1" Ref="R414"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B366CC1" Ref="R514"  Part="1" 
F 0 "R314" V 5280 4750 50  0000 C CNN
F 1 "25m" V 5200 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_2512_6332Metric" V 5130 4750 50  0001 C CNN
F 3 "" H 5200 4750 50  0001 C CNN
	1    5200 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0316
U 1 1 5B366CC7
P 5200 5000
AR Path="/59C5F008/59CBA3F1/5B366CC7" Ref="#PWR0316"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B366CC7" Ref="#PWR0416"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B366CC7" Ref="#PWR0516"  Part="1" 
F 0 "#PWR0516" H 5200 4750 50  0001 C CNN
F 1 "GND" H 5200 4850 50  0000 C CNN
F 2 "" H 5200 5000 50  0001 C CNN
F 3 "" H 5200 5000 50  0001 C CNN
	1    5200 5000
	1    0    0    -1  
$EndComp
Text Label 5300 3450 0    60   ~ 12
VOUT1
Text Label 5350 4950 0    60   ~ 12
VOUT_RTN1
Wire Wire Line
	3750 4050 3500 4050
Wire Wire Line
	3750 4250 3650 4250
Wire Wire Line
	3650 4250 3650 4550
Wire Wire Line
	3500 3900 3500 4050
Connection ~ 3500 4050
Wire Wire Line
	3500 3600 3500 3500
Wire Wire Line
	3500 3500 3400 3500
Wire Wire Line
	3500 4500 3500 5000
Wire Wire Line
	5200 4350 5200 4550
Wire Wire Line
	5200 3950 5200 3450
Wire Wire Line
	5200 3450 5300 3450
Connection ~ 5200 4550
Wire Wire Line
	5200 4900 5200 4950
Wire Wire Line
	4350 4150 4450 4150
Wire Wire Line
	5200 4950 5350 4950
Connection ~ 5200 4950
$Comp
L Device:R R311
U 1 1 5B366D15
P 4650 4150
AR Path="/59C5F008/59CBA3F1/5B366D15" Ref="R311"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B366D15" Ref="R411"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B366D15" Ref="R511"  Part="1" 
F 0 "R311" V 4730 4150 50  0000 C CNN
F 1 "10k" V 4650 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4580 4150 50  0001 C CNN
F 3 "" H 4650 4150 50  0001 C CNN
	1    4650 4150
	0    1    1    0   
$EndComp
$Comp
L Device:R R312
U 1 1 5B366D1B
P 4650 4550
AR Path="/59C5F008/59CBA3F1/5B366D1B" Ref="R312"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B366D1B" Ref="R412"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B366D1B" Ref="R512"  Part="1" 
F 0 "R312" V 4730 4550 50  0000 C CNN
F 1 "10k" V 4650 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4580 4550 50  0001 C CNN
F 3 "" H 4650 4550 50  0001 C CNN
	1    4650 4550
	0    1    1    0   
$EndComp
$Comp
L Device:C C304
U 1 1 5B366D21
P 4450 4350
AR Path="/59C5F008/59CBA3F1/5B366D21" Ref="C304"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B366D21" Ref="C404"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B366D21" Ref="C504"  Part="1" 
F 0 "C304" H 4550 4350 50  0000 L CNN
F 1 "0.1uF" H 4475 4250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4488 4200 50  0001 C CNN
F 3 "" H 4450 4350 50  0001 C CNN
	1    4450 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 4150 4900 4150
Wire Wire Line
	4450 4200 4450 4150
Connection ~ 4450 4150
Wire Wire Line
	4450 4500 4450 4550
Wire Wire Line
	3650 4550 4450 4550
Connection ~ 4450 4550
Wire Wire Line
	4800 4550 5200 4550
$Comp
L Amplifier_Operational:LM324 U301
U 4 1 5B3D2EB3
P 9050 4150
AR Path="/59C5F008/59CBA3F1/5B3D2EB3" Ref="U301"  Part="4" 
AR Path="/59C5F008/59CC6E78/5B3D2EB3" Ref="U401"  Part="4" 
AR Path="/59C5F008/5CF391C7/5B3D2EB3" Ref="U501"  Part="4" 
F 0 "U301" H 9050 4350 50  0000 L CNN
F 1 "TLV274" H 9050 3950 50  0000 L CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 9000 4250 50  0001 C CNN
F 3 "" H 9100 4350 50  0001 C CNN
	4    9050 4150
	1    0    0    -1  
$EndComp
Text Label 5900 3500 2    60   ~ 12
VDAC2
$Comp
L Device:R R316
U 1 1 5B3681F6
P 6000 4350
AR Path="/59C5F008/59CBA3F1/5B3681F6" Ref="R316"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3681F6" Ref="R416"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3681F6" Ref="R516"  Part="1" 
F 0 "R316" V 6080 4350 50  0000 C CNN
F 1 "5k" V 6000 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5930 4350 50  0001 C CNN
F 3 "" H 6000 4350 50  0001 C CNN
	1    6000 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R315
U 1 1 5B3681FC
P 6000 3750
AR Path="/59C5F008/59CBA3F1/5B3681FC" Ref="R315"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3681FC" Ref="R415"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3681FC" Ref="R515"  Part="1" 
F 0 "R315" V 6080 3750 50  0000 C CNN
F 1 "100k" V 6000 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5930 3750 50  0001 C CNN
F 3 "" H 6000 3750 50  0001 C CNN
	1    6000 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0317
U 1 1 5B368202
P 6000 5000
AR Path="/59C5F008/59CBA3F1/5B368202" Ref="#PWR0317"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B368202" Ref="#PWR0417"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B368202" Ref="#PWR0517"  Part="1" 
F 0 "#PWR0517" H 6000 4750 50  0001 C CNN
F 1 "GND" H 6000 4850 50  0000 C CNN
F 2 "" H 6000 5000 50  0001 C CNN
F 3 "" H 6000 5000 50  0001 C CNN
	1    6000 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R319
U 1 1 5B368208
P 7700 4750
AR Path="/59C5F008/59CBA3F1/5B368208" Ref="R319"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B368208" Ref="R419"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B368208" Ref="R519"  Part="1" 
F 0 "R319" V 7780 4750 50  0000 C CNN
F 1 "25m" V 7700 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_2512_6332Metric" V 7630 4750 50  0001 C CNN
F 3 "" H 7700 4750 50  0001 C CNN
	1    7700 4750
	1    0    0    -1  
$EndComp
Text Label 7800 3450 0    60   ~ 12
VOUT2
Text Label 7850 4950 0    60   ~ 12
VOUT_RTN2
Wire Wire Line
	6250 4050 6000 4050
Wire Wire Line
	6250 4250 6150 4250
Wire Wire Line
	6150 4250 6150 4550
Wire Wire Line
	6000 3900 6000 4050
Connection ~ 6000 4050
Wire Wire Line
	6000 3600 6000 3500
Wire Wire Line
	6000 3500 5900 3500
Wire Wire Line
	6000 4500 6000 5000
Wire Wire Line
	7700 4350 7700 4550
Wire Wire Line
	7700 3950 7700 3450
Wire Wire Line
	7700 3450 7800 3450
Connection ~ 7700 4550
Wire Wire Line
	7700 4900 7700 4950
Wire Wire Line
	6850 4150 6950 4150
Wire Wire Line
	7700 4950 7850 4950
Connection ~ 7700 4950
$Comp
L Device:R R317
U 1 1 5B36825B
P 7150 4150
AR Path="/59C5F008/59CBA3F1/5B36825B" Ref="R317"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B36825B" Ref="R417"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B36825B" Ref="R517"  Part="1" 
F 0 "R317" V 7230 4150 50  0000 C CNN
F 1 "10k" V 7150 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7080 4150 50  0001 C CNN
F 3 "" H 7150 4150 50  0001 C CNN
	1    7150 4150
	0    1    1    0   
$EndComp
$Comp
L Device:R R318
U 1 1 5B368261
P 7150 4550
AR Path="/59C5F008/59CBA3F1/5B368261" Ref="R318"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B368261" Ref="R418"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B368261" Ref="R518"  Part="1" 
F 0 "R318" V 7230 4550 50  0000 C CNN
F 1 "10k" V 7150 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7080 4550 50  0001 C CNN
F 3 "" H 7150 4550 50  0001 C CNN
	1    7150 4550
	0    1    1    0   
$EndComp
$Comp
L Device:C C305
U 1 1 5B368267
P 6950 4350
AR Path="/59C5F008/59CBA3F1/5B368267" Ref="C305"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B368267" Ref="C405"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B368267" Ref="C505"  Part="1" 
F 0 "C305" H 7050 4350 50  0000 L CNN
F 1 "0.1uF" H 6975 4250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6988 4200 50  0001 C CNN
F 3 "" H 6950 4350 50  0001 C CNN
	1    6950 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 4150 7400 4150
Wire Wire Line
	6950 4200 6950 4150
Connection ~ 6950 4150
Wire Wire Line
	6950 4500 6950 4550
Wire Wire Line
	6150 4550 6950 4550
Connection ~ 6950 4550
Wire Wire Line
	7300 4550 7700 4550
$Comp
L Device:Q_NMOS_GSD Q304
U 1 1 5B3F6EFA
P 3700 7150
AR Path="/59C5F008/59CBA3F1/5B3F6EFA" Ref="Q304"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3F6EFA" Ref="Q404"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3F6EFA" Ref="Q504"  Part="1" 
F 0 "Q304" H 3900 7200 50  0000 L CNN
F 1 "BSH105,215" H 3900 7100 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3900 7250 50  0001 C CNN
F 3 "" H 3700 7150 50  0001 C CNN
	1    3700 7150
	1    0    0    -1  
$EndComp
Text Label 3500 7150 2    60   ~ 12
VOUT2
Wire Wire Line
	3800 6150 3800 6050
Wire Wire Line
	3800 6450 3800 6550
$Comp
L Device:R R310
U 1 1 5B3F6F09
P 3800 6700
AR Path="/59C5F008/59CBA3F1/5B3F6F09" Ref="R310"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3F6F09" Ref="R410"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3F6F09" Ref="R510"  Part="1" 
F 0 "R310" V 3880 6700 50  0000 C CNN
F 1 "621" V 3800 6700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3730 6700 50  0001 C CNN
F 3 "" H 3800 6700 50  0001 C CNN
F 4 "CRCW0805620RFKEA" V 3800 6700 50  0001 C CNN "mfg#"
	1    3800 6700
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D303
U 1 1 5B3F6F0F
P 3800 6300
AR Path="/59C5F008/59CBA3F1/5B3F6F0F" Ref="D303"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3F6F0F" Ref="D403"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3F6F0F" Ref="D503"  Part="1" 
F 0 "D303" H 3800 6400 50  0000 C CNN
F 1 "LED" H 3800 6200 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3800 6300 50  0001 C CNN
F 3 "" H 3800 6300 50  0001 C CNN
	1    3800 6300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3800 6950 3800 6850
Wire Wire Line
	3800 7450 3800 7350
$Comp
L power:GND #PWR0313
U 1 1 5B3F6F17
P 3800 7450
AR Path="/59C5F008/59CBA3F1/5B3F6F17" Ref="#PWR0313"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3F6F17" Ref="#PWR0413"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3F6F17" Ref="#PWR0513"  Part="1" 
F 0 "#PWR0513" H 3800 7200 50  0001 C CNN
F 1 "GND" H 3800 7300 50  0000 C CNN
F 2 "" H 3800 7450 50  0001 C CNN
F 3 "" H 3800 7450 50  0001 C CNN
	1    3800 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GSD Q305
U 1 1 5B3F7852
P 4950 7150
AR Path="/59C5F008/59CBA3F1/5B3F7852" Ref="Q305"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3F7852" Ref="Q405"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3F7852" Ref="Q505"  Part="1" 
F 0 "Q305" H 5150 7200 50  0000 L CNN
F 1 "BSH105,215" H 5150 7100 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5150 7250 50  0001 C CNN
F 3 "" H 4950 7150 50  0001 C CNN
	1    4950 7150
	1    0    0    -1  
$EndComp
Text Label 4750 7150 2    60   ~ 12
VOUT3
Wire Wire Line
	5050 6150 5050 6050
Wire Wire Line
	5050 6450 5050 6550
$Comp
L Device:R R313
U 1 1 5B3F7861
P 5050 6700
AR Path="/59C5F008/59CBA3F1/5B3F7861" Ref="R313"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3F7861" Ref="R413"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3F7861" Ref="R513"  Part="1" 
F 0 "R313" V 5130 6700 50  0000 C CNN
F 1 "621" V 5050 6700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4980 6700 50  0001 C CNN
F 3 "" H 5050 6700 50  0001 C CNN
	1    5050 6700
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D304
U 1 1 5B3F7867
P 5050 6300
AR Path="/59C5F008/59CBA3F1/5B3F7867" Ref="D304"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3F7867" Ref="D404"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3F7867" Ref="D504"  Part="1" 
F 0 "D304" H 5050 6400 50  0000 C CNN
F 1 "LED" H 5050 6200 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5050 6300 50  0001 C CNN
F 3 "" H 5050 6300 50  0001 C CNN
	1    5050 6300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5050 6950 5050 6850
Wire Wire Line
	5050 7450 5050 7350
$Comp
L Device:Q_NMOS_GSD Q301
U 1 1 5B3F7875
P 1200 7150
AR Path="/59C5F008/59CBA3F1/5B3F7875" Ref="Q301"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3F7875" Ref="Q401"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3F7875" Ref="Q501"  Part="1" 
F 0 "Q301" H 1400 7200 50  0000 L CNN
F 1 "BSH105,215" H 1400 7100 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1400 7250 50  0001 C CNN
F 3 "" H 1200 7150 50  0001 C CNN
F 4 "BSH105\\,215" H 1200 7150 50  0001 C CNN "mfg#"
	1    1200 7150
	1    0    0    -1  
$EndComp
Text Label 1000 7150 2    60   ~ 12
VOUT0
Wire Wire Line
	1300 6150 1300 6050
Wire Wire Line
	1300 6450 1300 6550
$Comp
L Device:R R303
U 1 1 5B3F7884
P 1300 6700
AR Path="/59C5F008/59CBA3F1/5B3F7884" Ref="R303"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3F7884" Ref="R403"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3F7884" Ref="R503"  Part="1" 
F 0 "R303" V 1380 6700 50  0000 C CNN
F 1 "621" V 1300 6700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1230 6700 50  0001 C CNN
F 3 "" H 1300 6700 50  0001 C CNN
	1    1300 6700
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D301
U 1 1 5B3F788A
P 1300 6300
AR Path="/59C5F008/59CBA3F1/5B3F788A" Ref="D301"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3F788A" Ref="D401"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3F788A" Ref="D501"  Part="1" 
F 0 "D301" H 1300 6400 50  0000 C CNN
F 1 "LED" H 1300 6200 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1300 6300 50  0001 C CNN
F 3 "" H 1300 6300 50  0001 C CNN
F 4 "SML-LX1206IW-TR" H 1300 6300 50  0001 C CNN "mfg#"
	1    1300 6300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1300 6950 1300 6850
Wire Wire Line
	1300 7450 1300 7350
$Comp
L power:GND #PWR0303
U 1 1 5B3F7892
P 1300 7450
AR Path="/59C5F008/59CBA3F1/5B3F7892" Ref="#PWR0303"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3F7892" Ref="#PWR0403"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3F7892" Ref="#PWR0503"  Part="1" 
F 0 "#PWR0503" H 1300 7200 50  0001 C CNN
F 1 "GND" H 1300 7300 50  0000 C CNN
F 2 "" H 1300 7450 50  0001 C CNN
F 3 "" H 1300 7450 50  0001 C CNN
	1    1300 7450
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0312
U 1 1 5B4033C5
P 3800 6050
AR Path="/59C5F008/59CBA3F1/5B4033C5" Ref="#PWR0312"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B4033C5" Ref="#PWR0412"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B4033C5" Ref="#PWR0512"  Part="1" 
F 0 "#PWR0512" H 3800 5900 50  0001 C CNN
F 1 "+3V3" H 3800 6190 50  0000 C CNN
F 2 "" H 3800 6050 50  0001 C CNN
F 3 "" H 3800 6050 50  0001 C CNN
	1    3800 6050
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0314
U 1 1 5B403435
P 5050 6050
AR Path="/59C5F008/59CBA3F1/5B403435" Ref="#PWR0314"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B403435" Ref="#PWR0414"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B403435" Ref="#PWR0514"  Part="1" 
F 0 "#PWR0514" H 5050 5900 50  0001 C CNN
F 1 "+3V3" H 5050 6190 50  0000 C CNN
F 2 "" H 5050 6050 50  0001 C CNN
F 3 "" H 5050 6050 50  0001 C CNN
	1    5050 6050
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0302
U 1 1 5B4034A5
P 1300 6050
AR Path="/59C5F008/59CBA3F1/5B4034A5" Ref="#PWR0302"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B4034A5" Ref="#PWR0402"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B4034A5" Ref="#PWR0502"  Part="1" 
F 0 "#PWR0502" H 1300 5900 50  0001 C CNN
F 1 "+3V3" H 1300 6190 50  0000 C CNN
F 2 "" H 1300 6050 50  0001 C CNN
F 3 "" H 1300 6050 50  0001 C CNN
	1    1300 6050
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q303
U 1 1 5B413264
P 2600 4150
AR Path="/59C5F008/59CBA3F1/5B413264" Ref="Q303"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B413264" Ref="Q403"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B413264" Ref="Q503"  Part="1" 
F 0 "Q303" H 2800 4200 50  0000 L CNN
F 1 "NTD4858N" H 2800 4100 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 2800 4250 50  0001 C CNN
F 3 "" H 2600 4150 50  0001 C CNN
F 4 "NTD4858NT4G" H 2600 4150 50  0001 C CNN "mfg#"
	1    2600 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q306
U 1 1 5B4156AD
P 5100 4150
AR Path="/59C5F008/59CBA3F1/5B4156AD" Ref="Q306"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B4156AD" Ref="Q406"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B4156AD" Ref="Q506"  Part="1" 
F 0 "Q306" H 5300 4200 50  0000 L CNN
F 1 "NTD4858N" H 5300 4100 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 5300 4250 50  0001 C CNN
F 3 "" H 5100 4150 50  0001 C CNN
	1    5100 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q307
U 1 1 5B4157B9
P 7600 4150
AR Path="/59C5F008/59CBA3F1/5B4157B9" Ref="Q307"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B4157B9" Ref="Q407"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B4157B9" Ref="Q507"  Part="1" 
F 0 "Q307" H 7800 4200 50  0000 L CNN
F 1 "NTD4858N" H 7800 4100 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 7800 4250 50  0001 C CNN
F 3 "" H 7600 4150 50  0001 C CNN
	1    7600 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q308
U 1 1 5B415CA1
P 10100 4150
AR Path="/59C5F008/59CBA3F1/5B415CA1" Ref="Q308"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B415CA1" Ref="Q408"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B415CA1" Ref="Q508"  Part="1" 
F 0 "Q308" H 10300 4200 50  0000 L CNN
F 1 "NTD4858N" H 10300 4100 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 10300 4250 50  0001 C CNN
F 3 "" H 10100 4150 50  0001 C CNN
	1    10100 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0311
U 1 1 5B4091D8
P 3500 5000
AR Path="/59C5F008/59CBA3F1/5B4091D8" Ref="#PWR0311"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B4091D8" Ref="#PWR0411"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B4091D8" Ref="#PWR0511"  Part="1" 
F 0 "#PWR0511" H 3500 4750 50  0001 C CNN
F 1 "GND" H 3500 4850 50  0000 C CNN
F 2 "" H 3500 5000 50  0001 C CNN
F 3 "" H 3500 5000 50  0001 C CNN
	1    3500 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0306
U 1 1 5B40BCDE
P 2700 5000
AR Path="/59C5F008/59CBA3F1/5B40BCDE" Ref="#PWR0306"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B40BCDE" Ref="#PWR0406"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B40BCDE" Ref="#PWR0506"  Part="1" 
F 0 "#PWR0506" H 2700 4750 50  0001 C CNN
F 1 "GND" H 2700 4850 50  0000 C CNN
F 2 "" H 2700 5000 50  0001 C CNN
F 3 "" H 2700 5000 50  0001 C CNN
	1    2700 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0318
U 1 1 5B640D79
P 7700 5000
AR Path="/59C5F008/59CBA3F1/5B640D79" Ref="#PWR0318"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B640D79" Ref="#PWR0418"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B640D79" Ref="#PWR0518"  Part="1" 
F 0 "#PWR0518" H 7700 4750 50  0001 C CNN
F 1 "GND" H 7700 4850 50  0000 C CNN
F 2 "" H 7700 5000 50  0001 C CNN
F 3 "" H 7700 5000 50  0001 C CNN
	1    7700 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0315
U 1 1 5B64146D
P 5050 7450
AR Path="/59C5F008/59CBA3F1/5B64146D" Ref="#PWR0315"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B64146D" Ref="#PWR0415"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B64146D" Ref="#PWR0515"  Part="1" 
F 0 "#PWR0515" H 5050 7200 50  0001 C CNN
F 1 "GND" H 5050 7300 50  0000 C CNN
F 2 "" H 5050 7450 50  0001 C CNN
F 3 "" H 5050 7450 50  0001 C CNN
	1    5050 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 4050 8500 4200
Wire Wire Line
	10200 4550 10200 4600
Wire Wire Line
	1000 4050 1000 4200
Wire Wire Line
	2700 4550 2700 4600
Wire Wire Line
	2950 2350 2950 2600
Wire Wire Line
	2950 1150 2950 1450
Wire Wire Line
	2700 4950 2700 5000
Wire Wire Line
	10200 4950 10200 5000
Wire Wire Line
	9450 4150 9500 4150
Wire Wire Line
	9450 4550 9500 4550
Wire Wire Line
	1950 4150 2000 4150
Wire Wire Line
	1950 4550 2000 4550
Wire Wire Line
	3500 4050 3500 4200
Wire Wire Line
	5200 4550 5200 4600
Wire Wire Line
	5200 4950 5200 5000
Wire Wire Line
	4450 4150 4500 4150
Wire Wire Line
	4450 4550 4500 4550
Wire Wire Line
	6000 4050 6000 4200
Wire Wire Line
	7700 4550 7700 4600
Wire Wire Line
	7700 4950 7700 5000
Wire Wire Line
	6950 4150 7000 4150
Wire Wire Line
	6950 4550 7000 4550
$Comp
L Amplifier_Operational:LM324 U301
U 5 1 5CA57200
P 3050 1750
AR Path="/59C5F008/59CBA3F1/5CA57200" Ref="U301"  Part="5" 
AR Path="/59C5F008/59CC6E78/5CA57200" Ref="U401"  Part="5" 
AR Path="/59C5F008/5CF391C7/5CA57200" Ref="U501"  Part="5" 
F 0 "U301" H 3008 1796 50  0000 L CNN
F 1 "TLV274" H 3008 1705 50  0000 L CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 3000 1850 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 3100 1950 50  0001 C CNN
	5    3050 1750
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0304
U 1 1 5B403355
P 2550 6050
AR Path="/59C5F008/59CBA3F1/5B403355" Ref="#PWR0304"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B403355" Ref="#PWR0404"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B403355" Ref="#PWR0504"  Part="1" 
F 0 "#PWR0504" H 2550 5900 50  0001 C CNN
F 1 "+3V3" H 2550 6190 50  0000 C CNN
F 2 "" H 2550 6050 50  0001 C CNN
F 3 "" H 2550 6050 50  0001 C CNN
	1    2550 6050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0305
U 1 1 5B3F6983
P 2550 7450
AR Path="/59C5F008/59CBA3F1/5B3F6983" Ref="#PWR0305"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3F6983" Ref="#PWR0405"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3F6983" Ref="#PWR0505"  Part="1" 
F 0 "#PWR0505" H 2550 7200 50  0001 C CNN
F 1 "GND" H 2550 7300 50  0000 C CNN
F 2 "" H 2550 7450 50  0001 C CNN
F 3 "" H 2550 7450 50  0001 C CNN
	1    2550 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 7450 2550 7350
Wire Wire Line
	2550 6950 2550 6850
$Comp
L Device:LED D302
U 1 1 5B3F679F
P 2550 6300
AR Path="/59C5F008/59CBA3F1/5B3F679F" Ref="D302"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3F679F" Ref="D402"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3F679F" Ref="D502"  Part="1" 
F 0 "D302" H 2550 6400 50  0000 C CNN
F 1 "LED" H 2550 6200 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2550 6300 50  0001 C CNN
F 3 "" H 2550 6300 50  0001 C CNN
	1    2550 6300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R306
U 1 1 5B3F672A
P 2550 6700
AR Path="/59C5F008/59CBA3F1/5B3F672A" Ref="R306"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3F672A" Ref="R406"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3F672A" Ref="R506"  Part="1" 
F 0 "R306" V 2630 6700 50  0000 C CNN
F 1 "621" V 2550 6700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2480 6700 50  0001 C CNN
F 3 "" H 2550 6700 50  0001 C CNN
	1    2550 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 6450 2550 6550
Wire Wire Line
	2550 6150 2550 6050
Text Label 2250 7150 2    60   ~ 12
VOUT1
$Comp
L Device:Q_NMOS_GSD Q302
U 1 1 5B3F60EE
P 2450 7150
AR Path="/59C5F008/59CBA3F1/5B3F60EE" Ref="Q302"  Part="1" 
AR Path="/59C5F008/59CC6E78/5B3F60EE" Ref="Q402"  Part="1" 
AR Path="/59C5F008/5CF391C7/5B3F60EE" Ref="Q502"  Part="1" 
F 0 "Q302" H 2650 7200 50  0000 L CNN
F 1 "BSH105,215" H 2650 7100 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2650 7250 50  0001 C CNN
F 3 "" H 2450 7150 50  0001 C CNN
	1    2450 7150
	1    0    0    -1  
$EndComp
Text Label 2700 4550 0    60   ~ 12
VSET0
Text Label 5200 4550 0    60   ~ 12
VSET1
Text Label 7700 4550 0    60   ~ 12
VSET2
Text Label 10200 4550 0    60   ~ 12
VSET3
Entry Wire Line
	1050 950  1150 1050
Entry Wire Line
	1150 950  1250 1050
Entry Wire Line
	1250 950  1350 1050
Entry Wire Line
	1350 950  1450 1050
Text Label 1050 950  1    60   ~ 12
VSET0
Text Label 1150 950  1    60   ~ 12
VSET1
Text Label 1250 950  1    60   ~ 12
VSET2
Text Label 1350 950  1    60   ~ 12
VSET3
Text HLabel 1550 1050 2    60   Output ~ 12
VSET[0..3]
Wire Bus Line
	1150 2750 1550 2750
Wire Bus Line
	1150 1500 1550 1500
Wire Bus Line
	1150 2050 1550 2050
Wire Bus Line
	1150 1050 1550 1050
$EndSCHEMATC
