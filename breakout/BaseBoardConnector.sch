EESchema Schematic File Version 4
LIBS:pbv3_mass_test_adapter_breakout-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 14
Title "Powerboard v3 Massive Active Tester Board"
Date "2019-06-04"
Rev ""
Comp "Lawrence Berkeley National Laboratory"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1500 650  0    60   Input ~ 0
HVIN
Text HLabel 1500 750  0    60   Output ~ 0
HVIN_RTN
Text HLabel 1450 4950 0    60   Output ~ 0
HVOUT_RTN[0..9]
Wire Bus Line
	1500 4950 1450 4950
Entry Wire Line
	1500 5000 1600 5100
Entry Wire Line
	1500 5100 1600 5200
Entry Wire Line
	1500 5200 1600 5300
Entry Wire Line
	1500 5300 1600 5400
Entry Wire Line
	1500 5400 1600 5500
Entry Wire Line
	1500 5500 1600 5600
Entry Wire Line
	1500 5600 1600 5700
Entry Wire Line
	1500 5700 1600 5800
Entry Wire Line
	1500 5800 1600 5900
Wire Wire Line
	1600 5100 1650 5100
Wire Wire Line
	1600 5200 1650 5200
Wire Wire Line
	1600 5300 1650 5300
Wire Wire Line
	1600 5400 1650 5400
Wire Wire Line
	1600 5500 1650 5500
Wire Wire Line
	1600 5600 1650 5600
Wire Wire Line
	1600 5700 1650 5700
Wire Wire Line
	1600 5800 1650 5800
Wire Wire Line
	1600 5900 1650 5900
NoConn ~ 7400 1200
Text Label 7400 1100 2    60   ~ 0
HVIN_RTN
Text Label 7400 1700 2    60   ~ 0
VOUT_RTN0
Text Label 7400 3700 2    60   ~ 0
VOUT2
Text Label 7400 4100 2    60   ~ 0
VOUT3
Text Label 7400 5300 2    60   ~ 0
VOUT4
Text Label 7400 5700 2    60   ~ 0
VOUT5
Text Label 7400 2900 2    60   ~ 0
VOUT_RTN1
Text Label 7400 3300 2    60   ~ 0
VOUT_RTN2
Text Label 7400 4500 2    60   ~ 0
VOUT_RTN3
Text Label 7400 4900 2    60   ~ 0
VOUT_RTN4
Text Label 9400 1100 2    60   ~ 0
VOUT_RTN5
Text Label 9400 1900 2    60   ~ 0
VOUT6
Text Label 9400 2300 2    60   ~ 0
VOUT7
Text Label 9400 3500 2    60   ~ 0
VOUT8
Text Label 9400 3900 2    60   ~ 0
VOUT9
Text Label 9400 1500 2    60   ~ 0
VOUT_RTN6
Text Label 9400 2700 2    60   ~ 0
VOUT_RTN7
Text Label 9400 3100 2    60   ~ 0
VOUT_RTN8
Text Label 9400 4300 2    60   ~ 0
VOUT_RTN9
Text Label 8100 1200 0    60   ~ 0
HVIN
NoConn ~ 8100 1300
NoConn ~ 8100 1400
Text Label 8100 3500 0    60   ~ 0
CMDin_P2
Text Label 8100 4300 0    60   ~ 0
CMDin_P3
Text Label 8100 5100 0    60   ~ 0
CMDin_P4
Text Label 8100 5900 0    60   ~ 0
CMDin_P5
Text Label 10100 1700 0    60   ~ 0
CMDin_P6
Text Label 10100 2500 0    60   ~ 0
CMDin_P7
Text Label 10100 3300 0    60   ~ 0
CMDin_P8
Text Label 10100 4100 0    60   ~ 0
CMDin_P9
Text Label 8100 3600 0    60   ~ 0
CMDin_N2
Text Label 8100 4400 0    60   ~ 0
CMDin_N3
Text Label 8100 5200 0    60   ~ 0
CMDin_N4
Text Label 8100 6000 0    60   ~ 0
CMDin_N5
Text Label 10100 1800 0    60   ~ 0
CMDin_N6
Text Label 10100 2600 0    60   ~ 0
CMDin_N7
Text Label 10100 3400 0    60   ~ 0
CMDin_N8
Text Label 10100 4200 0    60   ~ 0
CMDin_N9
Text Label 8100 3700 0    60   ~ 0
CMDout_P2
Text Label 8100 4500 0    60   ~ 0
CMDout_P3
Text Label 8100 5300 0    60   ~ 0
CMDout_P4
Text Label 10100 1100 0    60   ~ 0
CMDout_P5
Text Label 10100 1900 0    60   ~ 0
CMDout_P6
Text Label 10100 2700 0    60   ~ 0
CMDout_P7
Text Label 10100 3500 0    60   ~ 0
CMDout_P8
Text Label 10100 4300 0    60   ~ 0
CMDout_P9
Text Label 8100 3800 0    60   ~ 0
CMDout_N2
Text Label 8100 4600 0    60   ~ 0
CMDout_N3
Text Label 8100 5400 0    60   ~ 0
CMDout_N4
Text Label 10100 1200 0    60   ~ 0
CMDout_N5
Text Label 10100 2000 0    60   ~ 0
CMDout_N6
Text Label 10100 2800 0    60   ~ 0
CMDout_N7
Text Label 10100 3600 0    60   ~ 0
CMDout_N8
Text Label 10100 4400 0    60   ~ 0
CMDout_N9
Text Label 8100 4000 0    60   ~ 0
HVOUT2
Text Label 8100 4800 0    60   ~ 0
HVOUT3
Text Label 8100 5600 0    60   ~ 0
HVOUT4
Text Label 10100 1400 0    60   ~ 0
HVOUT5
Text Label 10100 2200 0    60   ~ 0
HVOUT6
Text Label 10100 3800 0    60   ~ 0
HVOUT8
Text Label 10100 4600 0    60   ~ 0
HVOUT9
Text Label 8100 3900 0    60   ~ 0
HVOUT_RTN2
Text Label 8100 4700 0    60   ~ 0
HVOUT_RTN3
Text Label 8100 5500 0    60   ~ 0
HVOUT_RTN4
Text Label 10100 1300 0    60   ~ 0
HVOUT_RTN5
Text Label 10100 2100 0    60   ~ 0
HVOUT_RTN6
Text Label 10100 2900 0    60   ~ 0
HVOUT_RTN7
Text Label 10100 3700 0    60   ~ 0
HVOUT_RTN8
Text Label 10100 4500 0    60   ~ 0
HVOUT_RTN9
Text Label 9400 5400 2    60   ~ 0
VIN
Text Label 9400 5500 2    60   ~ 0
VIN_RTN
Text Label 7400 1300 2    60   ~ 0
I2C_SCL
Text Label 7400 1400 2    60   ~ 0
I2C_SDA
Text Label 9400 4700 2    60   ~ 0
3V3
Text Label 7400 1500 2    60   ~ 0
OUT
Text Label 8100 1700 0    60   ~ 0
ANALOGUE
Text Label 7400 1600 2    60   ~ 0
OUT_RTN
Wire Wire Line
	9400 4700 9150 4700
$Comp
L power:+3V3 #PWR05
U 1 1 5D5370FD
P 9150 4700
F 0 "#PWR05" H 9150 4550 50  0001 C CNN
F 1 "+3V3" H 9165 4873 50  0000 C CNN
F 2 "" H 9150 4700 50  0001 C CNN
F 3 "" H 9150 4700 50  0001 C CNN
	1    9150 4700
	-1   0    0    -1  
$EndComp
NoConn ~ 8100 1100
Wire Wire Line
	9400 4900 9400 5000
Connection ~ 9400 5000
Wire Wire Line
	9400 5000 9400 5100
Wire Wire Line
	9400 5500 9400 5600
Connection ~ 10100 5000
Wire Wire Line
	10100 5000 10100 4900
Connection ~ 10100 5100
Wire Wire Line
	10100 5100 10100 5000
Wire Wire Line
	10100 5500 10100 5600
Connection ~ 10100 5600
Wire Wire Line
	10100 5500 9400 5500
Wire Wire Line
	9400 5600 10100 5600
Wire Wire Line
	9400 5100 10100 5100
Wire Wire Line
	10100 5000 9400 5000
Wire Wire Line
	9400 4900 10100 4900
Connection ~ 9400 5600
Connection ~ 9400 5100
Wire Wire Line
	10100 5700 10100 5800
Wire Wire Line
	10100 5600 10100 5700
Connection ~ 10100 5700
Wire Wire Line
	10100 5200 10100 5100
Wire Wire Line
	9400 5600 9400 5700
Wire Wire Line
	9400 5200 9400 5100
Wire Wire Line
	9400 5700 9400 5800
Connection ~ 9400 5700
Text Label 8100 1800 0    60   ~ 0
ANALOGUE_RTN
Connection ~ 10100 5800
Wire Wire Line
	9400 5200 10100 5200
Wire Wire Line
	9400 5800 10100 5800
Wire Wire Line
	10100 5700 9400 5700
Connection ~ 10100 5200
Wire Wire Line
	10100 5900 10100 6000
Connection ~ 9400 5200
Connection ~ 9400 5800
Wire Wire Line
	10100 5900 10100 5800
Connection ~ 10100 5900
NoConn ~ 8100 1500
NoConn ~ 8100 1600
Text Label 8100 2900 0    60   ~ 0
CMDout_P1
Text Label 8100 3000 0    60   ~ 0
CMDout_N1
Text Label 8100 3200 0    60   ~ 0
HVOUT1
Text Label 8100 3100 0    60   ~ 0
HVOUT_RTN1
Wire Wire Line
	9400 6000 9400 5900
Connection ~ 9400 5900
Wire Wire Line
	9400 5900 9400 5800
Wire Wire Line
	9400 5900 10100 5900
Wire Wire Line
	10100 6000 9400 6000
Text Label 1650 5100 0    60   ~ 0
HVOUT_RTN0
Text Label 1650 5200 0    60   ~ 0
HVOUT_RTN1
Text Label 1650 5300 0    60   ~ 0
HVOUT_RTN2
Text Label 1650 5400 0    60   ~ 0
HVOUT_RTN3
Text Label 1650 5500 0    60   ~ 0
HVOUT_RTN4
Text Label 1650 5600 0    60   ~ 0
HVOUT_RTN5
Text Label 1650 5700 0    60   ~ 0
HVOUT_RTN6
Text Label 1650 5800 0    60   ~ 0
HVOUT_RTN7
Text Label 1650 5900 0    60   ~ 0
HVOUT_RTN8
Text Label 1650 6000 0    60   ~ 0
HVOUT_RTN9
Entry Wire Line
	1500 5900 1600 6000
Wire Wire Line
	1600 6000 1650 6000
Wire Bus Line
	1500 3800 1450 3800
Entry Wire Line
	1500 3850 1600 3950
Entry Wire Line
	1500 3950 1600 4050
Entry Wire Line
	1500 4050 1600 4150
Entry Wire Line
	1500 4150 1600 4250
Entry Wire Line
	1500 4250 1600 4350
Entry Wire Line
	1500 4350 1600 4450
Entry Wire Line
	1500 4450 1600 4550
Entry Wire Line
	1500 4550 1600 4650
Entry Wire Line
	1500 4650 1600 4750
Wire Wire Line
	1600 3950 1650 3950
Wire Wire Line
	1600 4050 1650 4050
Wire Wire Line
	1600 4150 1650 4150
Wire Wire Line
	1600 4250 1650 4250
Wire Wire Line
	1600 4350 1650 4350
Wire Wire Line
	1600 4450 1650 4450
Wire Wire Line
	1600 4550 1650 4550
Wire Wire Line
	1600 4650 1650 4650
Wire Wire Line
	1600 4750 1650 4750
Entry Wire Line
	1500 4750 1600 4850
Wire Wire Line
	1600 4850 1650 4850
Text HLabel 1450 2650 0    60   Output ~ 0
VOUT_RTN[0..9]
Wire Bus Line
	1500 2650 1450 2650
Entry Wire Line
	1500 2700 1600 2800
Entry Wire Line
	1500 2800 1600 2900
Entry Wire Line
	1500 2900 1600 3000
Entry Wire Line
	1500 3000 1600 3100
Entry Wire Line
	1500 3100 1600 3200
Entry Wire Line
	1500 3200 1600 3300
Entry Wire Line
	1500 3300 1600 3400
Entry Wire Line
	1500 3400 1600 3500
Entry Wire Line
	1500 3500 1600 3600
Wire Wire Line
	1600 2800 1650 2800
Wire Wire Line
	1600 2900 1650 2900
Wire Wire Line
	1600 3000 1650 3000
Wire Wire Line
	1600 3100 1650 3100
Wire Wire Line
	1600 3200 1650 3200
Wire Wire Line
	1600 3300 1650 3300
Wire Wire Line
	1600 3400 1650 3400
Wire Wire Line
	1600 3500 1650 3500
Wire Wire Line
	1600 3600 1650 3600
Entry Wire Line
	1500 3600 1600 3700
Wire Wire Line
	1600 3700 1650 3700
Text Label 1650 2800 0    60   ~ 0
VOUT_RTN0
Text Label 1650 2900 0    60   ~ 0
VOUT_RTN1
Text Label 1650 3000 0    60   ~ 0
VOUT_RTN2
Text Label 1650 3100 0    60   ~ 0
VOUT_RTN3
Text Label 1650 3200 0    60   ~ 0
VOUT_RTN4
Text Label 1650 3300 0    60   ~ 0
VOUT_RTN5
Text Label 1650 3400 0    60   ~ 0
VOUT_RTN6
Text Label 1650 3500 0    60   ~ 0
VOUT_RTN7
Text Label 1650 3600 0    60   ~ 0
VOUT_RTN8
Text Label 1650 3700 0    60   ~ 0
VOUT_RTN9
Wire Bus Line
	1500 1500 1450 1500
Wire Wire Line
	1600 1650 1650 1650
Wire Wire Line
	1600 1750 1650 1750
Wire Wire Line
	1600 1850 1650 1850
Wire Wire Line
	1600 1950 1650 1950
Wire Wire Line
	1600 2050 1650 2050
Wire Wire Line
	1600 2150 1650 2150
Wire Wire Line
	1600 2250 1650 2250
Wire Wire Line
	1600 2350 1650 2350
Wire Wire Line
	1600 2450 1650 2450
Wire Wire Line
	1600 2550 1650 2550
Text HLabel 2000 650  0    60   Input ~ 0
I2C_SCL
Text HLabel 2000 750  0    60   BiDi ~ 0
I2C_SDA
Text HLabel 2000 900  0    60   Output ~ 0
OUT
Text HLabel 2000 1000 0    60   UnSpc ~ 0
OUT_RTN
Text HLabel 1500 900  0    60   Input ~ 0
ANALOGUE
Text HLabel 1500 1000 0    60   UnSpc ~ 0
ANALOGUE_RTN
Wire Bus Line
	3100 1500 3050 1500
Wire Wire Line
	4700 2550 4750 2550
Wire Bus Line
	3100 2650 3050 2650
Entry Wire Line
	3100 3000 3200 3100
Entry Wire Line
	3100 3100 3200 3200
Entry Wire Line
	3100 3200 3200 3300
Entry Wire Line
	3100 3300 3200 3400
Entry Wire Line
	3100 3400 3200 3500
Entry Wire Line
	3100 3500 3200 3600
Wire Wire Line
	3200 3000 3250 3000
Wire Wire Line
	3200 3100 3250 3100
Wire Wire Line
	3200 3200 3250 3200
Wire Wire Line
	3200 3300 3250 3300
Wire Wire Line
	3200 3400 3250 3400
Wire Wire Line
	3200 3500 3250 3500
Wire Wire Line
	3200 3600 3250 3600
Entry Wire Line
	3100 3600 3200 3700
Wire Wire Line
	3200 3700 3250 3700
Wire Bus Line
	4600 2650 4550 2650
Entry Wire Line
	4600 2700 4700 2800
Entry Wire Line
	4600 2800 4700 2900
Entry Wire Line
	4600 2900 4700 3000
Entry Wire Line
	4600 3000 4700 3100
Entry Wire Line
	4600 3100 4700 3200
Entry Wire Line
	4600 3200 4700 3300
Entry Wire Line
	4600 3300 4700 3400
Entry Wire Line
	4600 3400 4700 3500
Entry Wire Line
	4600 3500 4700 3600
Wire Wire Line
	4700 2800 4750 2800
Wire Wire Line
	4700 2900 4750 2900
Wire Wire Line
	4700 3000 4750 3000
Wire Wire Line
	4700 3100 4750 3100
Wire Wire Line
	4700 3200 4750 3200
Wire Wire Line
	4700 3300 4750 3300
Wire Wire Line
	4700 3400 4750 3400
Wire Wire Line
	4700 3500 4750 3500
Wire Wire Line
	4700 3600 4750 3600
Entry Wire Line
	4600 3600 4700 3700
Wire Wire Line
	4700 3700 4750 3700
Wire Bus Line
	3100 3800 3050 3800
Entry Wire Line
	3100 3850 3200 3950
Entry Wire Line
	3100 3950 3200 4050
Entry Wire Line
	3100 4050 3200 4150
Entry Wire Line
	3100 4150 3200 4250
Entry Wire Line
	3100 4250 3200 4350
Entry Wire Line
	3100 4350 3200 4450
Entry Wire Line
	3100 4450 3200 4550
Entry Wire Line
	3100 4550 3200 4650
Entry Wire Line
	3100 4650 3200 4750
Wire Wire Line
	3200 3950 3250 3950
Wire Wire Line
	3200 4050 3250 4050
Wire Wire Line
	3200 4150 3250 4150
Wire Wire Line
	3200 4250 3250 4250
Wire Wire Line
	3200 4350 3250 4350
Wire Wire Line
	3200 4450 3250 4450
Wire Wire Line
	3200 4550 3250 4550
Wire Wire Line
	3200 4650 3250 4650
Wire Wire Line
	3200 4750 3250 4750
Entry Wire Line
	3100 4750 3200 4850
Wire Wire Line
	3200 4850 3250 4850
Wire Bus Line
	4600 3800 4550 3800
Entry Wire Line
	4600 3850 4700 3950
Entry Wire Line
	4600 3950 4700 4050
Entry Wire Line
	4600 4050 4700 4150
Entry Wire Line
	4600 4150 4700 4250
Entry Wire Line
	4600 4250 4700 4350
Entry Wire Line
	4600 4350 4700 4450
Entry Wire Line
	4600 4450 4700 4550
Entry Wire Line
	4600 4550 4700 4650
Entry Wire Line
	4600 4650 4700 4750
Wire Wire Line
	4700 3950 4750 3950
Wire Wire Line
	4700 4050 4750 4050
Wire Wire Line
	4700 4150 4750 4150
Wire Wire Line
	4700 4250 4750 4250
Wire Wire Line
	4700 4350 4750 4350
Wire Wire Line
	4700 4450 4750 4450
Wire Wire Line
	4700 4550 4750 4550
Wire Wire Line
	4700 4650 4750 4650
Wire Wire Line
	4700 4750 4750 4750
Entry Wire Line
	4600 4750 4700 4850
Wire Wire Line
	4700 4850 4750 4850
Text HLabel 950  650  0    60   Input ~ 0
VIN
Text HLabel 950  750  0    60   Output ~ 0
VIN_RTN
$Comp
L power:GND #PWR06
U 1 1 5D437E95
P 9150 4800
F 0 "#PWR06" H 9150 4550 50  0001 C CNN
F 1 "GND" H 9155 4627 50  0000 C CNN
F 2 "" H 9150 4800 50  0001 C CNN
F 3 "" H 9150 4800 50  0001 C CNN
	1    9150 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 4800 9400 4800
Text Label 8100 2500 0    60   ~ 0
COIL_P0
Text Label 8100 2600 0    60   ~ 0
COIL_N0
Text Label 3250 1650 0    60   ~ 0
COIL_P0
Text Label 3250 1750 0    60   ~ 0
COIL_P1
Text Label 3250 1850 0    60   ~ 0
COIL_P2
Text Label 3250 1950 0    60   ~ 0
COIL_P3
Text Label 3250 2050 0    60   ~ 0
COIL_P4
Text Label 3250 2150 0    60   ~ 0
COIL_P5
Text Label 3250 2250 0    60   ~ 0
COIL_P6
Text Label 3250 2350 0    60   ~ 0
COIL_P7
Text Label 3250 2450 0    60   ~ 0
COIL_P8
Text Label 3250 2550 0    60   ~ 0
COIL_P9
Text HLabel 3050 1500 0    60   Output ~ 0
COIL_P[0..9]
Text Label 1650 1650 0    60   ~ 0
VOUT0
Text Label 1650 1750 0    60   ~ 0
VOUT1
Text Label 1650 1850 0    60   ~ 0
VOUT2
Text Label 1650 1950 0    60   ~ 0
VOUT3
Text Label 1650 2050 0    60   ~ 0
VOUT4
Text Label 1650 2150 0    60   ~ 0
VOUT5
Text Label 1650 2250 0    60   ~ 0
VOUT6
Text Label 1650 2350 0    60   ~ 0
VOUT7
Text Label 1650 2450 0    60   ~ 0
VOUT8
Text Label 1650 2550 0    60   ~ 0
VOUT9
Text HLabel 1450 1500 0    60   Output ~ 0
VOUT[0..9]
Text Label 7400 2100 2    60   ~ 0
VOUT0
Entry Wire Line
	1500 1550 1600 1650
Entry Wire Line
	1500 1650 1600 1750
Entry Wire Line
	1500 1750 1600 1850
Entry Wire Line
	1500 1850 1600 1950
Entry Wire Line
	1500 1950 1600 2050
Entry Wire Line
	1500 2050 1600 2150
Entry Wire Line
	1500 2150 1600 2250
Entry Wire Line
	1500 2250 1600 2350
Entry Wire Line
	1500 2350 1600 2450
Entry Wire Line
	1500 2450 1600 2550
Text Label 3250 2800 0    60   ~ 0
CMDin_P0
Text Label 3250 2900 0    60   ~ 0
CMDin_P1
Text Label 3250 3000 0    60   ~ 0
CMDin_P2
Text Label 3250 3100 0    60   ~ 0
CMDin_P3
Text Label 3250 3200 0    60   ~ 0
CMDin_P4
Text Label 3250 3300 0    60   ~ 0
CMDin_P5
Text Label 3250 3400 0    60   ~ 0
CMDin_P6
Text Label 3250 3500 0    60   ~ 0
CMDin_P7
Text Label 3250 3600 0    60   ~ 0
CMDin_P8
Text Label 3250 3700 0    60   ~ 0
CMDin_P9
Text HLabel 3050 2650 0    60   Input ~ 0
CMDin_P[0..9]
Text Label 8100 2700 0    60   ~ 0
CMDin_P1
Text Label 8100 2800 0    60   ~ 0
CMDin_N1
Text Label 8100 1900 0    60   ~ 0
CMDin_P0
Text Label 8100 2000 0    60   ~ 0
CMDin_N0
Text Label 8100 3300 0    60   ~ 0
COIL_P1
Text Label 8100 3400 0    60   ~ 0
COIL_N1
Text Label 8100 4100 0    60   ~ 0
COIL_P2
Text Label 8100 4200 0    60   ~ 0
COIL_N2
Text Label 8100 4900 0    60   ~ 0
COIL_P3
Text Label 8100 5000 0    60   ~ 0
COIL_N3
Text Label 8100 5700 0    60   ~ 0
COIL_P4
Text Label 8100 5800 0    60   ~ 0
COIL_N4
Text Label 10100 1500 0    60   ~ 0
COIL_P5
Text Label 10100 1600 0    60   ~ 0
COIL_N5
Text Label 10100 2300 0    60   ~ 0
COIL_P6
Text Label 10100 2400 0    60   ~ 0
COIL_N6
Text Label 10100 3100 0    60   ~ 0
COIL_P7
Text Label 10100 3200 0    60   ~ 0
COIL_N7
Text Label 10100 3900 0    60   ~ 0
COIL_P8
Text Label 10100 4000 0    60   ~ 0
COIL_N8
Text Label 10100 4700 0    60   ~ 0
COIL_P9
Text Label 10100 4800 0    60   ~ 0
COIL_N9
Text Label 10100 3000 0    60   ~ 0
HVOUT7
Text Label 4750 2800 0    60   ~ 0
CMDin_N0
Text Label 4750 2900 0    60   ~ 0
CMDin_N1
Text Label 4750 3000 0    60   ~ 0
CMDin_N2
Text Label 4750 3100 0    60   ~ 0
CMDin_N3
Text Label 4750 3200 0    60   ~ 0
CMDin_N4
Text Label 4750 3300 0    60   ~ 0
CMDin_N5
Text Label 4750 3400 0    60   ~ 0
CMDin_N6
Text Label 4750 3500 0    60   ~ 0
CMDin_N7
Text Label 4750 3600 0    60   ~ 0
CMDin_N8
Text Label 4750 3700 0    60   ~ 0
CMDin_N9
Text HLabel 4550 2650 0    60   Input ~ 0
CMDin_N[0..9]
Text Label 3250 3950 0    60   ~ 0
CMDout_P0
Text Label 3250 4050 0    60   ~ 0
CMDout_P1
Text Label 3250 4150 0    60   ~ 0
CMDout_P2
Text Label 3250 4250 0    60   ~ 0
CMDout_P3
Text Label 3250 4350 0    60   ~ 0
CMDout_P4
Text Label 3250 4450 0    60   ~ 0
CMDout_P5
Text Label 3250 4550 0    60   ~ 0
CMDout_P6
Text Label 3250 4650 0    60   ~ 0
CMDout_P7
Text Label 3250 4750 0    60   ~ 0
CMDout_P8
Text Label 3250 4850 0    60   ~ 0
CMDout_P9
Text Label 4750 3950 0    60   ~ 0
CMDout_N0
Text Label 4750 4050 0    60   ~ 0
CMDout_N1
Text Label 4750 4150 0    60   ~ 0
CMDout_N2
Text Label 4750 4250 0    60   ~ 0
CMDout_N3
Text Label 4750 4350 0    60   ~ 0
CMDout_N4
Text Label 4750 4450 0    60   ~ 0
CMDout_N5
Text Label 4750 4550 0    60   ~ 0
CMDout_N6
Text Label 4750 4650 0    60   ~ 0
CMDout_N7
Text Label 4750 4750 0    60   ~ 0
CMDout_N8
Text Label 4750 4850 0    60   ~ 0
CMDout_N9
Text HLabel 4550 3800 0    60   Output ~ 0
CMDout_N[0..9]
Text HLabel 3050 3800 0    60   Output ~ 0
CMDout_P[0..9]
Text Label 7400 2500 2    60   ~ 0
VOUT1
Text Label 8100 2100 0    60   ~ 0
CMDout_P0
Text Label 8100 2200 0    60   ~ 0
CMDout_N0
Text Label 8100 2300 0    60   ~ 0
HVOUT_RTN0
Text Label 8100 2400 0    60   ~ 0
HVOUT0
Text Label 1650 3950 0    60   ~ 0
HVOUT0
Text Label 1650 4050 0    60   ~ 0
HVOUT1
Text Label 1650 4150 0    60   ~ 0
HVOUT2
Text Label 1650 4250 0    60   ~ 0
HVOUT3
Text Label 1650 4350 0    60   ~ 0
HVOUT4
Text Label 1650 4450 0    60   ~ 0
HVOUT5
Text Label 1650 4550 0    60   ~ 0
HVOUT6
Text Label 1650 4650 0    60   ~ 0
HVOUT7
Text Label 1650 4750 0    60   ~ 0
HVOUT8
Text Label 1650 4850 0    60   ~ 0
HVOUT9
Text HLabel 1450 3800 0    60   Output ~ 0
HVOUT[0..9]
Entry Wire Line
	3100 1550 3200 1650
Entry Wire Line
	3100 1650 3200 1750
Entry Wire Line
	3100 1750 3200 1850
Entry Wire Line
	3100 1850 3200 1950
Entry Wire Line
	3100 1950 3200 2050
Entry Wire Line
	3100 2050 3200 2150
Entry Wire Line
	3100 2150 3200 2250
Entry Wire Line
	3100 2250 3200 2350
Entry Wire Line
	3100 2350 3200 2450
Entry Wire Line
	3100 2450 3200 2550
Wire Wire Line
	3200 1650 3250 1650
Wire Wire Line
	3200 1750 3250 1750
Wire Wire Line
	3200 1850 3250 1850
Wire Wire Line
	3200 1950 3250 1950
Wire Wire Line
	3200 2050 3250 2050
Wire Wire Line
	3200 2150 3250 2150
Wire Wire Line
	3200 2250 3250 2250
Wire Wire Line
	3200 2350 3250 2350
Wire Wire Line
	3200 2450 3250 2450
Wire Wire Line
	3200 2550 3250 2550
Text HLabel 4550 1500 0    60   Output ~ 0
COIL_N[0..9]
Text Label 4750 2550 0    60   ~ 0
COIL_N9
Text Label 4750 2450 0    60   ~ 0
COIL_N8
Text Label 4750 2350 0    60   ~ 0
COIL_N7
Text Label 4750 2250 0    60   ~ 0
COIL_N6
Text Label 4750 2150 0    60   ~ 0
COIL_N5
Text Label 4750 2050 0    60   ~ 0
COIL_N4
Text Label 4750 1950 0    60   ~ 0
COIL_N3
Text Label 4750 1850 0    60   ~ 0
COIL_N2
Text Label 4750 1750 0    60   ~ 0
COIL_N1
Text Label 4750 1650 0    60   ~ 0
COIL_N0
Entry Wire Line
	4600 2450 4700 2550
Wire Wire Line
	4700 2450 4750 2450
Wire Wire Line
	4700 2350 4750 2350
Wire Wire Line
	4700 2250 4750 2250
Wire Wire Line
	4700 2150 4750 2150
Wire Wire Line
	4700 2050 4750 2050
Wire Wire Line
	4700 1950 4750 1950
Wire Wire Line
	4700 1850 4750 1850
Wire Wire Line
	4700 1750 4750 1750
Wire Wire Line
	4700 1650 4750 1650
Entry Wire Line
	4600 2350 4700 2450
Entry Wire Line
	4600 2250 4700 2350
Entry Wire Line
	4600 2150 4700 2250
Entry Wire Line
	4600 2050 4700 2150
Entry Wire Line
	4600 1950 4700 2050
Entry Wire Line
	4600 1850 4700 1950
Entry Wire Line
	4600 1750 4700 1850
Entry Wire Line
	4600 1650 4700 1750
Entry Wire Line
	4600 1550 4700 1650
Wire Bus Line
	4600 1500 4550 1500
Wire Wire Line
	9400 5300 9400 5400
Wire Wire Line
	9400 5200 9400 5300
Connection ~ 9400 5300
Wire Wire Line
	9400 4500 9400 4600
Connection ~ 9400 4500
Wire Wire Line
	9400 4400 9400 4500
Wire Wire Line
	9400 4300 9400 4400
Connection ~ 9400 4400
Wire Wire Line
	9400 4100 9400 4200
Connection ~ 9400 4100
Wire Wire Line
	9400 4000 9400 4100
Wire Wire Line
	9400 3900 9400 4000
Connection ~ 9400 4000
Wire Wire Line
	9400 3700 9400 3800
Connection ~ 9400 3700
Wire Wire Line
	9400 3600 9400 3700
Wire Wire Line
	9400 3500 9400 3600
Connection ~ 9400 3600
Wire Wire Line
	9400 3300 9400 3400
Connection ~ 9400 3300
Wire Wire Line
	9400 3200 9400 3300
Wire Wire Line
	9400 3100 9400 3200
Connection ~ 9400 3200
Wire Wire Line
	9400 2900 9400 3000
Connection ~ 9400 2900
Wire Wire Line
	9400 2800 9400 2900
Wire Wire Line
	9400 2700 9400 2800
Connection ~ 9400 2800
Wire Wire Line
	9400 2500 9400 2600
Connection ~ 9400 2500
Wire Wire Line
	9400 2400 9400 2500
Wire Wire Line
	9400 2300 9400 2400
Connection ~ 9400 2400
Wire Wire Line
	9400 2100 9400 2200
Connection ~ 9400 2100
Wire Wire Line
	9400 2000 9400 2100
Wire Wire Line
	9400 1900 9400 2000
Connection ~ 9400 2000
Wire Wire Line
	9400 1700 9400 1800
Connection ~ 9400 1700
Wire Wire Line
	9400 1600 9400 1700
Wire Wire Line
	9400 1500 9400 1600
Connection ~ 9400 1600
Wire Wire Line
	9400 1300 9400 1400
Connection ~ 9400 1300
Wire Wire Line
	9400 1200 9400 1300
Wire Wire Line
	9400 1100 9400 1200
Connection ~ 9400 1200
Wire Wire Line
	10100 5300 10100 5400
Wire Wire Line
	10100 5200 10100 5300
Connection ~ 10100 5300
$Comp
L lbl_conn:Conn_02x100_Row_Letter_First-lbl_conn U?
U 2 1 5CC7A09F
P 9750 2500
AR Path="/5CC7A09F" Ref="U?"  Part="2" 
AR Path="/5B3C449A/5CC7A09F" Ref="U1"  Part="2" 
F 0 "U1" H 9750 4165 50  0000 C CNN
F 1 "Conn_02x100_Row_Letter_First-lbl_conn" H 9750 -1150 50  0000 C CNN
F 2 "lbl_conn:Sullins_EdgeCard_200" H 9750 2500 50  0001 C CNN
F 3 "" H 9750 2500 50  0001 C CNN
	2    9750 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 1900 7400 2000
Connection ~ 7400 1900
Wire Wire Line
	7400 1800 7400 1900
Wire Wire Line
	7400 1700 7400 1800
Connection ~ 7400 1800
Wire Wire Line
	7400 5900 7400 6000
Connection ~ 7400 5900
Wire Wire Line
	7400 5800 7400 5900
Wire Wire Line
	7400 5700 7400 5800
Connection ~ 7400 5800
Wire Wire Line
	7400 5500 7400 5600
Connection ~ 7400 5500
Wire Wire Line
	7400 5400 7400 5500
Wire Wire Line
	7400 5300 7400 5400
Connection ~ 7400 5400
Wire Wire Line
	7400 5100 7400 5200
Connection ~ 7400 5100
Wire Wire Line
	7400 5000 7400 5100
Wire Wire Line
	7400 4900 7400 5000
Connection ~ 7400 5000
Wire Wire Line
	7400 4700 7400 4800
Connection ~ 7400 4700
Wire Wire Line
	7400 4600 7400 4700
Wire Wire Line
	7400 4500 7400 4600
Connection ~ 7400 4600
Wire Wire Line
	7400 4300 7400 4400
Connection ~ 7400 4300
Wire Wire Line
	7400 4200 7400 4300
Wire Wire Line
	7400 4100 7400 4200
Connection ~ 7400 4200
Wire Wire Line
	7400 3900 7400 4000
Connection ~ 7400 3900
Wire Wire Line
	7400 3800 7400 3900
Wire Wire Line
	7400 3700 7400 3800
Connection ~ 7400 3800
Wire Wire Line
	7400 3500 7400 3600
Connection ~ 7400 3500
Wire Wire Line
	7400 3400 7400 3500
Wire Wire Line
	7400 3300 7400 3400
Connection ~ 7400 3400
Wire Wire Line
	7400 3100 7400 3200
Connection ~ 7400 3100
Wire Wire Line
	7400 3000 7400 3100
Wire Wire Line
	7400 2900 7400 3000
Connection ~ 7400 3000
Wire Wire Line
	7400 2700 7400 2800
Connection ~ 7400 2700
Wire Wire Line
	7400 2600 7400 2700
Wire Wire Line
	7400 2500 7400 2600
Connection ~ 7400 2600
Wire Wire Line
	7400 2300 7400 2400
Connection ~ 7400 2300
Wire Wire Line
	7400 2200 7400 2300
Wire Wire Line
	7400 2100 7400 2200
Connection ~ 7400 2200
$Comp
L lbl_conn:Conn_02x100_Row_Letter_First-lbl_conn U?
U 1 1 5D5370FC
P 7750 2500
AR Path="/5D5370FC" Ref="U?"  Part="1" 
AR Path="/5B3C449A/5D5370FC" Ref="U1"  Part="1" 
F 0 "U1" H 7750 4165 50  0000 C CNN
F 1 "Conn_02x100_Row_Letter_First-lbl_conn" H 7750 -1150 50  0000 C CNN
F 2 "lbl_conn:Sullins_EdgeCard_200" H 6550 2500 50  0001 C CNN
F 3 "" H 6550 2500 50  0001 C CNN
F 4 "GBB100DHAN" H 7750 2500 50  0001 C CNN "mfg#"
	1    7750 2500
	1    0    0    -1  
$EndComp
Wire Bus Line
	3100 2650 3100 3600
Wire Bus Line
	3100 1500 3100 2450
Wire Bus Line
	1500 1500 1500 2450
Wire Bus Line
	4600 3800 4600 4750
Wire Bus Line
	3100 3800 3100 4750
Wire Bus Line
	4600 2650 4600 3600
Wire Bus Line
	1500 2650 1500 3600
Wire Bus Line
	1500 3800 1500 4750
Wire Bus Line
	1500 4950 1500 5900
Wire Bus Line
	4600 1500 4600 2450
$EndSCHEMATC
