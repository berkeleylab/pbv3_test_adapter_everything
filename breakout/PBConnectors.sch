EESchema Schematic File Version 4
LIBS:pbv3_mass_test_adapter_breakout-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 14
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5450 4025 2    60   UnSpc ~ 0
HVOUT_RTN
Text HLabel 4850 4025 0    60   Output ~ 0
HVOUT
Text HLabel 5400 2550 2    60   UnSpc ~ 0
VOUT_RTN
Text HLabel 4900 2650 0    60   Output ~ 0
VOUT
Text HLabel 4850 4750 0    60   Input ~ 0
COIL_P
Text HLabel 5450 4750 2    60   Input ~ 0
COIL_N
Text HLabel 5400 3100 2    60   Output ~ 0
CMDin_P
Text HLabel 4900 3100 0    60   Output ~ 0
CMDin_N
Text HLabel 5400 3000 2    60   Input ~ 0
CMDout_P
Text HLabel 4900 3000 0    60   Input ~ 0
CMDout_N
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J5
U 1 1 5D45DE84
P 5100 2550
AR Path="/5D418BF8/5D45DE84" Ref="J5"  Part="1" 
AR Path="/5D4BD3DE/5D45DE84" Ref="J9"  Part="1" 
AR Path="/5D4C35D5/5D45DE84" Ref="J13"  Part="1" 
AR Path="/5D4C35E2/5D45DE84" Ref="J17"  Part="1" 
AR Path="/5D4D1817/5D45DE84" Ref="J21"  Part="1" 
AR Path="/5D4D1824/5D45DE84" Ref="J41"  Part="1" 
AR Path="/5D4D1831/5D45DE84" Ref="J37"  Part="1" 
AR Path="/5D4D183E/5D45DE84" Ref="J33"  Part="1" 
AR Path="/5D4D8A1F/5D45DE84" Ref="J29"  Part="1" 
AR Path="/5D4D8A2C/5D45DE84" Ref="J25"  Part="1" 
F 0 "J41" H 5150 2767 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 5150 2676 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 5100 2550 50  0001 C CNN
F 3 "~" H 5100 2550 50  0001 C CNN
	1    5100 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 2550 5400 2550
Wire Wire Line
	4900 2650 5400 2650
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J6
U 1 1 5D4798B3
P 5100 3200
AR Path="/5D418BF8/5D4798B3" Ref="J6"  Part="1" 
AR Path="/5D4BD3DE/5D4798B3" Ref="J10"  Part="1" 
AR Path="/5D4C35D5/5D4798B3" Ref="J14"  Part="1" 
AR Path="/5D4C35E2/5D4798B3" Ref="J18"  Part="1" 
AR Path="/5D4D1817/5D4798B3" Ref="J22"  Part="1" 
AR Path="/5D4D1824/5D4798B3" Ref="J42"  Part="1" 
AR Path="/5D4D1831/5D4798B3" Ref="J38"  Part="1" 
AR Path="/5D4D183E/5D4798B3" Ref="J34"  Part="1" 
AR Path="/5D4D8A1F/5D4798B3" Ref="J30"  Part="1" 
AR Path="/5D4D8A2C/5D4798B3" Ref="J26"  Part="1" 
F 0 "J42" H 5150 3617 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 5150 3526 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 5100 3200 50  0001 C CNN
F 3 "~" H 5100 3200 50  0001 C CNN
	1    5100 3200
	1    0    0    -1  
$EndComp
NoConn ~ 4900 3200
Wire Wire Line
	5400 3200 5400 3300
Connection ~ 5400 3300
Wire Wire Line
	5400 3300 5400 3400
Connection ~ 5400 3400
Wire Wire Line
	5400 3400 5400 3500
$Comp
L power:GND #PWR07
U 1 1 5D490863
P 5400 3500
AR Path="/5D418BF8/5D490863" Ref="#PWR07"  Part="1" 
AR Path="/5D4BD3DE/5D490863" Ref="#PWR08"  Part="1" 
AR Path="/5D4C35D5/5D490863" Ref="#PWR09"  Part="1" 
AR Path="/5D4C35E2/5D490863" Ref="#PWR010"  Part="1" 
AR Path="/5D4D1817/5D490863" Ref="#PWR011"  Part="1" 
AR Path="/5D4D1824/5D490863" Ref="#PWR016"  Part="1" 
AR Path="/5D4D1831/5D490863" Ref="#PWR015"  Part="1" 
AR Path="/5D4D183E/5D490863" Ref="#PWR014"  Part="1" 
AR Path="/5D4D8A1F/5D490863" Ref="#PWR013"  Part="1" 
AR Path="/5D4D8A2C/5D490863" Ref="#PWR012"  Part="1" 
F 0 "#PWR016" H 5400 3250 50  0001 C CNN
F 1 "GND" H 5405 3327 50  0000 C CNN
F 2 "" H 5400 3500 50  0001 C CNN
F 3 "" H 5400 3500 50  0001 C CNN
	1    5400 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3400 5400 3400
Text HLabel 4900 3300 0    60   Output ~ 0
ANALOGUE
$Comp
L Connector_Generic:Conn_02x01 J7
U 1 1 5D49A440
P 5100 4025
AR Path="/5D418BF8/5D49A440" Ref="J7"  Part="1" 
AR Path="/5D4BD3DE/5D49A440" Ref="J11"  Part="1" 
AR Path="/5D4C35D5/5D49A440" Ref="J15"  Part="1" 
AR Path="/5D4C35E2/5D49A440" Ref="J19"  Part="1" 
AR Path="/5D4D1817/5D49A440" Ref="J23"  Part="1" 
AR Path="/5D4D1824/5D49A440" Ref="J43"  Part="1" 
AR Path="/5D4D1831/5D49A440" Ref="J39"  Part="1" 
AR Path="/5D4D183E/5D49A440" Ref="J35"  Part="1" 
AR Path="/5D4D8A1F/5D49A440" Ref="J31"  Part="1" 
AR Path="/5D4D8A2C/5D49A440" Ref="J27"  Part="1" 
F 0 "J43" H 5150 4242 50  0000 C CNN
F 1 "Conn_02x01" H 5150 4151 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 5100 4025 50  0001 C CNN
F 3 "~" H 5100 4025 50  0001 C CNN
	1    5100 4025
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5D49CEFC
P 5150 4400
AR Path="/5D418BF8/5D49CEFC" Ref="R11"  Part="1" 
AR Path="/5D4BD3DE/5D49CEFC" Ref="R12"  Part="1" 
AR Path="/5D4C35D5/5D49CEFC" Ref="R13"  Part="1" 
AR Path="/5D4C35E2/5D49CEFC" Ref="R14"  Part="1" 
AR Path="/5D4D1817/5D49CEFC" Ref="R15"  Part="1" 
AR Path="/5D4D1824/5D49CEFC" Ref="R20"  Part="1" 
AR Path="/5D4D1831/5D49CEFC" Ref="R19"  Part="1" 
AR Path="/5D4D183E/5D49CEFC" Ref="R18"  Part="1" 
AR Path="/5D4D8A1F/5D49CEFC" Ref="R17"  Part="1" 
AR Path="/5D4D8A2C/5D49CEFC" Ref="R16"  Part="1" 
F 0 "R20" V 4943 4400 50  0000 C CNN
F 1 "5k" V 5034 4400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5080 4400 50  0001 C CNN
F 3 "~" H 5150 4400 50  0001 C CNN
	1    5150 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	5000 4400 4900 4400
Wire Wire Line
	4900 4400 4900 4025
Wire Wire Line
	5400 4025 5400 4400
Wire Wire Line
	5400 4400 5300 4400
Wire Wire Line
	5450 4025 5400 4025
Connection ~ 5400 4025
Wire Wire Line
	4900 4025 4850 4025
Connection ~ 4900 4025
$Comp
L Connector_Generic:Conn_02x01 J8
U 1 1 5D4A32C1
P 5100 4750
AR Path="/5D418BF8/5D4A32C1" Ref="J8"  Part="1" 
AR Path="/5D4BD3DE/5D4A32C1" Ref="J12"  Part="1" 
AR Path="/5D4C35D5/5D4A32C1" Ref="J16"  Part="1" 
AR Path="/5D4C35E2/5D4A32C1" Ref="J20"  Part="1" 
AR Path="/5D4D1817/5D4A32C1" Ref="J24"  Part="1" 
AR Path="/5D4D1824/5D4A32C1" Ref="J44"  Part="1" 
AR Path="/5D4D1831/5D4A32C1" Ref="J40"  Part="1" 
AR Path="/5D4D183E/5D4A32C1" Ref="J36"  Part="1" 
AR Path="/5D4D8A1F/5D4A32C1" Ref="J32"  Part="1" 
AR Path="/5D4D8A2C/5D4A32C1" Ref="J28"  Part="1" 
F 0 "J44" H 5150 4967 50  0000 C CNN
F 1 "Conn_02x01" H 5150 4876 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 5100 4750 50  0001 C CNN
F 3 "~" H 5100 4750 50  0001 C CNN
	1    5100 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 4750 5400 4750
Wire Wire Line
	4900 4750 4850 4750
$EndSCHEMATC
