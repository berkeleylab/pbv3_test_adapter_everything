EESchema Schematic File Version 4
LIBS:pbv3_test_adapter_passive-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pbv3_test_adapter_passive-rescue:GND #PWR9
U 1 1 5B766A28
P 8150 4300
F 0 "#PWR9" H 8150 4050 50  0001 C CNN
F 1 "GND" H 8150 4150 50  0000 C CNN
F 2 "" H 8150 4300 50  0001 C CNN
F 3 "" H 8150 4300 50  0001 C CNN
	1    8150 4300
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:GND #PWR3
U 1 1 5B767491
P 1850 4300
F 0 "#PWR3" H 1850 4050 50  0001 C CNN
F 1 "GND" H 1850 4150 50  0000 C CNN
F 2 "" H 1850 4300 50  0001 C CNN
F 3 "" H 1850 4300 50  0001 C CNN
	1    1850 4300
	-1   0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:Conn_02x02_Odd_Even J3
U 1 1 5B767E6D
P 2100 1000
F 0 "J3" H 2150 1100 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 2150 800 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MiniFit-JR-5569-04A2_2x02x4.20mm_Angled" H 2100 1000 50  0001 C CNN
F 3 "" H 2100 1000 50  0001 C CNN
	1    2100 1000
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:GND #PWR4
U 1 1 5B76807B
P 2700 1000
F 0 "#PWR4" H 2700 750 50  0001 C CNN
F 1 "GND" H 2700 850 50  0000 C CNN
F 2 "" H 2700 1000 50  0001 C CNN
F 3 "" H 2700 1000 50  0001 C CNN
	1    2700 1000
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:GND #PWR5
U 1 1 5B76834F
P 4700 4650
F 0 "#PWR5" H 4700 4400 50  0001 C CNN
F 1 "GND" H 4700 4500 50  0000 C CNN
F 2 "" H 4700 4650 50  0001 C CNN
F 3 "" H 4700 4650 50  0001 C CNN
	1    4700 4650
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:Conn_02x02_Odd_Even J4
U 1 1 5B768AD9
P 2100 1450
F 0 "J4" H 2150 1550 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 2150 1250 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MiniFit-JR-5569-04A2_2x02x4.20mm_Angled" H 2100 1450 50  0001 C CNN
F 3 "" H 2100 1450 50  0001 C CNN
	1    2100 1450
	1    0    0    -1  
$EndComp
Text Label 3900 3200 2    60   ~ 0
VOUT
Text Label 6100 3200 0    60   ~ 0
VOUT
$Comp
L pbv3_test_adapter_passive-rescue:Conn_02x14_Odd_Even J9
U 1 1 5B76754C
P 2250 3500
F 0 "J9" H 2300 4200 50  0000 C CNN
F 1 "Conn_02x14_Odd_Even" H 2300 2700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x14_Pitch2.54mm_SMD" H 2250 3500 50  0001 C CNN
F 3 "" H 2250 3500 50  0001 C CNN
	1    2250 3500
	1    0    0    -1  
$EndComp
Text Label 1600 1550 2    60   ~ 0
VOUT
$Comp
L pbv3_test_adapter_passive-rescue:Conn_01x02 J6
U 1 1 5B769C52
P 2750 6400
F 0 "J6" H 2750 6500 50  0000 C CNN
F 1 "Conn_01x02" H 2750 6200 50  0000 C CNN
F 2 "lbl_conn:lemo_epb00250ntn" H 2750 6400 50  0001 C CNN
F 3 "" H 2750 6400 50  0001 C CNN
	1    2750 6400
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:Conn_01x02 J5
U 1 1 5B769E63
P 1400 6400
F 0 "J5" H 1400 6500 50  0000 C CNN
F 1 "Conn_01x02" H 1400 6200 50  0000 C CNN
F 2 "lbl_conn:lemo_epb00250ntn" H 1400 6400 50  0001 C CNN
F 3 "" H 1400 6400 50  0001 C CNN
	1    1400 6400
	1    0    0    -1  
$EndComp
Text Label 4250 4900 2    60   ~ 0
HVin
Text Label 4250 4800 2    60   ~ 0
HVout
Text Label 2200 6100 2    60   ~ 0
HVout
Text Label 1200 6400 2    60   ~ 0
HVin
$Comp
L pbv3_test_adapter_passive-rescue:R R1
U 1 1 5B762D36
P 2350 6250
F 0 "R1" V 2430 6250 50  0000 C CNN
F 1 "255k" V 2350 6250 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 2280 6250 50  0001 C CNN
F 3 "" H 2350 6250 50  0001 C CNN
	1    2350 6250
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R2
U 1 1 5B762F8B
P 2350 6650
F 0 "R2" V 2430 6650 50  0000 C CNN
F 1 "255k" V 2350 6650 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 2280 6650 50  0001 C CNN
F 3 "" H 2350 6650 50  0001 C CNN
	1    2350 6650
	1    0    0    -1  
$EndComp
Text Label 2200 6800 2    60   ~ 0
HVretx
Text Label 3900 4300 2    60   ~ 0
HVretx
Text Label 6100 4300 0    60   ~ 0
HVrety
$Comp
L pbv3_test_adapter_passive-rescue:Conn_02x01 J8
U 1 1 5B7727F6
P 4850 900
F 0 "J8" H 4900 1000 50  0000 C CNN
F 1 "Conn_02x01" H 4900 800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x01_Pitch2.54mm_SMD" H 4850 900 50  0001 C CNN
F 3 "" H 4850 900 50  0001 C CNN
	1    4850 900 
	1    0    0    -1  
$EndComp
Text Label 5300 900  0    60   ~ 0
COILn
Text Label 4500 900  2    60   ~ 0
COILp
$Comp
L pbv3_test_adapter_passive-rescue:Conn_02x14_Odd_Even J10
U 1 1 5B7A634B
P 7650 3500
F 0 "J10" H 7700 4200 50  0000 C CNN
F 1 "Conn_02x14_Odd_Even" H 7700 2700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x14_Pitch2.54mm_SMD" H 7650 3500 50  0001 C CNN
F 3 "" H 7650 3500 50  0001 C CNN
	1    7650 3500
	1    0    0    -1  
$EndComp
Text Label 3900 3300 2    60   ~ 0
VOUT_RTN
Text Label 6100 3300 0    60   ~ 0
VOUT_RTN
$Comp
L pbv3_test_adapter_passive-rescue:+12V #PWR6
U 1 1 5B7A769D
P 4700 5050
F 0 "#PWR6" H 4700 4900 50  0001 C CNN
F 1 "+12V" H 4700 5190 50  0000 C CNN
F 2 "" H 4700 5050 50  0001 C CNN
F 3 "" H 4700 5050 50  0001 C CNN
	1    4700 5050
	1    0    0    -1  
$EndComp
Text Label 5500 5050 0    60   ~ 0
OFin
Text Label 5500 4550 3    60   ~ 0
OFout
Text Label 2800 4100 0    60   ~ 0
OFin
Text Label 3450 4200 0    60   ~ 0
OFout
Text Label 2700 1450 0    60   ~ 0
VOUT_RTN
Text Label 4900 4550 3    60   ~ 0
CMDin_N
Text Label 5000 4550 3    60   ~ 0
CMDin_P
Text Label 5100 4550 3    60   ~ 0
CMDout_P
Text Label 5200 4550 3    60   ~ 0
CMDout_N
Text Label 6000 6650 0    60   ~ 0
CMDout_P
Text Label 5500 6650 2    60   ~ 0
CMDout_N
Text Label 6000 6750 0    60   ~ 0
CMDin_P
Text Label 5500 6750 2    60   ~ 0
CMDin_N
Text Label 5400 5350 0    60   ~ 0
CAL
Text Label 5500 6950 2    60   ~ 0
CAL
$Comp
L pbv3_test_adapter_passive-rescue:GND #PWR8
U 1 1 5B7CE369
P 6500 7050
F 0 "#PWR8" H 6500 6800 50  0001 C CNN
F 1 "GND" H 6500 6900 50  0000 C CNN
F 2 "" H 6500 7050 50  0001 C CNN
F 3 "" H 6500 7050 50  0001 C CNN
	1    6500 7050
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:+12V #PWR2
U 1 1 5B7D3107
P 1600 1100
F 0 "#PWR2" H 1600 950 50  0001 C CNN
F 1 "+12V" H 1600 1240 50  0000 C CNN
F 2 "" H 1600 1100 50  0001 C CNN
F 3 "" H 1600 1100 50  0001 C CNN
	1    1600 1100
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:PWR_FLAG #FLG1
U 1 1 5B7D3C41
P 950 6400
F 0 "#FLG1" H 950 6475 50  0001 C CNN
F 1 "PWR_FLAG" H 950 6550 50  0000 C CNN
F 2 "" H 950 6400 50  0001 C CNN
F 3 "" H 950 6400 50  0001 C CNN
	1    950  6400
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:PWR_FLAG #FLG2
U 1 1 5B7D4760
P 1750 1000
F 0 "#FLG2" H 1750 1075 50  0001 C CNN
F 1 "PWR_FLAG" H 1750 1150 50  0000 C CNN
F 2 "" H 1750 1000 50  0001 C CNN
F 3 "" H 1750 1000 50  0001 C CNN
	1    1750 1000
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:pbv3 U1
U 1 1 5B7D52B6
P 5000 3300
F 0 "U1" H 5850 3850 60  0000 C CNN
F 1 "pbv3" H 4150 3850 60  0000 C CNN
F 2 "lbl_strips:pbv3" H 6050 2700 60  0001 C CNN
F 3 "" H 6050 2700 60  0001 C CNN
	1    5000 3300
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R3
U 1 1 5B7B9C1B
P 3200 2900
F 0 "R3" V 3250 2700 50  0000 C CNN
F 1 "10k" V 3200 2900 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3130 2900 50  0001 C CNN
F 3 "" H 3200 2900 50  0001 C CNN
	1    3200 2900
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R4
U 1 1 5B7B9FFF
P 3200 3000
F 0 "R4" V 3250 2800 50  0000 C CNN
F 1 "10k" V 3200 3000 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3130 3000 50  0001 C CNN
F 3 "" H 3200 3000 50  0001 C CNN
	1    3200 3000
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R5
U 1 1 5B7BA03F
P 3200 3100
F 0 "R5" V 3250 2900 50  0000 C CNN
F 1 "10k" V 3200 3100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3130 3100 50  0001 C CNN
F 3 "" H 3200 3100 50  0001 C CNN
	1    3200 3100
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R6
U 1 1 5B7BA082
P 3200 3200
F 0 "R6" V 3250 3000 50  0000 C CNN
F 1 "10k" V 3200 3200 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3130 3200 50  0001 C CNN
F 3 "" H 3200 3200 50  0001 C CNN
	1    3200 3200
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R7
U 1 1 5B7BA0CC
P 3200 3300
F 0 "R7" V 3250 3100 50  0000 C CNN
F 1 "10k" V 3200 3300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3130 3300 50  0001 C CNN
F 3 "" H 3200 3300 50  0001 C CNN
	1    3200 3300
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R8
U 1 1 5B7BA115
P 3200 3400
F 0 "R8" V 3250 3200 50  0000 C CNN
F 1 "10k" V 3200 3400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3130 3400 50  0001 C CNN
F 3 "" H 3200 3400 50  0001 C CNN
	1    3200 3400
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R9
U 1 1 5B7BA161
P 3200 3500
F 0 "R9" V 3250 3300 50  0000 C CNN
F 1 "10k" V 3200 3500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3130 3500 50  0001 C CNN
F 3 "" H 3200 3500 50  0001 C CNN
	1    3200 3500
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R10
U 1 1 5B7BA1B0
P 3200 3600
F 0 "R10" V 3250 3400 50  0000 C CNN
F 1 "10k" V 3200 3600 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3130 3600 50  0001 C CNN
F 3 "" H 3200 3600 50  0001 C CNN
	1    3200 3600
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R11
U 1 1 5B7BA202
P 3200 3700
F 0 "R11" V 3250 3500 50  0000 C CNN
F 1 "10k" V 3200 3700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3130 3700 50  0001 C CNN
F 3 "" H 3200 3700 50  0001 C CNN
	1    3200 3700
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R12
U 1 1 5B7BA25B
P 3200 3800
F 0 "R12" V 3250 3600 50  0000 C CNN
F 1 "10k" V 3200 3800 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3130 3800 50  0001 C CNN
F 3 "" H 3200 3800 50  0001 C CNN
	1    3200 3800
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R13
U 1 1 5B7BA2B3
P 3200 3900
F 0 "R13" V 3250 3700 50  0000 C CNN
F 1 "10k" V 3200 3900 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3130 3900 50  0001 C CNN
F 3 "" H 3200 3900 50  0001 C CNN
	1    3200 3900
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R14
U 1 1 5B7BA30E
P 3200 4000
F 0 "R14" V 3250 3800 50  0000 C CNN
F 1 "10k" V 3200 4000 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3130 4000 50  0001 C CNN
F 3 "" H 3200 4000 50  0001 C CNN
	1    3200 4000
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R20
U 1 1 5B7BC531
P 6750 2900
F 0 "R20" V 6800 2700 50  0000 C CNN
F 1 "10k" V 6750 2900 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6680 2900 50  0001 C CNN
F 3 "" H 6750 2900 50  0001 C CNN
	1    6750 2900
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R21
U 1 1 5B7BC537
P 6750 3000
F 0 "R21" V 6800 2800 50  0000 C CNN
F 1 "10k" V 6750 3000 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6680 3000 50  0001 C CNN
F 3 "" H 6750 3000 50  0001 C CNN
	1    6750 3000
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R22
U 1 1 5B7BC53D
P 6750 3100
F 0 "R22" V 6800 2900 50  0000 C CNN
F 1 "10k" V 6750 3100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6680 3100 50  0001 C CNN
F 3 "" H 6750 3100 50  0001 C CNN
	1    6750 3100
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R23
U 1 1 5B7BC543
P 6750 3200
F 0 "R23" V 6800 3000 50  0000 C CNN
F 1 "10k" V 6750 3200 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6680 3200 50  0001 C CNN
F 3 "" H 6750 3200 50  0001 C CNN
	1    6750 3200
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R24
U 1 1 5B7BC549
P 6750 3300
F 0 "R24" V 6800 3100 50  0000 C CNN
F 1 "10k" V 6750 3300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6680 3300 50  0001 C CNN
F 3 "" H 6750 3300 50  0001 C CNN
	1    6750 3300
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R25
U 1 1 5B7BC54F
P 6750 3400
F 0 "R25" V 6800 3200 50  0000 C CNN
F 1 "10k" V 6750 3400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6680 3400 50  0001 C CNN
F 3 "" H 6750 3400 50  0001 C CNN
	1    6750 3400
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R26
U 1 1 5B7BC555
P 6750 3500
F 0 "R26" V 6800 3300 50  0000 C CNN
F 1 "10k" V 6750 3500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6680 3500 50  0001 C CNN
F 3 "" H 6750 3500 50  0001 C CNN
	1    6750 3500
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R27
U 1 1 5B7BC55B
P 6750 3600
F 0 "R27" V 6800 3400 50  0000 C CNN
F 1 "10k" V 6750 3600 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6680 3600 50  0001 C CNN
F 3 "" H 6750 3600 50  0001 C CNN
	1    6750 3600
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R28
U 1 1 5B7BC561
P 6750 3700
F 0 "R28" V 6800 3500 50  0000 C CNN
F 1 "10k" V 6750 3700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6680 3700 50  0001 C CNN
F 3 "" H 6750 3700 50  0001 C CNN
	1    6750 3700
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R29
U 1 1 5B7BC567
P 6750 3800
F 0 "R29" V 6800 3600 50  0000 C CNN
F 1 "10k" V 6750 3800 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6680 3800 50  0001 C CNN
F 3 "" H 6750 3800 50  0001 C CNN
	1    6750 3800
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R30
U 1 1 5B7BC56D
P 6750 3900
F 0 "R30" V 6800 3700 50  0000 C CNN
F 1 "10k" V 6750 3900 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6680 3900 50  0001 C CNN
F 3 "" H 6750 3900 50  0001 C CNN
	1    6750 3900
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R31
U 1 1 5B7BC573
P 6750 4000
F 0 "R31" V 6800 3800 50  0000 C CNN
F 1 "10k" V 6750 4000 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6680 4000 50  0001 C CNN
F 3 "" H 6750 4000 50  0001 C CNN
	1    6750 4000
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R32
U 1 1 5B7BC579
P 6750 4100
F 0 "R32" V 6800 3900 50  0000 C CNN
F 1 "10k" V 6750 4100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6680 4100 50  0001 C CNN
F 3 "" H 6750 4100 50  0001 C CNN
	1    6750 4100
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R33
U 1 1 5B7BC57F
P 6750 4200
F 0 "R33" V 6800 4000 50  0000 C CNN
F 1 "10k" V 6750 4200 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6680 4200 50  0001 C CNN
F 3 "" H 6750 4200 50  0001 C CNN
	1    6750 4200
	0    -1   -1   0   
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R18
U 1 1 5B7BD6AE
P 5300 5200
F 0 "R18" V 5380 5200 50  0000 C CNN
F 1 "1k" V 5300 5200 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 5230 5200 50  0001 C CNN
F 3 "" H 5300 5200 50  0001 C CNN
	1    5300 5200
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:R R19
U 1 1 5B7BF9CC
P 6250 7050
F 0 "R19" V 6330 7050 50  0000 C CNN
F 1 "0" V 6250 7050 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6180 7050 50  0001 C CNN
F 3 "" H 6250 7050 50  0001 C CNN
	1    6250 7050
	0    1    1    0   
$EndComp
Text Label 5600 5900 2    60   ~ 0
CMDout_P
Text Label 5600 6000 2    60   ~ 0
CMDout_N
Text Label 5600 6200 2    60   ~ 0
CMDin_P
Text Label 5600 6100 2    60   ~ 0
CMDin_N
$Comp
L pbv3_test_adapter_passive-rescue:Conn_02x05_Odd_Even J1
U 1 1 5B7D13EF
P 5700 6850
F 0 "J1" H 5750 7150 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 5750 6550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x05_Pitch2.54mm" H 5700 6850 50  0001 C CNN
F 3 "" H 5700 6850 50  0001 C CNN
	1    5700 6850
	1    0    0    -1  
$EndComp
Text Label 5500 6850 2    60   ~ 0
OFin
$Comp
L pbv3_test_adapter_passive-rescue:R R16
U 1 1 5B7D2781
P 5400 4900
F 0 "R16" V 5480 4900 50  0000 C CNN
F 1 "20k" V 5400 4900 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 5330 4900 50  0001 C CNN
F 3 "" H 5400 4900 50  0001 C CNN
	1    5400 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 2900 8150 2900
Wire Wire Line
	7950 3000 8150 3000
Connection ~ 8150 3000
Connection ~ 8150 3100
Connection ~ 8150 3200
Connection ~ 8150 3300
Connection ~ 8150 3400
Connection ~ 8150 3500
Connection ~ 8150 3600
Connection ~ 8150 3700
Connection ~ 8150 3800
Connection ~ 8150 3900
Connection ~ 8150 4000
Connection ~ 8150 4100
Wire Wire Line
	7950 3100 8150 3100
Wire Wire Line
	7950 3200 8150 3200
Wire Wire Line
	7950 3300 8150 3300
Wire Wire Line
	7950 3400 8150 3400
Wire Wire Line
	7950 3500 8150 3500
Wire Wire Line
	7950 3600 8150 3600
Wire Wire Line
	7950 3700 8150 3700
Wire Wire Line
	7950 3800 8150 3800
Wire Wire Line
	7950 3900 8150 3900
Wire Wire Line
	7950 4000 8150 4000
Wire Wire Line
	7950 4100 8150 4100
Wire Wire Line
	7950 4200 8150 4200
Connection ~ 8150 4200
Wire Wire Line
	2050 2900 1850 2900
Wire Wire Line
	1850 2900 1850 3000
Wire Wire Line
	1850 3000 1850 3100
Wire Wire Line
	1850 3100 1850 3200
Wire Wire Line
	1850 3200 1850 3300
Wire Wire Line
	1850 3300 1850 3400
Wire Wire Line
	1850 3400 1850 3500
Wire Wire Line
	1850 3500 1850 3600
Wire Wire Line
	1850 3600 1850 3700
Wire Wire Line
	1850 3700 1850 3800
Wire Wire Line
	1850 3800 1850 3900
Wire Wire Line
	1850 3900 1850 4000
Wire Wire Line
	1850 4000 1850 4100
Wire Wire Line
	1850 4100 1850 4200
Wire Wire Line
	1850 4200 1850 4300
Wire Wire Line
	2050 3000 1850 3000
Connection ~ 1850 3000
Connection ~ 1850 3100
Connection ~ 1850 3200
Connection ~ 1850 3300
Connection ~ 1850 3400
Connection ~ 1850 3500
Connection ~ 1850 3600
Connection ~ 1850 3700
Connection ~ 1850 3800
Connection ~ 1850 3900
Connection ~ 1850 4000
Connection ~ 1850 4100
Wire Wire Line
	2050 3100 1850 3100
Wire Wire Line
	2050 3200 1850 3200
Wire Wire Line
	2050 3300 1850 3300
Wire Wire Line
	2050 3400 1850 3400
Wire Wire Line
	2050 3500 1850 3500
Wire Wire Line
	2050 3600 1850 3600
Wire Wire Line
	2050 3700 1850 3700
Wire Wire Line
	2050 3800 1850 3800
Wire Wire Line
	2050 3900 1850 3900
Connection ~ 1850 4200
Wire Wire Line
	1900 1000 2400 1000
Wire Wire Line
	2400 1000 2700 1000
Connection ~ 2400 1000
Wire Wire Line
	1600 1100 1750 1100
Wire Wire Line
	1750 1100 1900 1100
Wire Wire Line
	1900 1100 2400 1100
Connection ~ 1900 1100
Wire Wire Line
	4700 4550 4700 4650
Wire Wire Line
	4800 4550 4800 5050
Wire Wire Line
	4800 5050 4700 5050
Wire Wire Line
	1900 1450 2400 1450
Wire Wire Line
	2400 1450 2700 1450
Connection ~ 2400 1450
Wire Wire Line
	1600 1550 1900 1550
Wire Wire Line
	1900 1550 2400 1550
Connection ~ 1900 1550
Wire Wire Line
	4600 4550 4600 4900
Wire Wire Line
	4600 4900 4250 4900
Wire Wire Line
	4500 4550 4500 4800
Wire Wire Line
	4500 4800 4250 4800
Wire Wire Line
	950  6500 1200 6500
Wire Wire Line
	2350 6500 2350 6400
Wire Wire Line
	2550 6100 2550 6400
Wire Wire Line
	2200 6100 2350 6100
Wire Wire Line
	2350 6100 2550 6100
Connection ~ 2350 6100
Wire Wire Line
	2550 6800 2550 6500
Wire Wire Line
	2200 6800 2350 6800
Wire Wire Line
	2350 6800 2550 6800
Connection ~ 2350 6800
Wire Wire Line
	4650 900  4500 900 
Wire Wire Line
	4500 900  4500 1200
Wire Wire Line
	4500 1200 5300 1200
Wire Wire Line
	5300 1200 5300 900 
Wire Wire Line
	5300 900  5150 900 
Wire Wire Line
	8150 2900 8150 3000
Wire Wire Line
	8150 3000 8150 3100
Wire Wire Line
	8150 3100 8150 3200
Wire Wire Line
	8150 3200 8150 3300
Wire Wire Line
	8150 3300 8150 3400
Wire Wire Line
	8150 3400 8150 3500
Wire Wire Line
	8150 3500 8150 3600
Wire Wire Line
	8150 3600 8150 3700
Wire Wire Line
	8150 3700 8150 3800
Wire Wire Line
	8150 3800 8150 3900
Wire Wire Line
	8150 3900 8150 4000
Wire Wire Line
	8150 4000 8150 4100
Wire Wire Line
	8150 4100 8150 4200
Wire Wire Line
	8150 4200 8150 4300
Wire Wire Line
	2050 4000 1850 4000
Wire Wire Line
	2050 4100 1850 4100
Wire Wire Line
	1850 4200 2050 4200
Wire Wire Line
	950  6400 1200 6400
Wire Wire Line
	1750 1100 1750 1000
Connection ~ 1750 1100
Wire Wire Line
	3050 2900 2550 2900
Wire Wire Line
	3050 3000 2550 3000
Wire Wire Line
	3050 3100 2550 3100
Wire Wire Line
	3050 3200 2550 3200
Wire Wire Line
	3050 3300 2550 3300
Wire Wire Line
	3050 3400 2550 3400
Wire Wire Line
	3050 3500 2550 3500
Wire Wire Line
	3050 3600 2550 3600
Wire Wire Line
	3050 3700 2550 3700
Wire Wire Line
	3050 3800 2550 3800
Wire Wire Line
	3050 3900 2550 3900
Wire Wire Line
	3050 4000 2550 4000
Wire Wire Line
	3350 2900 3900 2900
Wire Wire Line
	3350 3000 3900 3000
Wire Wire Line
	3350 3100 3900 3100
Wire Wire Line
	3350 3200 3900 3200
Wire Wire Line
	3350 3300 3900 3300
Wire Wire Line
	3350 3400 3900 3400
Wire Wire Line
	3350 3500 3900 3500
Wire Wire Line
	3350 3600 3900 3600
Wire Wire Line
	3350 3700 3900 3700
Wire Wire Line
	3350 3800 3900 3800
Wire Wire Line
	3350 3900 3900 3900
Wire Wire Line
	3350 4000 3900 4000
Wire Wire Line
	6600 2900 6100 2900
Wire Wire Line
	6600 3000 6100 3000
Wire Wire Line
	6600 3100 6100 3100
Wire Wire Line
	6600 3200 6100 3200
Wire Wire Line
	6600 3300 6100 3300
Wire Wire Line
	6600 3400 6100 3400
Wire Wire Line
	6600 3500 6100 3500
Wire Wire Line
	6600 3600 6100 3600
Wire Wire Line
	6600 3700 6100 3700
Wire Wire Line
	6600 3800 6100 3800
Wire Wire Line
	6600 3900 6100 3900
Wire Wire Line
	6600 4000 6100 4000
Wire Wire Line
	6600 4100 6100 4100
Wire Wire Line
	6600 4200 6100 4200
Wire Wire Line
	6900 2900 7450 2900
Wire Wire Line
	6900 3000 7450 3000
Wire Wire Line
	6900 3100 7450 3100
Wire Wire Line
	6900 3200 7450 3200
Wire Wire Line
	6900 3300 7450 3300
Wire Wire Line
	6900 3400 7450 3400
Wire Wire Line
	6900 3500 7450 3500
Wire Wire Line
	6900 3600 7450 3600
Wire Wire Line
	6900 3700 7450 3700
Wire Wire Line
	6900 3800 7450 3800
Wire Wire Line
	6900 3900 7450 3900
Wire Wire Line
	6900 4000 7450 4000
Wire Wire Line
	6900 4100 7450 4100
Wire Wire Line
	6900 4200 7450 4200
Wire Wire Line
	5300 4550 5300 5050
Wire Wire Line
	6500 7050 6400 7050
Wire Wire Line
	5500 7050 6000 7050
Wire Wire Line
	6000 7050 6100 7050
Wire Wire Line
	6000 7050 6000 6950
Wire Wire Line
	6000 6950 6000 6850
Connection ~ 6000 7050
Connection ~ 6000 6950
Wire Wire Line
	5400 4750 5400 4550
Wire Wire Line
	5300 5350 5400 5350
Wire Wire Line
	5400 5050 5500 5050
Wire Wire Line
	2800 4100 2550 4100
Wire Wire Line
	2550 4200 3050 4200
$Comp
L pbv3_test_adapter_passive-rescue:Conn_01x04 J2
U 1 1 5B7D95D8
P 5800 6000
F 0 "J2" H 5800 6200 50  0000 C CNN
F 1 "Conn_01x04" H 5800 5700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 5800 6000 50  0001 C CNN
F 3 "" H 5800 6000 50  0001 C CNN
	1    5800 6000
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:C C1
U 1 1 5B7DA71D
P 5100 5200
F 0 "C1" H 5125 5300 50  0000 L CNN
F 1 "0.1uF" H 4850 5100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 5138 5050 50  0001 C CNN
F 3 "" H 5100 5200 50  0001 C CNN
	1    5100 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 5050 5100 5050
$Comp
L pbv3_test_adapter_passive-rescue:GND #PWR7
U 1 1 5B7DAADD
P 5100 5400
F 0 "#PWR7" H 5100 5150 50  0001 C CNN
F 1 "GND" H 5100 5250 50  0000 C CNN
F 2 "" H 5100 5400 50  0001 C CNN
F 3 "" H 5100 5400 50  0001 C CNN
	1    5100 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 5400 5100 5350
$Comp
L pbv3_test_adapter_passive-rescue:R R17
U 1 1 5B7E356D
P 3200 4200
F 0 "R17" V 3250 4000 50  0000 C CNN
F 1 "10k" V 3200 4200 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3130 4200 50  0001 C CNN
F 3 "" H 3200 4200 50  0001 C CNN
	1    3200 4200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3450 4200 3350 4200
$Comp
L pbv3_test_adapter_passive-rescue:HOLE U2
U 1 1 5B7EDFD1
P 9500 1000
F 0 "U2" H 9500 1100 60  0000 C CNN
F 1 "HOLE" H 9500 950 60  0000 C CNN
F 2 "Mounting_Holes:MountingHole_5mm" H 9500 1000 60  0001 C CNN
F 3 "" H 9500 1000 60  0001 C CNN
	1    9500 1000
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:HOLE U4
U 1 1 5B7EE101
P 9900 1000
F 0 "U4" H 9900 1100 60  0000 C CNN
F 1 "HOLE" H 9900 950 60  0000 C CNN
F 2 "Mounting_Holes:MountingHole_5mm" H 9900 1000 60  0001 C CNN
F 3 "" H 9900 1000 60  0001 C CNN
	1    9900 1000
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:HOLE U3
U 1 1 5B7EE1D8
P 9500 1400
F 0 "U3" H 9500 1500 60  0000 C CNN
F 1 "HOLE" H 9500 1350 60  0000 C CNN
F 2 "Mounting_Holes:MountingHole_5mm" H 9500 1400 60  0001 C CNN
F 3 "" H 9500 1400 60  0001 C CNN
	1    9500 1400
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:HOLE U5
U 1 1 5B7EE2D2
P 9900 1400
F 0 "U5" H 9900 1500 60  0000 C CNN
F 1 "HOLE" H 9900 1350 60  0000 C CNN
F 2 "Mounting_Holes:MountingHole_5mm" H 9900 1400 60  0001 C CNN
F 3 "" H 9900 1400 60  0001 C CNN
	1    9900 1400
	1    0    0    -1  
$EndComp
Text Notes 9100 750  0    120  ~ 0
Mounting Holes
$Comp
L pbv3_test_adapter_passive-rescue:GND #PWR1
U 1 1 5B76A3B0
P 950 6500
F 0 "#PWR1" H 950 6250 50  0001 C CNN
F 1 "GND" H 950 6350 50  0000 C CNN
F 2 "" H 950 6500 50  0001 C CNN
F 3 "" H 950 6500 50  0001 C CNN
	1    950  6500
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_passive-rescue:Conn_02x01 J7
U 1 1 5B848A95
P 4850 1400
F 0 "J7" H 4900 1500 50  0000 C CNN
F 1 "Conn_02x01" H 4900 1300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x01_Pitch2.54mm_SMD" H 4850 1400 50  0001 C CNN
F 3 "" H 4850 1400 50  0001 C CNN
	1    4850 1400
	1    0    0    -1  
$EndComp
Text Label 5300 1400 0    60   ~ 0
PLATEn
Text Label 4500 1400 2    60   ~ 0
PLATEp
Wire Wire Line
	4650 1400 4500 1400
Wire Wire Line
	4500 1400 4500 1700
Wire Wire Line
	4500 1700 5300 1700
Wire Wire Line
	5300 1700 5300 1400
Wire Wire Line
	5300 1400 5150 1400
$EndSCHEMATC
