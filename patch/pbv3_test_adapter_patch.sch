EESchema Schematic File Version 4
LIBS:pbv3_test_adapter_patch-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L lbl_adafruit:adafruit_ft232h U1
U 1 1 5B7B3952
P 2000 1800
F 0 "U1" H 1650 2650 60  0000 C CNN
F 1 "adafruit_ft232h" H 2000 2450 60  0000 C CNN
F 2 "lbl_adafruit:adafruit_ft232h_nohole" H 2000 1800 60  0001 C CNN
F 3 "" H 2000 1800 60  0001 C CNN
	1    2000 1800
	1    0    0    -1  
$EndComp
NoConn ~ 1400 1650
$Comp
L power:GND #PWR01
U 1 1 5B7B44ED
P 2000 2650
F 0 "#PWR01" H 2000 2400 50  0001 C CNN
F 1 "GND" H 2000 2500 50  0000 C CNN
F 2 "" H 2000 2650 50  0001 C CNN
F 3 "" H 2000 2650 50  0001 C CNN
	1    2000 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2550 2000 2650
$Comp
L power:+5V #PWR02
U 1 1 5B7B4D47
P 2600 1000
F 0 "#PWR02" H 2600 850 50  0001 C CNN
F 1 "+5V" H 2600 1140 50  0000 C CNN
F 2 "" H 2600 1000 50  0001 C CNN
F 3 "" H 2600 1000 50  0001 C CNN
	1    2600 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 1050 2600 1000
NoConn ~ 2600 1550
NoConn ~ 2600 1650
NoConn ~ 2600 1750
NoConn ~ 2600 1850
NoConn ~ 2600 1950
Text Label 2600 1450 0    60   ~ 0
CMDout
Text Label 2600 1350 0    60   ~ 0
CMDin
$Comp
L power:+5V #PWR03
U 1 1 5B7B791D
P 4900 1050
F 0 "#PWR03" H 4900 900 50  0001 C CNN
F 1 "+5V" H 4900 1190 50  0000 C CNN
F 2 "" H 4900 1050 50  0001 C CNN
F 3 "" H 4900 1050 50  0001 C CNN
	1    4900 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1150 4900 1050
$Comp
L power:+3V3 #PWR04
U 1 1 5B7B7B38
P 6200 1150
F 0 "#PWR04" H 6200 1000 50  0001 C CNN
F 1 "+3V3" H 6200 1290 50  0000 C CNN
F 2 "" H 6200 1150 50  0001 C CNN
F 3 "" H 6200 1150 50  0001 C CNN
	1    6200 1150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5B7B7CEC
P 5400 1750
F 0 "#PWR05" H 5400 1500 50  0001 C CNN
F 1 "GND" H 5400 1600 50  0000 C CNN
F 2 "" H 5400 1750 50  0001 C CNN
F 3 "" H 5400 1750 50  0001 C CNN
	1    5400 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 1750 5400 1700
Wire Wire Line
	5900 1150 6200 1150
$Comp
L pbv3_test_adapter_patch-rescue:C C7
U 1 1 5B7BA791
P 6550 1200
F 0 "C7" H 6575 1300 50  0000 L CNN
F 1 "1uF" H 6575 1100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 6588 1050 50  0001 C CNN
F 3 "" H 6550 1200 50  0001 C CNN
	1    6550 1200
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_patch-rescue:C C8
U 1 1 5B7BA7FC
P 6850 1200
F 0 "C8" H 6875 1300 50  0000 L CNN
F 1 "1uF" H 6875 1100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 6888 1050 50  0001 C CNN
F 3 "" H 6850 1200 50  0001 C CNN
	1    6850 1200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5B7BA84D
P 6550 1400
F 0 "#PWR06" H 6550 1150 50  0001 C CNN
F 1 "GND" H 6550 1250 50  0000 C CNN
F 2 "" H 6550 1400 50  0001 C CNN
F 3 "" H 6550 1400 50  0001 C CNN
	1    6550 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 1400 6550 1350
$Comp
L power:GND #PWR07
U 1 1 5B7BA916
P 6850 1400
F 0 "#PWR07" H 6850 1150 50  0001 C CNN
F 1 "GND" H 6850 1250 50  0000 C CNN
F 2 "" H 6850 1400 50  0001 C CNN
F 3 "" H 6850 1400 50  0001 C CNN
	1    6850 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 1400 6850 1350
$Comp
L power:+3V3 #PWR08
U 1 1 5B7BA9E0
P 6850 1000
F 0 "#PWR08" H 6850 850 50  0001 C CNN
F 1 "+3V3" H 6850 1140 50  0000 C CNN
F 2 "" H 6850 1000 50  0001 C CNN
F 3 "" H 6850 1000 50  0001 C CNN
	1    6850 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 1050 6850 1000
$Comp
L power:+5V #PWR09
U 1 1 5B7BAAAB
P 6550 1000
F 0 "#PWR09" H 6550 850 50  0001 C CNN
F 1 "+5V" H 6550 1140 50  0000 C CNN
F 2 "" H 6550 1000 50  0001 C CNN
F 3 "" H 6550 1000 50  0001 C CNN
	1    6550 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 1050 6550 1000
$Comp
L power:+3V3 #PWR010
U 1 1 5B7BB0E0
P 2650 3850
F 0 "#PWR010" H 2650 3700 50  0001 C CNN
F 1 "+3V3" H 2650 3990 50  0000 C CNN
F 2 "" H 2650 3850 50  0001 C CNN
F 3 "" H 2650 3850 50  0001 C CNN
	1    2650 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 3900 2650 3850
$Comp
L power:GND #PWR011
U 1 1 5B7BB329
P 2650 4750
F 0 "#PWR011" H 2650 4500 50  0001 C CNN
F 1 "GND" H 2650 4600 50  0000 C CNN
F 2 "" H 2650 4750 50  0001 C CNN
F 3 "" H 2650 4750 50  0001 C CNN
	1    2650 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 4750 2650 4700
$Comp
L power:+3V3 #PWR012
U 1 1 5B7BB3F7
P 2550 5550
F 0 "#PWR012" H 2550 5400 50  0001 C CNN
F 1 "+3V3" H 2550 5690 50  0000 C CNN
F 2 "" H 2550 5550 50  0001 C CNN
F 3 "" H 2550 5550 50  0001 C CNN
	1    2550 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 5600 2550 5550
$Comp
L power:GND #PWR013
U 1 1 5B7BB95D
P 2550 6450
F 0 "#PWR013" H 2550 6200 50  0001 C CNN
F 1 "GND" H 2550 6300 50  0000 C CNN
F 2 "" H 2550 6450 50  0001 C CNN
F 3 "" H 2550 6450 50  0001 C CNN
	1    2550 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 6450 2550 6400
$Comp
L pbv3_test_adapter_patch-rescue:C C4
U 1 1 5B7BCA1F
P 3350 3650
F 0 "C4" H 3375 3750 50  0000 L CNN
F 1 "0.1uF" H 3375 3550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3388 3500 50  0001 C CNN
F 3 "" H 3350 3650 50  0001 C CNN
	1    3350 3650
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_patch-rescue:C C6
U 1 1 5B7BCA25
P 3650 3650
F 0 "C6" H 3675 3750 50  0000 L CNN
F 1 "10uF" H 3675 3550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3688 3500 50  0001 C CNN
F 3 "" H 3650 3650 50  0001 C CNN
	1    3650 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5B7BCA2B
P 3350 3850
F 0 "#PWR014" H 3350 3600 50  0001 C CNN
F 1 "GND" H 3350 3700 50  0000 C CNN
F 2 "" H 3350 3850 50  0001 C CNN
F 3 "" H 3350 3850 50  0001 C CNN
	1    3350 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 3850 3350 3800
$Comp
L power:GND #PWR015
U 1 1 5B7BCA32
P 3650 3850
F 0 "#PWR015" H 3650 3600 50  0001 C CNN
F 1 "GND" H 3650 3700 50  0000 C CNN
F 2 "" H 3650 3850 50  0001 C CNN
F 3 "" H 3650 3850 50  0001 C CNN
	1    3650 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 3850 3650 3800
$Comp
L power:+3V3 #PWR016
U 1 1 5B7BCA39
P 3650 3450
F 0 "#PWR016" H 3650 3300 50  0001 C CNN
F 1 "+3V3" H 3650 3590 50  0000 C CNN
F 2 "" H 3650 3450 50  0001 C CNN
F 3 "" H 3650 3450 50  0001 C CNN
	1    3650 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 3500 3650 3450
Wire Wire Line
	3350 3500 3350 3450
$Comp
L pbv3_test_adapter_patch-rescue:C C3
U 1 1 5B7BD274
P 3300 5350
F 0 "C3" H 3325 5450 50  0000 L CNN
F 1 "0.1uF" H 3325 5250 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3338 5200 50  0001 C CNN
F 3 "" H 3300 5350 50  0001 C CNN
	1    3300 5350
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_patch-rescue:C C5
U 1 1 5B7BD27A
P 3600 5350
F 0 "C5" H 3625 5450 50  0000 L CNN
F 1 "10uF" H 3625 5250 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3638 5200 50  0001 C CNN
F 3 "" H 3600 5350 50  0001 C CNN
	1    3600 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 5B7BD280
P 3300 5550
F 0 "#PWR017" H 3300 5300 50  0001 C CNN
F 1 "GND" H 3300 5400 50  0000 C CNN
F 2 "" H 3300 5550 50  0001 C CNN
F 3 "" H 3300 5550 50  0001 C CNN
	1    3300 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 5550 3300 5500
$Comp
L power:GND #PWR018
U 1 1 5B7BD287
P 3600 5550
F 0 "#PWR018" H 3600 5300 50  0001 C CNN
F 1 "GND" H 3600 5400 50  0000 C CNN
F 2 "" H 3600 5550 50  0001 C CNN
F 3 "" H 3600 5550 50  0001 C CNN
	1    3600 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 5550 3600 5500
$Comp
L power:+3V3 #PWR019
U 1 1 5B7BD28E
P 3600 5150
F 0 "#PWR019" H 3600 5000 50  0001 C CNN
F 1 "+3V3" H 3600 5290 50  0000 C CNN
F 2 "" H 3600 5150 50  0001 C CNN
F 3 "" H 3600 5150 50  0001 C CNN
	1    3600 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 5200 3600 5150
Wire Wire Line
	3300 5200 3300 5150
Text Label 3100 4300 0    60   ~ 0
CMDout
Text Label 2100 6000 2    60   ~ 0
CMDin
Text Label 1100 3950 2    60   ~ 0
CMDout_P
Text Label 1100 4650 2    60   ~ 0
CMDout_N
Text Label 3100 5950 0    60   ~ 0
CMDin_P
Text Label 3100 6050 0    60   ~ 0
CMDin_N
$Comp
L lbl_lvds:ADN4661 U3
U 1 1 5B7C2FF4
P 2600 6000
F 0 "U3" H 2900 5750 60  0000 C CNN
F 1 "ADN4661" H 2250 5750 60  0000 C CNN
F 2 "SMD_Packages:SOIC-8-N" H 2550 5600 60  0001 C CNN
F 3 "" H 2550 5600 60  0001 C CNN
	1    2600 6000
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_patch-rescue:MCP4801 U5
U 1 1 5B7C79FD
P 6300 3400
F 0 "U5" H 6350 3825 50  0000 L CNN
F 1 "MCP4801" H 6350 3750 50  0000 L CNN
F 2 "SMD_Packages:SOIC-8-N" H 7200 3300 50  0001 C CNN
F 3 "" H 7200 3300 50  0001 C CNN
	1    6300 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5B7C8EAE
P 6300 3850
F 0 "#PWR020" H 6300 3600 50  0001 C CNN
F 1 "GND" H 6300 3700 50  0000 C CNN
F 2 "" H 6300 3850 50  0001 C CNN
F 3 "" H 6300 3850 50  0001 C CNN
	1    6300 3850
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR021
U 1 1 5B7C9354
P 6300 2950
F 0 "#PWR021" H 6300 2800 50  0001 C CNN
F 1 "+3V3" H 6300 3090 50  0000 C CNN
F 2 "" H 6300 2950 50  0001 C CNN
F 3 "" H 6300 2950 50  0001 C CNN
	1    6300 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 3000 6300 2950
Wire Wire Line
	6300 3850 6300 3800
Text Label 5800 3600 2    60   ~ 0
SDI
Text Label 5800 3500 2    60   ~ 0
SCK
Text Label 5800 3400 2    60   ~ 0
CS_CAL
$Comp
L power:GND #PWR022
U 1 1 5B7C966B
P 5400 3300
F 0 "#PWR022" H 5400 3050 50  0001 C CNN
F 1 "GND" H 5400 3150 50  0000 C CNN
F 2 "" H 5400 3300 50  0001 C CNN
F 3 "" H 5400 3300 50  0001 C CNN
	1    5400 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3300 5400 3300
$Comp
L power:+3V3 #PWR023
U 1 1 5B7C9763
P 5400 3200
F 0 "#PWR023" H 5400 3050 50  0001 C CNN
F 1 "+3V3" H 5400 3340 50  0000 C CNN
F 2 "" H 5400 3200 50  0001 C CNN
F 3 "" H 5400 3200 50  0001 C CNN
	1    5400 3200
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_patch-rescue:R R6
U 1 1 5B7C97AB
P 5600 3200
F 0 "R6" V 5500 3200 50  0000 C CNN
F 1 "10k" V 5600 3200 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 5530 3200 50  0001 C CNN
F 3 "" H 5600 3200 50  0001 C CNN
	1    5600 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 3200 5400 3200
Wire Wire Line
	5750 3200 5800 3200
Wire Wire Line
	6900 3400 7000 3400
Text Label 7100 3750 0    60   ~ 0
CAL
$Comp
L pbv3_test_adapter_patch-rescue:R R7
U 1 1 5B7CA16B
P 7000 3550
F 0 "R7" V 7080 3550 50  0000 C CNN
F 1 "10k" V 7000 3550 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6930 3550 50  0001 C CNN
F 3 "" H 7000 3550 50  0001 C CNN
	1    7000 3550
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_patch-rescue:R R8
U 1 1 5B7CA286
P 7000 3950
F 0 "R8" V 7080 3950 50  0000 C CNN
F 1 "10k" V 7000 3950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6930 3950 50  0001 C CNN
F 3 "" H 7000 3950 50  0001 C CNN
	1    7000 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3700 7000 3750
$Comp
L power:GND #PWR024
U 1 1 5B7CA39C
P 7000 4150
F 0 "#PWR024" H 7000 3900 50  0001 C CNN
F 1 "GND" H 7000 4000 50  0000 C CNN
F 2 "" H 7000 4150 50  0001 C CNN
F 3 "" H 7000 4150 50  0001 C CNN
	1    7000 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 4150 7000 4100
Wire Wire Line
	7100 3750 7000 3750
Connection ~ 7000 3750
Text Label 1400 1850 2    60   ~ 0
SDI
Text Label 1400 1950 2    60   ~ 0
SCK
Text Label 1400 1550 2    60   ~ 0
CS_CAL
NoConn ~ 2600 1250
Text Label 6300 5300 0    60   ~ 0
CMDout_P
Text Label 5800 5300 2    60   ~ 0
CMDout_N
Text Label 6300 5400 0    60   ~ 0
CMDin_P
Text Label 5800 5400 2    60   ~ 0
CMDin_N
Text Label 5800 5600 2    60   ~ 0
CAL
$Comp
L power:GND #PWR025
U 1 1 5B7BBC8A
P 6500 5750
F 0 "#PWR025" H 6500 5500 50  0001 C CNN
F 1 "GND" H 6500 5600 50  0000 C CNN
F 2 "" H 6500 5750 50  0001 C CNN
F 3 "" H 6500 5750 50  0001 C CNN
	1    6500 5750
	1    0    0    -1  
$EndComp
Text Label 5800 5500 2    60   ~ 0
OFin
Wire Wire Line
	6300 5600 6500 5600
Wire Wire Line
	6500 5500 6500 5600
Connection ~ 6500 5600
Wire Wire Line
	6300 5500 6500 5500
Text Label 1400 1250 2    60   ~ 0
OFin
$Comp
L lbl_power:APD122 U4
U 1 1 5B7C79FB
P 5400 1600
F 0 "U4" H 5150 1600 60  0000 C CNN
F 1 "APD122-3V3" H 5750 1650 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:TSOT-23-5" H 5150 1600 60  0001 C CNN
F 3 "" H 5150 1600 60  0001 C CNN
	1    5400 1600
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_patch-rescue:R R5
U 1 1 5B7C86B6
P 4900 1300
F 0 "R5" V 4980 1300 50  0000 C CNN
F 1 "10k" V 4900 1300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 4830 1300 50  0001 C CNN
F 3 "" H 4900 1300 50  0001 C CNN
	1    4900 1300
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_patch-rescue:C C9
U 1 1 5B7C81EB
P 6950 2800
F 0 "C9" H 6975 2900 50  0000 L CNN
F 1 "1uF" H 6975 2700 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 6988 2650 50  0001 C CNN
F 3 "" H 6950 2800 50  0001 C CNN
	1    6950 2800
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_patch-rescue:C C10
U 1 1 5B7C81F1
P 7250 2800
F 0 "C10" H 7275 2900 50  0000 L CNN
F 1 "10uF" H 7275 2700 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 7288 2650 50  0001 C CNN
F 3 "" H 7250 2800 50  0001 C CNN
	1    7250 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR027
U 1 1 5B7C81F7
P 6950 3000
F 0 "#PWR027" H 6950 2750 50  0001 C CNN
F 1 "GND" H 6950 2850 50  0000 C CNN
F 2 "" H 6950 3000 50  0001 C CNN
F 3 "" H 6950 3000 50  0001 C CNN
	1    6950 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 3000 6950 2950
$Comp
L power:GND #PWR028
U 1 1 5B7C81FE
P 7250 3000
F 0 "#PWR028" H 7250 2750 50  0001 C CNN
F 1 "GND" H 7250 2850 50  0000 C CNN
F 2 "" H 7250 3000 50  0001 C CNN
F 3 "" H 7250 3000 50  0001 C CNN
	1    7250 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 3000 7250 2950
$Comp
L power:+3V3 #PWR029
U 1 1 5B7C8205
P 7250 2600
F 0 "#PWR029" H 7250 2450 50  0001 C CNN
F 1 "+3V3" H 7250 2740 50  0000 C CNN
F 2 "" H 7250 2600 50  0001 C CNN
F 3 "" H 7250 2600 50  0001 C CNN
	1    7250 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 2650 7250 2600
Wire Wire Line
	6950 2650 6950 2600
$Comp
L power:+3V3 #PWR030
U 1 1 5B7C827F
P 6950 2600
F 0 "#PWR030" H 6950 2450 50  0001 C CNN
F 1 "+3V3" H 6950 2740 50  0000 C CNN
F 2 "" H 6950 2600 50  0001 C CNN
F 3 "" H 6950 2600 50  0001 C CNN
	1    6950 2600
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR031
U 1 1 5B7C8613
P 3350 3450
F 0 "#PWR031" H 3350 3300 50  0001 C CNN
F 1 "+3V3" H 3350 3590 50  0000 C CNN
F 2 "" H 3350 3450 50  0001 C CNN
F 3 "" H 3350 3450 50  0001 C CNN
	1    3350 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR032
U 1 1 5B7C8651
P 3300 5150
F 0 "#PWR032" H 3300 5000 50  0001 C CNN
F 1 "+3V3" H 3300 5290 50  0000 C CNN
F 2 "" H 3300 5150 50  0001 C CNN
F 3 "" H 3300 5150 50  0001 C CNN
	1    3300 5150
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_patch-rescue:Conn_02x05_Odd_Even J1
U 1 1 5B7D01E1
P 6000 5500
F 0 "J1" H 6050 5800 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 6050 5200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x05_Pitch2.54mm" H 6000 5500 50  0001 C CNN
F 3 "" H 6000 5500 50  0001 C CNN
	1    6000 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 5700 6300 5700
Connection ~ 6500 5700
Connection ~ 6300 5700
$Comp
L pbv3_test_adapter_patch-rescue:R R3
U 1 1 5B80D943
P 1550 4100
F 0 "R3" V 1630 4100 50  0000 C CNN
F 1 "50" V 1550 4100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 1480 4100 50  0001 C CNN
F 3 "" H 1550 4100 50  0001 C CNN
	1    1550 4100
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_patch-rescue:R R4
U 1 1 5B80D990
P 1550 4500
F 0 "R4" V 1630 4500 50  0000 C CNN
F 1 "50" V 1550 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 1480 4500 50  0001 C CNN
F 3 "" H 1550 4500 50  0001 C CNN
	1    1550 4500
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_patch-rescue:R R1
U 1 1 5B80D9F3
P 600 4100
F 0 "R1" V 680 4100 50  0000 C CNN
F 1 "2.9k" V 600 4100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 530 4100 50  0001 C CNN
F 3 "" H 600 4100 50  0001 C CNN
	1    600  4100
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_patch-rescue:R R2
U 1 1 5B80E171
P 600 4500
F 0 "R2" V 680 4500 50  0000 C CNN
F 1 "1.1k" V 600 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 530 4500 50  0001 C CNN
F 3 "" H 600 4500 50  0001 C CNN
	1    600  4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 4250 1550 4300
Wire Wire Line
	600  4250 600  4300
Connection ~ 1550 4300
Connection ~ 600  4300
Wire Wire Line
	1450 4650 1550 4650
Wire Wire Line
	1450 3950 1550 3950
$Comp
L lbl_lvds:ADN4662 U2
U 1 1 5B7C2F8E
P 2600 4300
F 0 "U2" H 2900 4050 60  0000 C CNN
F 1 "ADN4662" H 2250 4050 60  0000 C CNN
F 2 "SMD_Packages:SOIC-8-N" H 2550 3900 60  0001 C CNN
F 3 "" H 2550 3900 60  0001 C CNN
	1    2600 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 4250 1700 4250
Wire Wire Line
	1700 4250 1700 3950
Connection ~ 1550 3950
Wire Wire Line
	1700 4650 1700 4350
Wire Wire Line
	1700 4350 2100 4350
Connection ~ 1550 4650
Wire Wire Line
	600  4650 600  4750
Wire Wire Line
	600  3950 600  3850
$Comp
L power:+3V3 #PWR033
U 1 1 5B80EF9B
P 600 3850
F 0 "#PWR033" H 600 3700 50  0001 C CNN
F 1 "+3V3" H 600 3990 50  0000 C CNN
F 2 "" H 600 3850 50  0001 C CNN
F 3 "" H 600 3850 50  0001 C CNN
	1    600  3850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR034
U 1 1 5B80EFE3
P 600 4750
F 0 "#PWR034" H 600 4500 50  0001 C CNN
F 1 "GND" H 600 4600 50  0000 C CNN
F 2 "" H 600 4750 50  0001 C CNN
F 3 "" H 600 4750 50  0001 C CNN
	1    600  4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 4300 600  4300
$Comp
L lbl_adc:MAX11619 U6
U 1 1 5B810854
P 10200 1400
F 0 "U6" H 10650 950 50  0000 L CNN
F 1 "MAX11619" H 10650 850 50  0000 L CNN
F 2 "Housings_SSOP:QSOP-16_3.9x4.9mm_Pitch0.635mm" H 10650 750 50  0001 L CNN
F 3 "" H 10300 2200 50  0001 C CNN
	1    10200 1400
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_patch-rescue:Conn_02x04_Odd_Even J2
U 1 1 5B810FBC
P 8550 1000
F 0 "J2" H 8600 1200 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 8600 700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04_Pitch2.54mm" H 8550 1000 50  0001 C CNN
F 3 "" H 8550 1000 50  0001 C CNN
	1    8550 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 900  8100 900 
Wire Wire Line
	8100 900  8100 1000
$Comp
L power:GND #PWR035
U 1 1 5B811291
P 8100 1350
F 0 "#PWR035" H 8100 1100 50  0001 C CNN
F 1 "GND" H 8100 1200 50  0000 C CNN
F 2 "" H 8100 1350 50  0001 C CNN
F 3 "" H 8100 1350 50  0001 C CNN
	1    8100 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 1000 8100 1000
Connection ~ 8100 1000
Wire Wire Line
	8350 1100 8100 1100
Connection ~ 8100 1100
Wire Wire Line
	8350 1200 8100 1200
Connection ~ 8100 1200
$Comp
L power:+3V3 #PWR036
U 1 1 5B81182F
P 10200 650
F 0 "#PWR036" H 10200 500 50  0001 C CNN
F 1 "+3V3" H 10200 790 50  0000 C CNN
F 2 "" H 10200 650 50  0001 C CNN
F 3 "" H 10200 650 50  0001 C CNN
	1    10200 650 
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 700  10200 650 
$Comp
L pbv3_test_adapter_patch-rescue:R R11
U 1 1 5B811EAA
P 9450 1700
F 0 "R11" V 9530 1700 50  0000 C CNN
F 1 "10k" V 9450 1700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 9380 1700 50  0001 C CNN
F 3 "" H 9450 1700 50  0001 C CNN
	1    9450 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 1700 9600 1700
NoConn ~ 9700 1800
Text Label 10700 1300 0    60   ~ 0
SCK
Text Label 10700 1400 0    60   ~ 0
SDO
Text Label 10700 1500 0    60   ~ 0
SDI
Text Label 10700 1600 0    60   ~ 0
CS_ADC
Text Label 1400 1450 2    60   ~ 0
CS_ADC
Text Label 1400 1750 2    60   ~ 0
SDO
Text Label 1400 1350 2    60   ~ 0
~CNVST
Text Label 9300 1700 2    60   ~ 0
~CNVST
Wire Wire Line
	10200 2200 10200 2350
$Comp
L power:GND #PWR037
U 1 1 5B816199
P 10200 2350
F 0 "#PWR037" H 10200 2100 50  0001 C CNN
F 1 "GND" H 10200 2200 50  0000 C CNN
F 2 "" H 10200 2350 50  0001 C CNN
F 3 "" H 10200 2350 50  0001 C CNN
	1    10200 2350
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR038
U 1 1 5B8161E7
P 9550 2000
F 0 "#PWR038" H 9550 1850 50  0001 C CNN
F 1 "+3V3" H 9550 2140 50  0000 C CNN
F 2 "" H 9550 2000 50  0001 C CNN
F 3 "" H 9550 2000 50  0001 C CNN
	1    9550 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 2000 9600 2000
$Comp
L pbv3_test_adapter_patch-rescue:C C11
U 1 1 5B8162A1
P 9600 2150
F 0 "C11" H 9625 2250 50  0000 L CNN
F 1 "0.1uF" H 9625 2050 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 9638 2000 50  0001 C CNN
F 3 "" H 9600 2150 50  0001 C CNN
	1    9600 2150
	1    0    0    -1  
$EndComp
Connection ~ 9600 2000
$Comp
L power:GND #PWR039
U 1 1 5B816318
P 9600 2350
F 0 "#PWR039" H 9600 2100 50  0001 C CNN
F 1 "GND" H 9600 2200 50  0000 C CNN
F 2 "" H 9600 2350 50  0001 C CNN
F 3 "" H 9600 2350 50  0001 C CNN
	1    9600 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 2350 9600 2300
$Comp
L power:+3V3 #PWR040
U 1 1 5B816966
P 11000 650
F 0 "#PWR040" H 11000 500 50  0001 C CNN
F 1 "+3V3" H 11000 790 50  0000 C CNN
F 2 "" H 11000 650 50  0001 C CNN
F 3 "" H 11000 650 50  0001 C CNN
	1    11000 650 
	1    0    0    -1  
$EndComp
$Comp
L pbv3_test_adapter_patch-rescue:C C12
U 1 1 5B8169B6
P 11000 850
F 0 "C12" H 11025 950 50  0000 L CNN
F 1 "0.1uF" H 11025 750 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 11038 700 50  0001 C CNN
F 3 "" H 11000 850 50  0001 C CNN
	1    11000 850 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR041
U 1 1 5B816A2C
P 11000 1050
F 0 "#PWR041" H 11000 800 50  0001 C CNN
F 1 "GND" H 11000 900 50  0000 C CNN
F 2 "" H 11000 1050 50  0001 C CNN
F 3 "" H 11000 1050 50  0001 C CNN
	1    11000 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 1050 11000 1000
Wire Wire Line
	11000 700  11000 650 
$Comp
L pbv3_test_adapter_patch-rescue:C C1
U 1 1 5B807A9A
P 1300 3950
F 0 "C1" H 1325 4050 50  0000 L CNN
F 1 "150pF" H 1325 3850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 1338 3800 50  0001 C CNN
F 3 "" H 1300 3950 50  0001 C CNN
	1    1300 3950
	0    1    1    0   
$EndComp
$Comp
L pbv3_test_adapter_patch-rescue:C C2
U 1 1 5B807AFD
P 1300 4650
F 0 "C2" H 1325 4750 50  0000 L CNN
F 1 "150pF" H 1325 4550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 1338 4500 50  0001 C CNN
F 3 "" H 1300 4650 50  0001 C CNN
	1    1300 4650
	0    1    1    0   
$EndComp
Text Label 1700 3950 0    60   ~ 0
LVDS_CMDout_P
Text Label 1700 4650 0    60   ~ 0
LVDS_CMDout_N
$Comp
L pbv3_test_adapter_patch-rescue:R R12
U 1 1 5B808A59
P 1150 4300
F 0 "R12" V 1230 4300 50  0000 C CNN
F 1 "100" V 1150 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1080 4300 50  0001 C CNN
F 3 "" H 1150 4300 50  0001 C CNN
	1    1150 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 4450 1150 4650
Wire Wire Line
	1150 4150 1150 3950
Wire Wire Line
	1150 3950 1100 3950
Wire Wire Line
	1150 4650 1100 4650
Text Label 9700 900  2    60   ~ 0
ADC0
Text Label 9700 1000 2    60   ~ 0
ADC1
Text Label 9700 1100 2    60   ~ 0
ADC2
Text Label 9700 1200 2    60   ~ 0
ADC3
Text Label 8850 1200 0    60   ~ 0
ADC0
Text Label 8850 1100 0    60   ~ 0
ADC1
Text Label 8850 1000 0    60   ~ 0
ADC2
Text Label 8850 900  0    60   ~ 0
ADC3
Wire Wire Line
	7000 3750 7000 3800
Wire Wire Line
	6500 5600 6500 5700
Wire Wire Line
	6500 5700 6500 5750
Wire Wire Line
	6300 5700 6500 5700
Wire Wire Line
	1550 4300 1550 4350
Wire Wire Line
	600  4300 600  4350
Wire Wire Line
	1550 3950 1700 3950
Wire Wire Line
	1550 4650 1700 4650
Wire Wire Line
	8100 1000 8100 1100
Wire Wire Line
	8100 1100 8100 1200
Wire Wire Line
	8100 1200 8100 1350
Wire Wire Line
	9600 2000 9700 2000
NoConn ~ 1400 2050
NoConn ~ 1400 2150
$EndSCHEMATC
